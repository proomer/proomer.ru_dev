$(function () {

    /* наводим на колонку */
    $('.column').mouseenter(function () {
        $currentColumn = $(this);
        /* фон переключаем на темный */
        $('.column').switchClass("column-default", "column-black", 400, "easeInOutQuad");
        /* показываем содержимое*/
        $currentColumn.find('.content').fadeIn(300, function () {
            $currentColumnText = $(this);
            /* убираем заголовок */
            $currentColumn.find('.title').hide(200, function () {
                /* всплывает текст */
                $currentColumnText.find('.text').slideDown(200);
            });

        });

    });

    /* уходим с колонки */
    $('.column').mouseleave(function () {
        $currentColumn = $(this);
        $currentColumn.find('.title').fadeIn(300);
        $('.column').find('.content:visible').fadeOut(200, function () {});
        $currentColumn.find('.text').slideUp(function () {});

    });

    /* зашли на шапку */
    $('.header').mouseenter(function () {

        $('.column').find('.content:visible').find('.text').slideUp(function () {
            $('.column').find('.content:visible').fadeOut(200);
            $('.column').find('.title').fadeIn(300);
            $('.column').switchClass("column-black", "column-default", 500, "easeInOutQuad");
        });

    });

    /* ушли со страницы */
    $(document).mouseleave(function () {
        $('.column').switchClass("column-black", "column-default", 400, "easeInOutQuad");
        $('.column').find('.content:visible').find('.text').slideUp(function () {
            $('.column').find('.content:visible').fadeOut(200);
            $('.column').find('.title').fadeIn(300);
        });
    });

});
