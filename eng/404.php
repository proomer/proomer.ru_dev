<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$zend = $APPLICATION->GetViewContent('ZEND_OUTPUT');
if ((!defined('ERROR_404') || ERROR_404 != 'Y') && !empty($zend)) {
    echo($zend);

} else {

    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");

    $APPLICATION->SetTitle("Страница не найдена");
    $APPLICATION->SetPageProperty('page-type', 'not-found');

    ?>
    <div class="bg-wrapper"></div>
    <div class="content-container">
        <h1>В комнате пусто ...</h1>
        <p>Вы попали на страницу, которой не существует,
            или зашли по неверному адресу.</p>
        <p>Не переживайте — попробуйте выбрать дизайн
            мечты в нашем <a href="<?= EZendManager::url([], 'design-list') ?>" class="under-link">каталоге проектов</a>,  где собраны
            более 3 000 вариантов для вашей квартиры. </p>
        <div class="image"></div>
        <div class="image-404"></div>
    </div>
    <?	
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>
