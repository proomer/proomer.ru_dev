<?

/**
 * я понимаю что это костыль, но намерено его вставляю <ivanscm>
 * это необходимо для показа заглушки неавторизованым пользователям
 * подключаем апи биткрикса для начала для опознания юзера, а затем пролог
 * http://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=2814
 * 
 * Особенностью служебной части пролога является то, что она не выводит никаких данных (не посылает header браузеру).
 * 
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;

if (TRUE /*$USER->IsAuthorized()*/)
{
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    $APPLICATION->SetPageProperty("keywords", "дизайн-проекты, онлайн, заказать дизайн-проект");
    $APPLICATION->SetPageProperty("description", "Онлайн-сервис готовых дизайнерских интерьерных решений.");
    $APPLICATION->SetPageProperty("title", "Proomer: онлайн-сервис готовых дизайн-проектов");
    $APPLICATION->SetTitle("Proomer");
    $APPLICATION->SetPageProperty('page-type', 'main');

    echo $APPLICATION->GetViewContent('ZEND_OUTPUT');

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
}
else
{
    include($_SERVER["DOCUMENT_ROOT"] . "/_cap/index.php");
}
?>
