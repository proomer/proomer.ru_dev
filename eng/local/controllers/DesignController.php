<?

/**
 * Class DesignController
 */
class DesignController extends Sibirix_Controller {
    /**
     * @var Sibirix_Model_Design
     */
    protected $_model;

    public function init() {
        /**
         * @var $ajaxContext Sibirix_Controller_Action_Helper_SibirixAjaxContext
         */
        $ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('design-list', 'html')
            ->initContext();

        $this->_model = new Sibirix_Model_Design();
    }

    public function designListAction() {

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'design-list');
        Zend_Registry::get('BX_APPLICATION')->SetTitle("Готовые дизайны для новостроек");

        $mainSliderModel = new Sibirix_Model_MainSlider();
        $slides = $mainSliderModel->getItems();
        $this->view->slides = $slides;

        if ($this->getParam("viewCounter") > 0) {
            Sibirix_Model_ViewCounter::setViewCounter($this->getParam("viewCounter"));
            $this->_model->reinitViewCounter();
        }
		
        $sortData = $this->getParam("sort");
        if (!empty($sortData)) {
            if (!empty($sortData["popular"])) {
                $catalogSort["PROPERTY_LIKE_CNT"] = $sortData["popular"];
            }
            if (!empty($sortData["date"])) {
                $catalogSort["DATE_CREATE"] = $sortData["date"];
            }
            if (!empty($sortData["price"])) {
                $catalogSort["catalog_PRICE_" . BASE_PRICE] = $sortData["price"];
            }
            if (!empty($sortData["budget"])) {
                $catalogSort["PROPERTY_BUDGET"] = $sortData["budget"];
            }
			
        }
		
        $catalogSort["SORT"] = "ASC";
		
        $filter = new Sibirix_Form_FilterDesign();

        $filterParams = $this->getAllParams();


        if ($filterParams["priceMin"] === null || $filterParams["priceMax"] === null) {
          //  $filterParams["price"] = $this->_model->getExtremePrice();
        } else {
            $filterParams["price"] = array(
                $filterParams["priceMin"],
                $filterParams["priceMax"]
            );
        }

        if ($filterParams["budgetMin"] === null || $filterParams["budgetMax"] === null) {
            $filterParams["budget"] = $this->_model->getExtremeBudget();
        } else {
            $filterParams["budget"] = array(
                $filterParams["budgetMin"],
                $filterParams["budgetMax"]
            );
        }

        $filter->populate($filterParams);
        $validFilterValues = $filter->getValues();
        $catalogFilter = $this->_model->prepareFilter($validFilterValues);
        $pageTitle     = $this->_model->getPageTitle($validFilterValues);
		
		
        $result = $this->_model->getDesignList($catalogFilter, $catalogSort, $this->getParam("page", 1));
		
		foreach($result->items as $design){
			if (!$design->PROPERTY_IMG_3_VALUE) {
				$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
			} else {
				$period = 32/8;
				$gallery3d = array();
				for ($i=1; $i < 8+1; $i++) {
					$cur_index = $i * $period;
					$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
				}
				$design->GALLERY3D = $gallery3d;
			}						
		};
		
        $this->view->assign([
            "pageTitle" => $pageTitle,
            "filter"    => $filter,
            "itemList"  => $result->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
		
    }

    public function detailAction() {

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'design-detail');

        $elementCode = $this->getParam("elementCode");
        $design = $this->_model->where(["CODE" => $elementCode])->getElement();

        if (!$design || $design->PROPERTY_STATUS_ENUM_ID != DESIGN_STATUS_PUBLISHED) {
            throw new Zend_Exception('Not found', 404);
        }
        $this->_model->getImageData($design, ["DETAIL_PICTURE", "PROPERTY_PLAN_FLAT_VALUE", "PROPERTY_ESTIMATE_VALUE", "PROPERTY_DOCUMENTS_VALUE"]);
        $this->_model->getSeoElementParams($design);

        $modelUser = new Sibirix_Model_User();
        $design->DESIGNER = $modelUser->where(["ID" => $design->PROPERTY_CREATED_BY_VALUE])->getElement();
        $modelRoom = new Sibirix_Model_Room();
		$modelPin = new Sibirix_Model_Pin();
        $basketModel = new Sibirix_Model_Basket();
        $basketItems = $basketModel->getBasketProductsId();
		
        $design->IS_IN_BASKET = (!empty($basketItems))? in_array($design->ID, $basketItems) : false;
        $design->ROOMS = $modelRoom->where(['PROPERTY_DESIGN' => $design->ID])->getElements();
		
	
        $modelRoom->getImageData($design->ROOMS, 'PROPERTY_IMAGES_VALUE');
	
		//Получаем пины
		foreach($design->ROOMS as $room){
			$room->PINS = $modelPin->getPin($room->ID);
			foreach($room->PINS as $pin){
				//x = 488px - 100%; y = 493px - 100%;
				$coords = explode(",", $pin->PROPERTY_COORDS_VALUE[0]);

				$x = (($coords[0] - 19) / 488) * 100;
				$y = (($coords[1] - 19) / 493) * 100;
				$pin->X = $x;
				$pin->Y = $y;
			}
		}
	
        $list = [];
        if (Sibirix_Model_User::isAuthorized()) {
            $hh = Highload::instance(HL_LIKES)->cache(0);
            $list = $hh->where(['UF_USER_ID' => Sibirix_Model_User::getId(), 'UF_DESIGN' => $design->ID])->fetch();
        }
        $design->IS_LIKED = (!empty($list)) ? true : false;
		
        //Похожие дизайны - получаем до переопределения PROPERTY_STYLE_VALUE и PROPERTY_PRIMARY_COLOR_VALUE
        $filter = [
            "!ID" => $design->ID,
            "PROPERTY_PLAN" => $design->PROPERTY_PLAN,
            "PROPERTY_STATUS" => DESIGN_STATUS_PUBLISHED,
            [
                "LOGIC" => "OR",
                ["PROPERTY_STYLE" => $design->PROPERTY_STYLE],
                ["PROPERTY_PRIMARY_COLOR" => $design->PROPERTY_PRIMARY_COLOR],
            ],
        ];
		
        $limit = Settings::getOption('similarDesignSliderCount', DEFAULT_SLIDES_COUNT);
        $similarDesigns = $this->_model->getRandElements('similarDesign'.$design->ID, [], $filter, $limit, '_getDesignInfo', $this->_model);

        //Получаем характеристики
        $styleList = Sibirix_Model_Reference::getReference(HL_STYLE, array("UF_NAME"), "UF_XML_ID");

        $propertyStylesValue = array();
        foreach ($design->PROPERTY_STYLE_VALUE as $key => $stylesValue) {
            $propertyStylesValue[$key] = $styleList[$stylesValue];
        }
        $design->PROPERTY_STYLE_VALUE = $propertyStylesValue;

        //Получаем цвета
        $colorList = Sibirix_Model_Reference::getReference(HL_PRIMARY_COLORS, array("UF_HEX"), "UF_XML_ID");

        $propertyColorsValue = array();
        foreach ($design->PROPERTY_PRIMARY_COLOR_VALUE as $key => $colorsValue) {
            $propertyColorsValue[$key] = $colorList[$colorsValue];
        }
        $design->PROPERTY_PRIMARY_COLOR_VALUE = $propertyColorsValue;

        $this->_setSeoElementParams($design);
        $this->view->design = $design;
        $this->view->similarDesigns = $similarDesigns;
    }
	
    /**
     * Добавление дизайна
     */
    public function editAction() {

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'profile-inner-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'design-edit');

        if (!Sibirix_Model_User::isAuthorized() || Sibirix_Model_User::getCurrent()->getType() != DESIGNER_TYPE_ID) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('edit', 'html')
		->initContext();
		
		//4984
        $designId = $this->getParam("designId");
        Zend_Registry::get('BX_APPLICATION')->SetTitle(($designId > 0?"Редактирование проекта" : "Добавление проекта"));
		
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$houseModel = new Sibirix_Model_House();

        if ($designId > 0) {
	
            $designData = $this->_model->select(['ID', 
												'NAME',
												'CODE',
												'CREATED_BY',
												'PROPERTY_CREATED_BY',
												'PREVIEW_TEXT', 
												'DETAIL_TEXT', 
												'DETAIL_PICTURE', 
												'PROPERTY_PLAN', 
												'PROPERTY_DESIGN_ROOM', 
												'PROPERTY_AREA', 
												'PROPERTY_STYLE', 
												'PROPERTY_PRIMARY_COLOR',
												'PROPERTY_STATUS',
												'PROPERTY_BUDGET',
												'PROPERTY_DOCUMENTS',
												'PROPERTY_STAGE'], true)->getElement($designId);
			
            if ($designData->PROPERTY_STATUS_ENUM_ID ==  EnumUtils::getListIdByXmlId(IB_DESIGN, "STATUS", "moderation")) {
                //LocalRedirect($this->view->url([],"profile-design"));
            }
            if (!$this->_model->checkDesignAccess($designId, $designData->PROPERTY_CREATED_BY_VALUE)) {
                //Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
                //throw new Zend_Exception('Not found', 404);
            }
        }
		
        /**
         * Шаг 1 планировки, планировочные решения
        */
		//поиск планировок по адресу
		$street = $this->getParam('street');			
		if($street){
			$filter["PROPERTY_STREET"] = $street;
			$number = $this->getParam('number');
			if($number){$filter["PROPERTY_HOUSE_NUMBER"] = $number;};
		}

		//1)Находим дом
		if(!empty($street)){

			$house = $houseModel->select(['ID', 'NAME', 'PROPERTY_COMPLEX'], true)->where($filter, true)->getElement();		
			//2)Находим квартиры
			$ARR_FLAT = $houseModel->getFlatListb($house->ID);
			
			//ids планировок
			$planIds = array_map(function ($obj) {
						return $obj->PROPERTY_PLAN;
						}, $ARR_FLAT);
						
			$planIds = array_unique($planIds);
			
			$catalogSort["SORT"] = "ASC";	
			$filterParams = $this->getAllParams();
			$catalogFilter = $planModel->prepareFilter($filterParams);
			$catalogFilter['=ID'] = $planIds;
			
			$PLAN_ARR = $planModel
			->select(["ID", "PROPERTY_ROOM", "PROPERTY_IMAGES", "PROPERTY_AREA"], true)
			->where($catalogFilter)
			->getElements();
	
			$plan_id = (int) $this->getParam('plan');
			$option_id = (int) $this->getParam('option');
						
			if($plan_id){	
				foreach($PLAN_ARR as $res){
					if($res->ID == $plan_id){
						$PLAN_OPTION_ARR = $planoptionModel
						->select(["ID", "PROPERTY_ROOM", "PROPERTY_IMAGES", "PROPERTY_AREA"], true)
						->where(['=PROPERTY_PLAN_FLAT'=>$plan_id])
						->getElements();
						break;
					};
				};

			};	
		}
	
        /**
         * Шаг 2 дизайн-проекты комнат 
        */

		$designModel = new Sibirix_Model_Design();
		$designroomModel = new Sibirix_Model_DesignRoom();
		$roomModel = new Sibirix_Model_Room();
		
		if($designId > 0){

			$designDesignIds = array_unique($designData->PROPERTY_DESIGN_ROOM_VALUE);
			$designData->PRICE = array_shift($designModel->getPrice($designData));
		
			if(count($designDesignIds) > 0){
				$filterDesignDesign['=ID'] = $designDesignIds;
			}
			else{
				$filterDesignDesign['=ID'] = 0;
			}
			
			$designDesigns = $designroomModel->select(['ID', 'NAME', 'DETAIL_TEXT', 'CREATED_BY', 'PROPERTY_CREATED_BY',
													'PROPERTY_STYLE',
													'PROPERTY_PRIMARY_COLOR',
													'PROPERTY_DESIGN_ROOM',
													'PROPERTY_DOCUMENTS',
													'PROPERTY_PRICE_ELECTRONICS',
													'PROPERTY_PRICE_MATERIALS',
													'PROPERTY_PRICE_PLUMBING',
													'PROPERTY_PRICE_LIGHT',
													'PROPERTY_PRICE_FURNITURE',
													'PROPERTY_PRICE_SQUARE',
													'PROPERTY_AREA',
													'PROPERTY_IMAGES',
													'PROPERTY_TYPE'], true)->where($filterDesignDesign)->getElements();
								
			$designDesignIds = array_map(function ($obj) {
						return $obj->ID;
						}, $designDesigns);
						
			$designDesignIds = array_unique($designDesignIds);
			
			$filterDesignDesign;
			if(count($designDesignIds) > 0){
				$filterRoom['=PROPERTY_DESIGN'] = $designDesignIds;
			}
			else{
				$filterRoom['=ID'] = 0;
			}
			
			$ROOMS = $roomModel->select(['ID', 'NAME',
													'PROPERTY_DESIGN'
													//'PROPERTY_STYLE',
													/*'PROPERTY_PRICE_MATERIALS',
													'PROPERTY_PRICE_ELECTRONICS',
													'PROPERTY_PRICE_PLUMBING',
													'PROPERTY_PRICE_LIGHT',
													'PROPERTY_PRICE_FURNITURE',
													'PROPERTY_PRICE_SQUARE',
													'PROPERTY_DESIGN',
													'PROPERTY_AREA',
													//'PROPERTY_IMAGES'*/
													], true)->where($filterRoom)->getElements();
			
			foreach($ROOMS as $roomItem){
				$roomItem->PRICE = array_shift($roomModel->getPrice($roomItem));
			}

			foreach($designDesigns as $design){
				$design->ROOM = array();
				
				foreach($ROOMS as $room){
					if($room->PROPERTY_DESIGN_VALUE == $design->ID){
						$design->ROOM = $room;
					};
				};

				$photos = array();
						
				foreach($design->PROPERTY_IMAGES_VALUE as $photo){
					array_push($photos, array(
					"imageSrc" => Resizer::resizeImage($photo, 'PROMO_PHOTO'),
					"valueId" => $photo
					));				
				}
				$design->PHOTOS = $photos;
				
				$design->PROPERTY_DOCUMENTS_VALUE = CFile::GetByID($design->PROPERTY_DOCUMENTS_VALUE)->Fetch();
			};

			if($designData->DETAIL_PICTURE){
				$designData->DETAIL_PICTURE = Resizer::resizeImage($designData->DETAIL_PICTURE, 'DROPZONE_MAIN_PHOTO');
			};
			if($designData->PROPERTY_DOCUMENTS_VALUE){
				$designData->PROPERTY_DOCUMENTS_VALUE = CFile::GetByID($designData->PROPERTY_DOCUMENTS_VALUE)->Fetch();
			};
		}
		
		$designroomModel->_getDesignInfo($designDesigns);

        $this->view->assign([
            "pageTitle" => $pageTitle,
			"designDesigns" => $designDesigns,
			"PLAN_ARR"  => $PLAN_ARR,
			"PLAN_OPTION_ARR"  => $PLAN_OPTION_ARR,
			"plan_id"	=> $plan_id,
			"option_id"	=> $option_id,
			"street"    => $street,
			"number"	=> $number,
			"designId"	=> $designId,
			"roomList"	=> $ROOMS,
			"designData"=> $designData
            //'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);		
    }
		
	/**
     * Сохранение первого шага
     */
    public function addSaveStepFirstAction() {
		/*
			Прицип работы:
			Принимается id планировочного решения IB_PLAN_OPTION и id дизайна IB_DESIGN
			Если id дизайна > 0, то обновляем у него свойство PROPERTY_PLAN, иначе создается новый дизайн
			со статусом "черновик" и записывается в свойство PROPERTY_PLAN id планировочного решения
		*/
		$designModel = new Sibirix_Model_Design();
		
		//принимаем всё
		$stepValues = $this->getAllParams();
		$designId = (int) $stepValues["designId"];
		
		if($designId > 0){
			$designData = $designModel->select(['ID', 'PROPERTY_STAGE'], true)->getElement($designId);
		};
		
        $planId = false;
        if ($stepValues["option"] > 0) {
			//id планировочного решения
            $planId = $stepValues["option"];

        }

        if ($stepValues["designId"] < 0) {
            if (!$designModel->checkDesignAccess($stepValues["designId"])) {
                Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
                throw new Zend_Exception('Not found', 404);
            }
        }
		
	
        $fields = array(
            "ID"              => $stepValues["designId"],
            "PROPERTY_VALUES" => array(
                "PLAN" => $planId
            )
        );
		
		if($designData->PROPERTY_STAGE_VALUE > 1){
			$fields['PROPERTY_VALUES']['STAGE'] = $designData->PROPERTY_STAGE_VALUE;
		}
		else{
			$fields['PROPERTY_VALUES']['STAGE'] = 2;
		};
		
        $planModel = new Sibirix_Model_Plan();
        $plan = $planModel->getElement($planId);
        $stepValue = "Дизайн для планировки: ".$plan->NAME;
        $editResult = $designModel->editDesign($fields);
		if(!$designId){
			$designId = $editResult;
		}
        $this->_helper->json(['result' => (bool)$editResult, 'newId' => (is_numeric($designId) ? $designId : ""), "stepValue" => $stepValue]);
    }
	
    /**
     * Сохранение второго шага
    */
    public function addSaveStepSecondAction() {
			
		$stepValues = $this->getAllParams();
		
		$fields_design = array();
		$fields_room   = array();
		
		$mainDesignId = (int) $this->getParam('mainDesignId');
		$designId = (int) $stepValues['designId'];
		
		$roomModel   		= new Sibirix_Model_Room();
		$designModel 	= new Sibirix_Model_Design();
		$designRoomModel 	= new Sibirix_Model_DesignRoom();
						
		$hlTypeRoomReference 	= Highload::instance(HL_TYPEROOM);
		$typeRoomHighloadList  = $hlTypeRoomReference->select(array_merge(["ID"], array("UF_NAME", "UF_XML_ID", "UF_NAME_CASE_1")), true)->fetch();

		foreach($typeRoomHighloadList as $type){
			if($type['UF_XML_ID'] == $stepValues['type']){
				$type_room = $type['UF_NAME_CASE_1'];
				break;
			};
		};

		if($stepValues['roomId']){$fields_room['ID'] = (int) $stepValues['roomId'];};
		if($type_room){$fields_design['NAME'] = 'Дизайн-проект '.$type_room;};	
		if($stepValues['area']){$fields_design['PROPERTY_VALUES']['AREA'] = (int) $stepValues['area'];};	
		if($stepValues['priceSquare']){$fields_design['PROPERTY_VALUES']['PRICE_SQUARE'] = (int) $stepValues['priceSquare'];};	
		if($stepValues['priceFurniture']){$fields_design['PROPERTY_VALUES']['PRICE_FURNITURE'] = (int) $stepValues['priceFurniture'];};	
		if($stepValues['pricePlumbing']){$fields_design['PROPERTY_VALUES']['PRICE_PLUMBING'] 	= (int) $stepValues['pricePlumbing'];};
		if($stepValues['priceLight']){$fields_design['PROPERTY_VALUES']['PRICE_LIGHT'] = (int) $stepValues['priceLight'];};
		if($stepValues['priceElectronics']){$fields_design['PROPERTY_VALUES']['PRICE_ELECTRONICS'] = (int) $stepValues['priceElectronics'];};
		if($stepValues['priceMaterials']){$fields_design['PROPERTY_VALUES']['PRICE_MATERIALS'] = (int) $stepValues['priceMaterials'];};
		if($stepValues['type']){$fields_design['PROPERTY_VALUES']['TYPE'] = $stepValues['type'];};
		if($stepValues['designId']){$fields_design['ID'] = (int) $stepValues['designId'];};
		if($stepValues['detailDesignDescription']){$fields_design['DETAIL_TEXT'] = $stepValues['detailDesignDescription'];};
		if($stepValues['style']){$fields_design['PROPERTY_VALUES']['STYLE'] = $stepValues['style'];};
		if($stepValues['color']){$fields_design['PROPERTY_VALUES']['PRIMARY_COLOR'] = $stepValues['color'];};
		
		$photoIds = explode(',', $stepValues['photoRoom']);
		$photoData = array();
		
		foreach($photoIds as $photo){
			$rsPhoto = CFile::MakeFileArray(CFile::GetPath($photo));
			array_push($photoData, $rsPhoto);
		}
		
		if($photoData){
			$fields_design['PROPERTY_VALUES']['IMAGES'] = $photoData;
		}

		$fields_design['PROPERTY_VALUES']['STATUS'] = DESIGN_ROOM_STATUS_DRAFT;
				
		$fields_design["PRICE_VALUE"] = array(
											"PRODUCT_ID"       => $designId,
											"CATALOG_GROUP_ID" => BASE_PRICE,
											"PRICE"            => $fields_design['PROPERTY_VALUES']['AREA'] * $fields_design['PROPERTY_VALUES']['PRICE_SQUARE'],
											"CURRENCY"         => "RUB"
										);
	
		$editDesign = $designRoomModel->editDesign($fields_design);

		$designId = $editDesign;

		if($designId){
			$fields_room['PROPERTY_VALUES']['DESIGN'] = $designId; 
			$editRoom = $roomModel->editRoom($fields_room);			
		}
		
		//получим дизайн IB_DESIGN
		$MAIN_DESIGN = $designModel->select(['ID', 'PROPERTY_DESIGN_ROOM', 'PROPERTY_STAGE'],true)->getElement($mainDesignId);
		
		//цена элемента IB_DESIGN
		$main_total_price = 0;
		//получим массив ids элементов дизайн комнат IB_DESIGN_ROOM
		$arr_design_room = $MAIN_DESIGN->PROPERTY_DESIGN_ROOM;
		
		$fields_main_design["ID"] = $mainDesignId;
		
		$flag = 1;
		foreach($arr_design_room as $id_design){
			if($id_design == $designId){
				$flag = 0;
			}
		}
		if($flag == 1){
			array_push($arr_design_room, $designId);
		};		
		$fields_main_design["PROPERTY_VALUES"]["DESIGN_ROOM"] = $arr_design_room;
		
		$designModel->editDesign($fields_main_design);
		$MAIN_DESIGN = $designModel->select(['ID', 'PROPERTY_DESIGN_ROOM', 'PROPERTY_STAGE'],true)->getElement($mainDesignId);
		foreach($MAIN_DESIGN->PROPERTY_DESIGN_ROOM_VALUE as $design_room){
			$main_total_price += CPrice::GetBasePrice($design_room)['PRICE'];
		}
	
		$fields_main_design["PRICE_VALUE"] = array(
			"PRODUCT_ID"       => $mainDesignId,
			"CATALOG_GROUP_ID" => BASE_PRICE,
			"PRICE"            => $main_total_price,
			"CURRENCY"         => "RUB"
		);

		if($MAIN_DESIGN->PROPERTY_STAGE_VALUE > 2){
			$fields_main_design['PROPERTY_VALUES']['STAGE'] = $MAIN_DESIGN->PROPERTY_STAGE_VALUE;
		}
		else{
			$fields_main_design['PROPERTY_VALUES']['STAGE'] = 3;
		};
		
		$designModel->editDesign($fields_main_design);

		$this->_helper->json(["result" => (bool)$editDesign, "newId" => $editDesign, "roomId" => $editRoom, "stepValue" => $stepValue]);
    
	}
	
	public function addSaveStepSecondSecondAction() {
		
		$roomModel 	 = new Sibirix_Model_Room();
		$designModel = new Sibirix_Model_Design();
		
		$stepValues = $this->getAllParams();
		
		//получим дизайн IB_DESIGN
		$MAIN_DESIGN = $designModel->select(['ID', 'PROPERTY_DESIGN_ROOM', 'PROPERTY_STAGE'],true)->getElement((int) $stepValues["designId"]);
		
		$fields_room = array();
		
		$fields_design = array(
			"ID"	=> (int) $stepValues["designId"],
			"NAME"	=> $stepValues["designName"],
			"DETAIL_TEXT" => $stepValues["detailDesignDescription"],
			"PROPERTY_VALUES" => array(
					"PRICE_SQUARE" 		=> $stepValues["priceSquare"],
					"PRICE_FURNITURE"	=> $stepValues["priceFurniture"],
					"PRICE_LIGHT"		=> $stepValues["priceLight"],
					"PRICE_PLUMBING"	=> $stepValues["pricePlumbing"],
					"PRICE_ELECTRONICS"	=> $stepValues["priceElectronics"],
					"PRICE_MATERIALS"	=> $stepValues["priceMaterials"]
				)
		);
		
		if($MAIN_DESIGN->PROPERTY_STAGE_VALUE > 3){
			$fields_design['PROPERTY_VALUES']['STAGE'] = $MAIN_DESIGN->PROPERTY_STAGE_VALUE;
		}
		else{
			$fields_design['PROPERTY_VALUES']['STAGE'] = 4;
		};

		$editDesign = $designModel->editDesign($fields_design);
		
		/*$fields_room = array();
		for($i = 0; $i < count($stepValues["roomId"]); $i++){
			$fields_room = array(
				"ID"	=> $stepValues[$i]["roomId"],
				"PROPERTY_VALUES" => array(
					"PRICE_SQUARE" 		=> $stepValues[$i]["priceSquare"],
					"PRICE_FURNITURE"	=> $stepValues[$i]["priceFurniture"],
					"PRICE_LIGHT"		=> $stepValues[$i]["priceLight"],
					"PRICE_PLUMBING"	=> $stepValues[$i]["pricePlumbing"],
					"PRICE_ELECTRONICS"	=> $stepValues[$i]["priceElectronics"]
				)
			);
			$roomModel->editRoom($fields_room);
		}*/
		$this->_helper->json(["result" => (bool)$editDesign, "newId" => $editDesign]);
	}
	
	public function addSaveStepThirdAction() {

		$designModel = new Sibirix_Model_Design();
		
		$designId = $this->getParam('designId');

		//получим дизайн IB_DESIGN
		$MAIN_DESIGN = $designModel->select(['ID', 'PROPERTY_DESIGN_ROOM', 'PROPERTY_STAGE'],true)->getElement((int) $stepValues["designId"]);
		
		$fields = array(
			"ID"	=> $designId,
			"PROPERTY_VALUES" => array(
				"STATUS" => DESIGN_STATUS_MODERATION
			)
		);
		
		if($MAIN_DESIGN->PROPERTY_STAGE_VALUE == 4){
			$fields['PROPERTY_VALUES']['STAGE'] = $MAIN_DESIGN->PROPERTY_STAGE_VALUE;
		}
		else{
			$fields['PROPERTY_VALUES']['STAGE'] = 4;
		};
		
		$result = $designModel->editDesign($fields);
		
		$this->_helper->json(['result' => (bool)$result]);	
		
	}
	
	public function getStepSecondAction() {
		$designId = (int) $this->getParam("designId");
		$designModel = new Sibirix_Model_Design();
		$designroomModel = new Sibirix_Model_DesignRoom();
		$roomModel = new Sibirix_Model_Room();
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('get-step-second', 'html')
			->initContext();
		
		$designDesign = $designModel->select(['ID', 'NAME', 'PROPERTY_DESIGN_ROOM'], true)->where(['=ID'=>$designId])->getElement();

		$designDesignIds = array_unique($designDesign->PROPERTY_DESIGN_ROOM_VALUE);
		$filterDesignDesign;
		if(count($designDesignIds) > 0){
			$filterDesignDesign['=ID'] = $designDesignIds;
		}
		else{
			$filterDesignDesign['=ID'] = 0;
		}

		$designDesigns = $designroomModel->select(['ID', 'NAME', 'DETAIL_TEXT',
												'PROPERTY_STYLE',
												'PROPERTY_TYPE',
												'PROPERTY_PRIMARY_COLOR',
												'PROPERTY_DESIGN_ROOM',
												'PROPERTY_PRICE_MATERIALS',
												'PROPERTY_PRICE_ELECTRONICS',
												'PROPERTY_PRICE_PLUMBING',
												'PROPERTY_PRICE_LIGHT',
												'PROPERTY_PRICE_FURNITURE',
												'PROPERTY_PRICE_SQUARE',
												'PROPERTY_AREA',
												'PROPERTY_IMAGES'], true)->where($filterDesignDesign)->getElements();

		$designDesignIds = array_map(function ($obj) {
					return $obj->ID;
					}, $designDesigns);
		$designDesignIds = array_unique($designDesignIds);
		
		$filterDesignDesign;
		if(count($designDesignIds) > 0){
			$filterRoom['=PROPERTY_DESIGN'] = $designDesignIds;
		}
		else{
			$filterRoom['=ID'] = 0;
		}
		
		$ROOMS = $roomModel->select(['ID', 'NAME',
											'PROPERTY_DESIGN',
											'PROPERTY_TYPE'
									], true)->where($filterRoom)->getElements();
		
		foreach($designDesigns as $design){
			$design->ROOM = array();
			foreach($ROOMS as $room){
				if($room->PROPERTY_DESIGN_VALUE == $design->ID){
					$photos = array();
					foreach($design->PROPERTY_IMAGES_VALUE as $photo){
						array_push($photos, array(
							"imageSrc" => Resizer::resizeImage($photo, 'PROMO_PHOTO'),
							"valueId" => $photo
						));				
					}
					$design->ROOM = $room;
					$design->PHOTOS = $photos;
				};
			};
		};

		$this->view->assign([
			"designDesigns" => $designDesigns
		]);	
		
		//$this->_helper->json(['result' => (bool)$designDesigns, 'designDesigns' => $designDesigns ? $designDesigns : array()]);											
	}
		
	public function getStepSecondSecondAction() {
		$designId = (int) $this->getParam("designId");
		$designModel = new Sibirix_Model_Design();
		$designroomModel = new Sibirix_Model_DesignRoom();
		$roomModel = new Sibirix_Model_Room();
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('get-step-second-second', 'html')
			->initContext();

		$designData = $designModel->select(['ID',
											'CODE',
											'CREATED_BY',
											'PROPERTY_CREATED_BY',
											'NAME',
											'DETAIL_PICTURE',
											'PREVIEW_TEXT',
											'DETAIL_TEXT',
											'PROPERTY_BUDGET',
											'PROPERTY_STATUS',
											'PROPERTY_DESIGN_ROOM'], 
									true)->getElement($designId);

		$designDesignIds = $designData->PROPERTY_DESIGN_ROOM_VALUE;
		$designData->PRICE = array_shift($designModel->getPrice($designData));
		if($designData->DETAIL_PICTURE){
			$designData->DETAIL_PICTURE = Resizer::resizeImage($designData->DETAIL_PICTURE, 'DROPZONE_MAIN_PHOTO');
		};
		$filterDesignDesign;
		if(count($designDesignIds) > 0){
			$filterDesignDesign['=ID'] = $designDesignIds;
		}
		else{
			$filterDesignDesign['=ID'] = 0;
		}
		$designDesigns = $designroomModel->select(['ID', 'NAME', 'DETAIL_TEXT',
												'PROPERTY_STYLE',
												'PROPERTY_PRIMARY_COLOR',
												'PROPERTY_DESIGN_ROOM',
												'CREATED_BY',
												'PROPERTY_CREATED_BY',
												'PROPERTY_STYLE',
												'PROPERTY_PRICE_MATERIALS',
												'PROPERTY_PRICE_ELECTRONICS',
												'PROPERTY_PRICE_PLUMBING',
												'PROPERTY_PRICE_LIGHT',
												'PROPERTY_PRICE_FURNITURE',
												'PROPERTY_PRICE_SQUARE',
												'PROPERTY_AREA',
												'PROPERTY_TYPE',
												'PROPERTY_IMAGES'
												], true)->where($filterDesignDesign)->getElements();
							
		$designDesignIds = array_map(function ($obj) {
					return $obj->ID;
					}, $designDesigns);
		$designDesignIds = array_unique($designDesignIds);

		$filterDesignDesign;
		if(count($designDesignIds) > 0){
			$filterRoom['=PROPERTY_DESIGN'] = $designDesignIds;
		}
		else{
			$filterRoom['=ID'] = 0;
		}
		
		$ROOMS = $roomModel->select(['ID', 'NAME', 'PROPERTY_DESIGN'], true)->where($filterRoom)->getElements();
		
		foreach($ROOMS as $roomItem){
			$roomItem->PRICE = array_shift($roomModel->getPrice($roomItem));
		}
		
		foreach($designDesigns as $design){
			$design->ROOM = array();
			foreach($ROOMS as $room){
				if($room->PROPERTY_DESIGN_VALUE == $design->ID){
					$design->ROOM = $room;
				};
			};
		};
		$designroomModel->_getDesignInfo($designDesigns);

		$this->view->assign([
			"designDesigns" => $designDesigns,
			"designId"	=> $designId,
			"roomList"	=> $ROOMS,
			"designData"=> $designData
		]);		
		
	}
	
	public function getStepThirdAction() {
		$designId = (int) $this->getParam("designId");
		$designModel = new Sibirix_Model_Design();
		$designroomModel = new Sibirix_Model_DesignRoom();
		$roomModel = new Sibirix_Model_Room();
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('get-step-third', 'html')
			->initContext();

		$designDesign = $designModel->select(['ID', 'NAME', 'PROPERTY_DESIGN_ROOM', 'PROPERTY_DOCUMENTS'], true)->getElement($designId);
		if($designDesign->PROPERTY_DOCUMENTS_VALUE){
			$designDesign->PROPERTY_DOCUMENTS_VALUE = CFile::GetByID($designDesign->PROPERTY_DOCUMENTS_VALUE)->Fetch();
		}
		$designDesignIds = array_unique($designDesign->PROPERTY_DESIGN_ROOM_VALUE);
		
		if(count($designDesignIds) > 0){
			$filterDesignDesign['=ID'] = $designDesignIds;
		}
		else{
			$filterDesignDesign['=ID'] = 0;
		}

		$designDesigns = $designroomModel->select(['ID', 'NAME', 'DETAIL_TEXT',
			'PROPERTY_STYLE',
			'PROPERTY_PRIMARY_COLOR',
			'PROPERTY_DESIGN_ROOM',
			'PROPERTY_DOCUMENTS'], true)->where($filterDesignDesign)->getElements();
		
		$filterDesignDesign;
		if(count($designDesignIds) > 0){
			$filterRoom['=PROPERTY_DESIGN'] = $designDesignIds;
		}
		else{
			$filterRoom['=ID'] = 0;
		}
		
		$ROOMS = $roomModel->select(['ID', 'NAME',
												'PROPERTY_STYLE',
												'PROPERTY_PRICE_MATERIALS',
												'PROPERTY_PRICE_ELECTRONICS',
												'PROPERTY_PRICE_PLUMBING',
												'PROPERTY_PRICE_LIGHT',
												'PROPERTY_PRICE_FURNITURE',
												'PROPERTY_PRICE_SQUARE',
												'PROPERTY_DESIGN',
												'PROPERTY_AREA',
												'PROPERTY_TYPE',
												'PROPERTY_IMAGES'
												], true)->where($filterRoom)->getElements();
		
		foreach($ROOMS as $roomItem){
			$roomItem->PRICE = array_shift($roomModel->getPrice($roomItem));
		}

		foreach($designDesigns as $design){
			$design->ROOM = array();
			foreach($ROOMS as $room){
				if($room->PROPERTY_DESIGN_VALUE == $design->ID){
					$photos = array();
					foreach($room->PROPERTY_IMAGES_VALUE as $photo){
						array_push($photos, array(
							"imageSrc" => Resizer::resizeImage($photo, 'PROMO_PHOTO'),
							"valueId" => $photo
						));				
					}
					$room->PHOTOS = $photos;
					$design->ROOM = $room;
				};
			};
			if($design->PROPERTY_DOCUMENTS_VALUE){
				$design->PROPERTY_DOCUMENTS_VALUE = CFile::GetByID($design->PROPERTY_DOCUMENTS_VALUE)->Fetch();
			}
		};

		$this->view->assign([
			"designDesigns" => $designDesigns,
			"designData"	=> $designDesign
		]);	
	
	}
	
	/**
     * Загрузка изображений с dropzone "налету"
    */
    public function uploadFileAction() {
		
		$upload   = new Zend_File_Transfer();
		
		$fileType 	= $this->getParam("fileType");
		$resizeType = $this->getParam('resizeType');
	
		//получим информацию о файле
		$file = $upload->getFileInfo()["file"];
		
		$arDataFile["name"]		 = $file['name'];
		$arDataFile["size"]		 = $file['size'];
		$arDataFile["tmp_name"]  = $file['tmp_name'];
		$arDataFile["MODULE_ID"] = 'iblock';
		$arDataFile['type']		 = $file['type'];
		
		//если файл загружен
		if (strlen($arDataFile["name"]) > 0){
			//регистрируем файл
			$fid = CFile::SaveFile($arDataFile, "iblock");
		}
		else{
			//посылаем
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
			throw new Zend_Exception('Not found', 404);
		}
		
        if ($this->getParam("files") == "true") {
            $response = array();
			$fileData = EHelper::getFileData($newFile->$fileType);
			$response = array(
				"valueId" => $fid,
				"fileType" => GetFileExtension($arDataFile["name"]),
				"fileName" => $arDataFile["name"],
				"fileSize" => round($arDataFile["size"]/1024) . " Кб",
			);
        }else {
			
            $response = array();
			if (empty($resizeType)) {
				$resizeType = "DROPZONE_MAIN_PHOTO";
			}

			$response = array(
				[
				"imageSrc" => Resizer::resizeImage($fid, $resizeType),
				"valueId" => $fid
				],
			);
        }

        $this->_helper->json(['result' => (bool)$fid, "response" => $response]);
    }
	
	public function uploadFilebAction() {
		$upload = new Zend_File_Transfer();
		$modelDesignRoom = new Sibirix_Model_DesignRoom();
        $designId = (int) $this->getParam("designId");
        $fileModel = new Sibirix_Model_Files();

        if (!$this->_model->checkDesignAccess($designId)) {
          //  Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
          //  throw new Zend_Exception('Not found', 404);
        }

        $fileType   = $this->getParam("fileType");
        $resizeType = $this->getParam("resizeType");

        $fields = array(
            "ID" => $designId,
            $fileType => $upload->getFileInfo()["file"]
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }

        $accessExtensions = "";
        switch ($fileType) {
            case "DETAIL_PICTURE":
            case "PROPERTY_PLAN_FLAT":
                $accessExtensions = "jpg,png,jpeg,gif,bmp";
                break;
            case "PROPERTY_DOCUMENTS":
                $accessExtensions = "doc,docx,xls,xlsx,pdf,rar,tar,zip";
                break;
            default:
                $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
                break;
        }

        if (!$fileModel->checkFile($upload->getFileInfo()["file"], $accessExtensions)) {
            $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
        }
	
        $editResult = $modelDesignRoom->editDesign($fields);
		if(!$designId){
			$designId = $editResult;
		}
        $newFile = false;
        if ($editResult) {
            $newFile = $modelDesignRoom->select([$fileType], true)->getElement($designId);
        }

        if ($this->getParam("files") == "true") {
            $response = array();
            if ($editResult) {
                $fileData = EHelper::getFileData($newFile->$fileType);
                $response = array(
					"designId" => $designId,
                    "valueId" => $newFile->$fileType,
                    "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                    "fileName" => $fileData["ORIGINAL_NAME"],
                    "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
                );
            }
        } else {
            $response = array();
            if ($editResult) {
                if (empty($resizeType)) {
                    $resizeType = "DROPZONE_MAIN_PHOTO";
                }
                $response["imageSrc"] =  Resizer::resizeImage($newFile->$fileType, $resizeType);
				$response["id"] = $designId;
            }

        }

        $this->_helper->json(['result' => (bool)$editResult, "response" => $response]);
	}
	
	public function uploadFilecAction() {

		$upload = new Zend_File_Transfer();
		$modelDesign = new Sibirix_Model_Design();
        $designId = (int) $this->getParam("designId");
        $fileModel = new Sibirix_Model_Files();

        if (!$this->_model->checkDesignAccess($designId)) {
          //  Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
          //  throw new Zend_Exception('Not found', 404);
        }

        $fileType   = $this->getParam("fileType");
        $resizeType = $this->getParam("resizeType");

        $fields = array(
            "ID" => $designId,
            $fileType => $upload->getFileInfo()["file"]
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }

        $accessExtensions = "";
        switch ($fileType) {
            case "DETAIL_PICTURE":
            case "PROPERTY_PLAN_FLAT":
                $accessExtensions = "jpg,png,jpeg,gif,bmp";
                break;
            case "PROPERTY_DOCUMENTS":
                $accessExtensions = "doc,docx,xls,xlsx,pdf,rar,tar,zip";
                break;
            default:
                $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
                break;
        }
		
        if (!$fileModel->checkFile($upload->getFileInfo()["file"], $accessExtensions)) {
            $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
        }

        $editResult = $modelDesign->editDesign($fields);

		if(!$designId){
			$designId = $editResult;
		}
		
        $newFile = false;
		
        if ($editResult) {
            $newFile = $modelDesign->select([$fileType], true)->getElement($designId);
        }

        if ($this->getParam("files") == "true") {
            $response = array();
            if ($editResult) {
                $fileData = EHelper::getFileData($newFile->$fileType);
                $response = array(
					"designId" => $designId,
                    "valueId" => $newFile->$fileType,
                    "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                    "fileName" => $fileData["ORIGINAL_NAME"],
                    "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
                );
            }
        } else {
            $response = array();
            if ($editResult) {
                if (empty($resizeType)) {
                    $resizeType = "DROPZONE_MAIN_PHOTO";
                }
                $response["imageSrc"] =  Resizer::resizeImage($newFile->DETAIL_PICTURE, $resizeType);
				$response["id"] = $designId;
            }
			
        }

        $this->_helper->json(['result' => (bool)$editResult, "response" => $response]);
	}
		
    /**
     * Удаляет файл
    */
    public function deleteFileAction() {
        $designId = $this->getParam("designId");

        if (!$this->_model->checkDesignAccess($designId)) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }
        $fileType   = $this->getParam("fileType");

        $fields = array(
            "ID" => $designId,
            $fileType => array("del" => "Y")
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }
        $editResult = $this->_model->editDesign($fields);

        $this->_helper->json(['result' => (bool)$editResult]);
    }
	
	public function deleteRoomFileAction() {
        $designId = $this->getParam("designId");
		$model = new Sibirix_Model_DesignRoom();
		
        $fileType   = $this->getParam("fileType");

        $fields = array(
            "ID" => $designId,
            $fileType => array("del" => "Y")
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }

        $editResult = $model->editDesign($fields);

        $this->_helper->json(['result' => (bool)$editResult]);
    }
	
    public function publishDesignAction() {
        $designId = $this->getParam("designId");

        if (!$this->_model->checkDesignAccess($designId)) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $fields = array(
            "ID"              => $designId,
            "PROPERTY_VALUES" => array(
                "STATUS" => EnumUtils::getListIdByXmlId(IB_DESIGN, "STATUS", "moderation")
            )
        );
        $editResult = $this->_model->editDesign($fields);

        $this->_helper->json(['result' => (bool)$editResult]);
    }

    public function deleteAction() {
        $designId = $this->getParam("designId", 0);

        if (!$this->_model->checkDesignAccess($designId) || !check_bitrix_sessid()) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $result = $this->_model->update($designId, ['PROPERTY_VALUES' => ['STATUS' => DESIGN_STATUS_DELETED]]);
        $this->_helper->json(['success' => true, 'result' => $result]);
    }

    public function likeAddAction() {
        if (!check_bitrix_sessid() || !Sibirix_Model_User::isAuthorized()) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }
	
        $likeModel = new Sibirix_Model_Like();
        $itemId = (int) $this->getParam("itemId");
		
        $likeModel->add($itemId);
		
        $count = $this->_model->cacheLikes($itemId);

        $this->_helper->json(['success' => true, 'likeCnt' => $count]);
    }

    public function likeRemoveAction() {
        if (!check_bitrix_sessid() || !Sibirix_Model_User::isAuthorized()) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $likeModel = new Sibirix_Model_Like();
        $itemId = (int) $this->getParam("itemId");
        $likeModel->remove($itemId);
        $count = $this->_model->cacheLikes($itemId);

        $this->_helper->json(['success' => true, 'likeCnt' => $count]);
    }

    /**
     * Загрузка плана
     */
    public function addPlanAction() {
        $upload = new Zend_File_Transfer();
        $designId = $this->getParam("designId");
        $fileModel = new Sibirix_Model_Files();

        if (!$this->_model->getElement($designId)->PROPERTY_CREATED_BY_VALUE == Sibirix_Model_User::getId()) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $imageFile = $upload->getFileInfo()["file"];

        if (!$fileModel->checkFile($upload->getFileInfo()["file"], "jpg,png,jpeg,gif,bmp")) {
            $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
        }
        $resultImages = $this->_model->addPlan($designId, $imageFile);

        $this->_helper->json(['result' => true, 'response' => $resultImages]);
    }

    /**
     * Удаление изображения плана
     */
    public function deletePlanAction() {
        $designId = $this->getParam("designId");
        $imageId = $this->getParam("imageId");

        if (!$this->_model->getElement($designId)->PROPERTY_CREATED_BY_VALUE == Sibirix_Model_User::getId()) {
            Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
        }

        $resultImages = $this->_model->deletePlan($designId, $imageId);

        $this->_helper->json(['result' => true, 'response' => $resultImages]);
    }
	
	public function getImgsAction() {
        $ibType = $this->getParam("type");
		$indx = $this->getParam("indx");
        /*switch ($ibType) {
            case "complex":
                $model = new Sibirix_Model_Complex();
                $property = "PROPERTY_COMPLEX_PLAN";
                break;
            case "floor":
                $model = new Sibirix_Model_Floor();
                $property = "PROPERTY_FLOOR_PLAN";
                break;
			case "room":
                $model_room = new Sibirix_Model_Room();
				$model_pin = new Sibirix_Model_Pin();
                $property = "PROPERTY_IMAGES";
			  // $property = "PROPERTY_ROOM_PLAN";
                break;
        }*/

		$model_room = new Sibirix_Model_Room();
		$model_pin = new Sibirix_Model_Pin();
		$property = "PROPERTY_IMAGES";
        if (empty($model_room) || empty($model_pin) || empty($property)) {
            return false;
        }

        $elementId = $this->getParam("id");

        if (is_numeric($elementId) && $elementId > 0) {
            $item = $model_room->select(["ID", $property], true)->getElement($this->getParam("id"));
			//Получаем пины
			$item->PINS = $model_pin->getPin($item->ID);
			foreach($item->PINS as $pin){
				//x = 488px - 100%; y = 493px - 100%;
				$coords = explode(",", $pin->PROPERTY_COORDS_VALUE[$indx]);
				if($coords[0] > 19 && $coords[1] > 19){
					$x = $coords[0] - 19;
					$y = $coords[1] - 19;
				}
				else{
					$x = $coords[0];
					$y = $coords[1];
				};
				$pin->X = $x;
				$pin->Y = $y;
			}
        }
	
        if (!empty($item)){
            $model_room->getImageData($item, $property."_VALUE");
            $propertyValue = $property . "_VALUE";
			$this->view->imageUrl = Resizer::resizeImage($item->PROPERTY_IMAGES_VALUE[$indx], 'SEARCH_SERVICE_PLAN');
			$this->view->item = $item;
        }
        $this->_response->stopBitrix(true);
	
    }
	
	public function addPinAction() {
		//ini_set('error_reporting', E_ALL);
		//ini_set('display_errors', 1);
		//ini_set('display_startup_errors', 1);
        $coords = $this->getParam("coords");
		$name = $this->getParam("name");
		$url = $this->getParam("url");
		$roomId = $this->getParam("roomId");
		$indx = $this->getParam("indx");
		$data = array();
		$data['NAME'] = $name;
		$data['PROPERTY_VALUES']['URL'] = $url;
		$data['PROPERTY_VALUES']['ROOM'] = $roomId;
		$data['PROPERTY_VALUES']['COORDS'][$indx] = $coords;
		$model = new Sibirix_Model_Pin();
		$arr_pins = $model->getPins(["=NAME" => $name, "=PROPERTY_ROOMS" => $roomId]);
		
		if(count($arr_pins) == 1){
			if(!$arr_pins[0]->PROPERTY_COORDS_VALUE[$indx] || $arr_pins[0]->PROPERTY_COORDS_VALUE[$indx] == '0, 0'){
				$data['ID'] = $arr_pins[0]->ID;
				$data['PROPERTY_VALUES']['COORDS'] = $arr_pins[0]->PROPERTY_COORDS_VALUE;
				$data['PROPERTY_VALUES']['COORDS'][$indx] = $coords;
				$model->updatePin($data);
			}
		}
		else{
			if(empty($name)){
				$data['NAME'] = 'new_pin_'.time();
			};
		
			for($i = 0; $i < $indx; $i++){
				if(!$data['PROPERTY_VALUES']['COORDS'][$i]){
					$data['PROPERTY_VALUES']['COORDS'][$i] = '0, 0';
				};
			}
	
			$data['PROPERTY_VALUES']['COORDS'][$indx] = $coords;
			sort($data['PROPERTY_VALUES']['COORDS']);
			$model->addPin($data, $indx);
		}

	

		/*$indx = $this->getParam("indx");
		
        switch ($ibType) {
            case "complex":
                $model = new Sibirix_Model_Complex();
                $property = "PROPERTY_COMPLEX_PLAN";
                break;
            case "floor":
                $model = new Sibirix_Model_Floor();
                $property = "PROPERTY_FLOOR_PLAN";
                break;
			case "room":
                $model = new Sibirix_Model_Room();
                $property = "PROPERTY_IMAGES";
			  // $property = "PROPERTY_ROOM_PLAN";
                break;
        }

        if (empty($model) || empty($property)) {
            return false;
        }

        $elementId = $this->getParam("id");
        if (is_numeric($elementId) && $elementId > 0) {
            $item = $model->select(["ID", $property], true)->getElement($this->getParam("id"));
        }

        if (!empty($item)) {
            $model->getImageData($item, $property."_VALUE");
            $propertyValue = $property . "_VALUE";
			
			$this->view->imageUrl = Resizer::resizeImage($item->PROPERTY_IMAGES_VALUE[$indx], 'SEARCH_SERVICE_PLAN');
        }*/
		$this->view->pinCoords = $coords;
        $this->_response->stopBitrix(true);
	
    }
	
	public function delPinAction() {
        $pinId = $this->getParam("pinId");
		$model = new Sibirix_Model_Pin();
		$result = $model->delPin($pinId);
		$this->_helper->json(['result' => true, 'response' => $result]);
    }
	
	public function changePosPinAction() {
        $pinId = $this->getParam("pinId");
		$pos = $this->getParam("pos");
		$indxs = $this->getParam("indx");
		$model = new Sibirix_Model_Pin();
		$data = array();
		$data['ID'] = $pinId;
		$data['PROPERTY_VALUES']['COORDS'][$indxs] = $pos;
		$result = $model->updatePin($data);
		$this->_helper->json(['result' => true, 'response' => $result]);
    }
	
	public function editbAction() {
        $designIds = json_decode($this->getParam("designIds"));
		$modelDesign = new Sibirix_Model_Design();
		$modelDesignRoom = new Sibirix_Model_DesignRoom();
		$modelRoom = new Sibirix_Model_Room();
		$fields['ID'] = $this->getParam('designId');
		$fields['PROPERTY_VALUES']['DESIGN_ROOM'] = $designIds;

		$total_price_main_design = 0;
		foreach($fields['PROPERTY_VALUES']['DESIGN_ROOM'] as $item){
			$design->ID = $item;
			$price_design_room = array_shift($modelDesignRoom->getPrice($design));
			$total_price_main_design+=$price_design_room['PRICE'];
		}

		$fields["PRICE_VALUE"] = array(
				"PRODUCT_ID"       => $fields['ID'],
				"CATALOG_GROUP_ID" => BASE_PRICE,
				"PRICE"            => $total_price_main_design,
				"CURRENCY"         => "RUB"
		);

		$result = $modelDesign->editDesign($fields);
		
		$design = $modelDesign->select(['ID', 'PROPERTY_DESIGN_ROOM'], true)->getElement($result);
				
		$rooms = $modelRoom->select(['ID', 'PROPERTY_DESIGN_ROOM', 
									'PROPERTY_AREA', 
									'PROPERTY_PRICE_SQUARE', 
									'PROPERTY_PRICE_FURNITURE',
									'PROPERTY_PRICE_LIGHT',
									'PROPERTY_PRICE_PLUMBING',
									'PROPERTY_PRICE_ELECTRONICS',
									'PROPERTY_PRICE_MATERIALS'], true)->where(['=PROPERTY_DESIGN' => $design->PROPERTY_DESIGN_ROOM_VALUE])->getElements();
		

		$this->_helper->json(['result' => (bool)$result, 'newId' => (is_numeric($result) ? $result : ""), "stepValue" => $stepValue]);
		//$this->_helper->json(['result' => true, 'response' => $result]);
    }
	
	public function deleteRoomAction() {
		$designRoomId = (int) $this->getParam('designRoomId');
		$designId = (int) $this->getParam('designId');
		
		$modelDesign = new Sibirix_Model_Design();
		$modelDesignRoom = new Sibirix_Model_DesignRoom();
		
		if($designRoomId > 0){
			$modelDesignRoom->remove($designRoomId);
			$success = true;
		}
		else{
			$success = false;
		}
		$MAIN_DESIGN = $modelDesign->select(['ID', 'PROPERTY_DESIGN_ROOM'],true)->getElement($mainDesignId);
		$main_total_price = 0;
		foreach($MAIN_DESIGN->PROPERTY_DESIGN_ROOM_VALUE as $design_room){
			$main_total_price += CPrice::GetBasePrice($design_room)['PRICE'];
		};
	
		$fields_main_design["PRICE_VALUE"] = array(
			"PRODUCT_ID"       => $mainDesignId,
			"CATALOG_GROUP_ID" => BASE_PRICE,
			"PRICE"            => $main_total_price,
			"CURRENCY"         => "RUB"
		);
		
		if(!$modelDesign->editDesign($fields_main_design)){$success = false;};
		$this->_helper->json(['success' => $success]);
	}
}