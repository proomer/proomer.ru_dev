<?

/**
 * Class FeedbackController
 */
class ProjectController extends Sibirix_Controller {

    /**
     * @var Sibirix_Model_Feedback
     */
    protected $_model;

    public function init() {
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('index', 'html')
		->initContext();

        $this->_model  = new Sibirix_Model_Plan();
    }
	
	public function indexAction() {
		
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'project-page index');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'project');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Выбор планировки"));
		
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$complexModel = new Sibirix_Model_Complex();
		$houseModel = new Sibirix_Model_House();
		$flatModel = new Sibirix_Model_Flat();
		$entranceModel = new Sibirix_Model_Entrance();
		
		$street = $this->getParam('street');
		
		if(!$street){
			$this->redirect('/');
		}
		
		$filter["PROPERTY_STREET"] = $street;
		$number = $this->getParam('number');
		if($number){
			$filter["PROPERTY_HOUSE_NUMBER"] = $number;
		}
		//1)Находим дом
		if(!empty($street)){
			$house = $houseModel->select(['ID', 'NAME', 'PROPERTY_COMPLEX'], true)->where($filter, true)->getElement();
		}
		//1)Находим комплекс
		if($house){
			$complex = $complexModel->select(['ID', 'NAME'], true)->where(["ID" => $house->PROPERTY_COMPLEX])->getElement();
		}
		//2)Находим подъезды
		
		//3)Находим этажи
		
		//4)Находим квартиры
		$ARR_FLAT = $houseModel->getFlatListb($house->ID);
		$planIds = array_map(function ($obj) {
					return $obj->PROPERTY_PLAN;
					}, $ARR_FLAT);
		$planIds = array_unique($planIds);
		/*plans = $planModel
		->select(["ID", "PROPERTY_ROOM", "PROPERTY_IMAGES", "PROPERTY_AREA"], true)
		->where(["ID" => $planIds])
		->getElements();*/
		
		$designModel = new Sibirix_Model_Design();
        $catalogSort["SORT"] = "ASC";	
        $filter = new Sibirix_Form_FilterDesign();	
        $filterParams = $this->getAllParams();
        $catalogFilter = $this->_model->prepareFilter($filterParams);
		$catalogFilter['=ID'] = $planIds;
		//$catalogFilter['=PROPERTY_SHOW_STEP_1_VALUE'] = 'Y';
        $pageTitle     = $this->_model->getPageTitle($validFilterValues);
		if($this->getParam('page')){
			$limit = $this->getParam('page') * 11;
		}
		else{
			$limit = 11;
		}
		if($house){
			$result = $planModel->getPlanList($catalogFilter, $catalogSort, 1, false, $limit);
		};
		$option_id = $this->getParam('options');
		$designs_id = $this->getParam('designs');
		
		$resultOptions = array();
		
		if($option_id){
			foreach($result->items as $res){
				if($res->ID == $option_id){
					$resultOptions = $planoptionModel->getPlanList(['=PROPERTY_PLAN_FLAT'=>$option_id], $catalogSort, 1, false, 999);
				};
			};
		};
	
		if($designs_id){
			foreach($resultOptions->items as $res){
				if($res->ID == $designs_id){
					$resultDesigns = $designModel->getDesignbList(null, ['=PROPERTY_PLAN'=>$designs_id], $catalogSort, 1, false, 999);
					foreach($resultDesigns->items as $design){

						if (!$design->PROPERTY_IMG_3_VALUE) {
							$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
						} else {
							$period = 32/8;
							$gallery3d = array();
							for ($i=1; $i < 8+1; $i++) {
								$cur_index = $i * $period;
								$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
							}
							$design->GALLERY3D = $gallery3d;
						}						
					};
				};
			};
		};

		$designModel->_getDesignInfo($resultDesigns->items);
	
        $this->view->assign([
            "pageTitle" => $pageTitle,
            "filter"    => $filter,
            "itemList"  => $result->items,
			"complex"	=> $complex,
			"itemListPlan"  => $resultOptions->items,
			"options"	=> $option_id,
			"itemListDesign"  => $resultDesigns->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
    }
		
	public function step1Action(){
		$APP = Zend_Registry::get('BX_APPLICATION');
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'create-project-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'project');
		$pageTitle = Zend_Registry::get('BX_APPLICATION')->SetTitle(("Выбор квартиры"));
		
		/*=============================================
			Определим контекст для AJAX
		=============================================*/
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('step1', 'html')
		->initContext();
		/*==========================================*/
		
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$flatModel = new Sibirix_Model_Flat();
		$projectroomModel = new Sibirix_Model_ProjectRoom();
		$projectapartModel = new Sibirix_Model_ProjectApart();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
		$projectfamilyModel = new Sibirix_Model_ProjectFamily();
		$complexModel = new Sibirix_Model_Complex();
		$houseModel = new Sibirix_Model_House();
		$flatModel = new Sibirix_Model_Flat();
		$entranceModel = new Sibirix_Model_Entrance();
		
		//изначально доступ к следующему шагу закрыт
		$access = false;
		$limit = 8;
		//в зависимости от page показываем кол-во планировок(если page = 1, то показываем 8 планировок)	
		if(!empty($this->getParam('page'))){
			$page = (int) $this->getParam('page');
			if($page == 0){
				Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
				throw new Zend_Exception('Not found', 404);
			}
			else{
				if($page){$limit = $page * 8;}
				else{$limit = 8;};
			};
		}
		
		if(!empty($this->getParam('id_flat'))){
			$id_flat = (int) $this->getParam('id_flat');
			if($id_flat <= 0){
				Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
				throw new Zend_Exception('Not found', 404);
			};
		}
				
		/*=============================================
			АВТОРИЗОВАН?
		=============================================*/
		if(Sibirix_Model_User::isAuthorized()){
			$auth = true;
			$id_user = (int) $userModel->getId();
			/*====================================
				Берем заказ(если есть)
			====================================*/
			$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user], [], 1, false, 1);
			$count = $PROJECT_ARR->pageData->totalItemsCount;
			if($count > 0){
				//если ранее 1 шаг был пройден, то откроем доступ к следующему шагу
				if($PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE > 1){
					$access = true;
				};
				$item = $PROJECT_ARR->items[0];	
				
				$id_flat = $item->PROPERTY_ID_FLAT_VALUE;
				if($this->getParam('selected_plan')){
					$id_flat = $this->getParam('selected_plan');
				}
					
				//получим выбранную планировку
				if($item->PROPERTY_ID_FLAT_VALUE){
					$PLAN_SAVE = $planModel->getPlanList(['=ID'=> (int) $id_flat], [], 1, false, 1);
				};
				
				//получим вариант планировки
				if($item->PROPERTY_ID_OPTION_PLAN_VALUE){
					$PLAN_OPTION_SAVE = $planoptionModel->getPlanList(['=ID'=> (int) $item->PROPERTY_ID_OPTION_PLAN_VALUE], [], 1, false, 1);
				};
				//получим путь картинки
				foreach($PLAN_SAVE->items as $plan){
					if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
						$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
					} else {
						$image = '/local/images/proomer2.png';
					}
					$plan->PLAN_IMAGE = $image;
				}
				//получим путь картинки
				foreach($PLAN_OPTION_SAVE->items as $plan){
					if (!empty($plan->PROPERTY_IMAGES_VALUE)){
						$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
					} else {
						$image = '/local/images/proomer2.png';
					}
					$plan->PLAN_IMAGE = $image;
				}
				/*============================================================
					Получим данные о заказах данного проекта
				============================================================*/			
				if($item->PROPERTY_TYPE_ORDER_ENUM_ID == PROJECT_TYPE_ORDER_ROOM){
					$ORDER_PROJECT_LIST = $projectroomModel->getProjectRoomList(['=PROPERTY_ID_ORDER'=> $item->ID], [], 1, false);	
				}
				else if($item->PROPERTY_TYPE_ORDER_ENUM_ID == PROJECT_TYPE_ORDER_APARTMENT){
					$ORDER_PROJECT_LIST = $projectapartModel->getProjectApartList(['=PROPERTY_ID_ORDER'=> $item->ID], [], 1, false);
				};
				
				/*============================================================
					Получим состав семьи
				*===========================================================*/	
				$family_people = array();
				$family_people_child = array();
				$family_animal = array();
				foreach($ORDER_PROJECT_LIST->items as $order){
					array_push($family_people, $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER'=> $order->PROPERTY_PEOPLE_VALUE, 
																						  '=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_GROWN], [], 1, false));
					array_push($family_people_child, $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER'=> $order->PROPERTY_PEOPLE_CHILDREN_VALUE,
																								'=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_CHILDREN], [], 1, false));
					array_push($family_animal, $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER'=> $order->PROPERTY_ANIMAL_VALUE,
																						  '=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_ANIMAL
																						], [], 1, false));
				}
	
				//Получаем характеристики
				$animalList = Sibirix_Model_Reference::getReference(HL_ANIMAL, array("UF_NAME"), "UF_XML_ID");
		
				$propertyStylesValue = array();
				foreach($family_animal as $items){
					foreach($items->items as $item){
						$propertyAnimalsValue = $animalList[$item->PROPERTY_ANIMAL_VALUE];	
						$item->PROPERTY_ANIMAL_VALUE = $propertyAnimalsValue;
					};
				};
			
				$PROJECT_FAMILY = array(
					"PEOPLE"		=> $family_people,
					"PEOPLE_CHILD"	=> $family_people_child,
					"ANIMAL"		=> $family_animal
				);
				
			}
			else{
				if($this->getParam('id_flat')){
					$id_flat = (int) $this->getParam('id_flat');
				}		
				//получим выбранную планировку
				if($id_flat > 0){
					$PLAN_SAVE = $planModel->getPlanList(['=ID'=> (int) $id_flat], [], 1, false, 1);
				};		
				//получим путь картинки
				foreach($PLAN_SAVE->items as $plan){
					if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
						$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
					} else {
						$image = '/local/images/proomer2.png';
					}
					$plan->PLAN_IMAGE = $image;
				}
			}
		}
		else{
			if($this->getParam('id_flat')){
				$id_flat = (int) $this->getParam('id_flat');
			}		
			//получим выбранную планировку
			if($id_flat > 0){
				$PLAN_SAVE = $planModel->getPlanList(['=ID'=> (int) $id_flat], [], 1, false, 1);
			};		
			//получим путь картинки
			foreach($PLAN_SAVE->items as $plan){
				if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
					$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
				} else {
					$image = '/local/images/proomer2.png';
				}
				$plan->PLAN_IMAGE = $image;
			}
			$auth = false;
		};
		
		$catalogSort["SORT"] = "ASC";		
		$catalogFilter = [];
		
		$street = $this->getParam('street');
		
		if(!$street){
			
		}
		
		$filter["PROPERTY_STREET"] = $street;
		$number = $this->getParam('number');
		if($number){
			$filter["PROPERTY_HOUSE_NUMBER"] = $number;
		}
		//1)Находим дом
		if(!empty($street)){
			$house = $houseModel->select(['ID', 'NAME', 'PROPERTY_COMPLEX'], true)->where($filter, true)->getElement();
		}
		//1)Находим комплекс
		if($house){
			$complex = $complexModel->select(['ID', 'NAME'], true)->where(["ID" => $house->PROPERTY_COMPLEX])->getElement();
		}
		//2)Находим подъезды
		
		//3)Находим этажи
		
		//4)Находим квартиры
		$FLAT_ARR = $houseModel->getFlatListb($house->ID);
	
		$planIds = array_map(function ($obj) {
					return $obj->PROPERTY_PLAN;
					}, $FLAT_ARR);
		$planIds = array_unique($planIds);
	
		//оставим только уникальные id квартир
		$planIds = array_unique($planIds);
		//это планировки квартир
		
		if(count($planIds) > 0){
			$planIds = false;
			$filter = ['=ID' => $planIds];
		}
		else{$filter = [];};

		$PLAN_ARR = $planModel->getPlanList($filter,[], 1, false, $limit);
		
		//получим путь для картинок
		foreach($PLAN_ARR->items as $plan){			
			if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
				$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
			} else {
				$image = '/local/images/proomer2.png';
			}
			
			$plan->PLAN_IMAGE = $image;
		
		}
		//массив с параметрами для следующего шага
		$this->view->assign([
            "pageTitle" 		=> $pageTitle,
			"show_plan"			=> "N",
            "itemList"  		=> $PLAN_ARR->items,
			"projectData" 		=> $PROJECT_ARR->items[0],
			"url"				=> "/project/step2",
			"step"				=> "step1",
			"nextstep"			=> "savestep1",
			"nextstep_d"		=> "К параметрам <br/> дизайн-проекта",
			"id_flat"			=> $id_flat,
			"auth"				=> $auth,
			"access"			=> $access,
			"PLAN_SAVE"			=> $PLAN_SAVE->items,
			"PLAN_OPTION_SAVE"	=> $PLAN_OPTION_SAVE->items,
			"PROJECT_FAMILY" 	=> $PROJECT_FAMILY
        ]);
    }
	
	public function savestep1Action(){
		/*========================================
			Сохранение
			Принцип работы: Если пользователь не авторизован, то храним в куку первый и второй шаг,
			далее он должен будет авторизоваться и первый, второй шаг переносим в бд, следущие шаги также сохраняются в бд
			Если авторизован просто храним в бд
		=========================================*/
		$planModel = new Sibirix_Model_Plan();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
		$id_flat = $this->getParam('id_flat');
		$id_plan = $this->getParam('id_plan');
		/*==========================================
			Авторизован?
		=========================================*/
		$catalogSort = ['ASC'];
		if(Sibirix_Model_User::isAuthorized()){
			//Проверка есть ли в куках уже добавленный заказ	
			//.................??????????................
			$id_user = $userModel->getId();
			//получаем планировку выбранную на первом шаге
			$result = $planModel->getPlanList(['=ID' => $id_flat], [], 1, false, 1);
			$item = $result->items[0];
	
			$fields = array();
			$fields["NAME"] = PATTERN_ORDER_NAME .time();
			$fields["PROPERTY_VALUES"]["USER"] = $id_user;
			$fields["PROPERTY_VALUES"]["ID_OPTION_PLAN"] = false;
			$fields["PROPERTY_VALUES"]["STATUS"] = PROJECT_STATUS_ADDED;
			$fields["PROPERTY_VALUES"]["ID_FLAT"] = $id_flat;
			$fields["PROPERTY_VALUES"]["ID_OPTION_PLAN"] = $id_plan;
			
			//проверим есть ли уже добавленные заказы
			$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user], $catalogSort, 1, false, 1);
			$count = $PROJECT_ARR->pageData->totalItemsCount;
			if($count == 0){
				//адд
				$fields["PROPERTY_VALUES"]["STEP"] = 2;
				if($projectModel -> addProject($fields)){
				
				};
			}
			else{
				//упдате
				$fields['ID'] = $PROJECT_ARR->items[0]->ID;
				$step = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
				if(empty($step) || $step < 2){
					$fields["PROPERTY_VALUES"]["STEP"] = 2;
				}
				else{
					$fields["PROPERTY_VALUES"]["STEP"] = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
				};
				//CIBlockElement::SetPropertyValuesEx($result_project->items[0]->ID, IB_ORDER_PROJECT, $fields['PROPERTY_VALUES']);
				$projectModel -> editProject($fields);
			};
		}
		else{
			$this->_helper->json(['result' => false, "response" => false]);
		};
		$this->_helper->json(['result' => 1, "response" => true]);
	}
	
	public function step2Action(){
		
		$APP = Zend_Registry::get('BX_APPLICATION');	
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'create-project-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'project');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Параметры дизайн-проекта"));
		
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$projectModel = new Sibirix_Model_Project();
		$projectroomModel = new Sibirix_Model_ProjectRoom();
		$projectapartModel = new Sibirix_Model_ProjectApart();
		$projectfamilyModel = new Sibirix_Model_ProjectFamily();
		$roomModel = new Sibirix_Model_Room();
		$userModel = new Sibirix_Model_User();
		
		/*=============================================
			АВТОРИЗОВАН?
		=============================================*/
		if(Sibirix_Model_User::isAuthorized()){
			$auth = true;
			$id_user = $userModel->getId();
			/*==========================================
				Узнаем есть ли у пользователя уже заказ
				Если есть берем
			=========================================*/
			$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user], [], 1, false, 1);
			//если не пройден 1 шаг посылаем отсюда
			if($PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE < 2){
				$this->redirect('/project/step1/');
			}
			else if($PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE >= 2){
				$access = true;
			}
			$count = $PROJECT_ARR->pageData->totalItemsCount;
			if($count > 0){
				$item = $PROJECT_ARR->items[0];
				//id планировки
				$id_flat = $item->PROPERTY_ID_FLAT_VALUE;
				//id планировки(вариант)
				$id_plan = $item->PROPERTY_ID_OPTION_PLAN_VALUE;	
				$PLAN_SAVE = $planModel->getPlanList(['=ID'=>$id_flat], [], 1, false, 1);
				if($id_plan){
					$PLAN_OPTION_SAVE = $planoptionModel->getPlanList(['=ID'=>$id_plan], [], 1, false, 1);
				};
				//получии путь к картинке
				foreach($PLAN_SAVE->items as $plan){
					if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
						$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'COMPLEX_LIST');
					} else {
						$image = '/local/images/proomer2.png';
					}
					$plan->PLAN_IMAGE = $image;
				}
				//получии путь к картинке
				foreach($PLAN_OPTION_SAVE->items as $plan){
					if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
						$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'COMPLEX_LIST');
					} else {
						$image = '/local/images/proomer2.png';
					}
					$plan->PLAN_IMAGE = $image;
				}
				/*============================================
					Узнаем цену проекта
				============================================*/
				$PRICE_PROJECT += CPrice::GetBasePrice($PROJECT_ARR->items[0]->ID)['PRICE'];
			};
		}
		else{
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
		};
		
		$filterParams = $this->getAllParams();	
		// $catalogFilter = $this->_model->prepareFilter($filterParams);
		$catalogFilter = ['=PROPERTY_SHOW_STEP' => false, '=PROPERTY_SHOW_STEP_1_VALUE' => 'Y'];
		$pageTitle = $this->_model->getPageTitle($validFilterValues);
		$id_item = $this->getParam('id_item');
	
		$roomIds = array_map(function ($obj) {
			return $obj->PROPERTY_ROOM_PLAN_VALUE;
		}, $PLAN_OPTION_SAVE->items);
		$roomIds = array_unique($roomIds);
		
		if($roomIds){
			$ARR_ROOM = $roomModel->getRoomList(['=ID' => $roomIds[0]], [], 1, false);		
			foreach($ARR_ROOM->items as $room){
				$room->PRICE_ROOM = ($room->PROPERTY_PRICE_SQUARE_VALUE * $room->PROPERTY_AREA_VALUE) + ($room->PROPERTY_PRICE_SQUARE_VALUE * $room->PROPERTY_AREA_VALUE) / 100 * COMISSION_PERCENT;
			};
			$access = true;	
		}
		else{
			$access = false;
		}

		$roomIds = array_map(function ($obj) {
		   return $obj->PROPERTY_ROOM_ORDER_VALUE;
		}, $PROJECT_ARR->items);
		$roomIds = array_unique($roomIds);
	
		$ARR_PROJECT_ROOM = $projectroomModel->getProjectRoomList(['=ID' => $roomIds], [], 1, false);
		$ARR_PROJECT_APARTMENT = $projectapartModel->getProjectApartList(['=ID' => $roomIds], [], 1, false);	
		
		foreach($ARR_PROJECT_ROOM->items as $room){
			$room->PRICE_ROOM = (PRICE_SQUARE * $room->PROPERTY_AREA_VALUE) + (PRICE_SQUARE * $room->PROPERTY_AREA_VALUE) / 100 * COMISSION_PERCENT;
					
			$resanimal = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $room->ID,																								   
																    '=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY],[], 1);
			
			$respeoplechild = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $room->ID,
																		 '=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_CHILDREN],[], 1);

			$respeople = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $room->ID,
																	'=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_GROWN],[], 1);
		
			$room->FAMILY = array(
				'PEOPLE' 			=> $respeople,
				'PEOPLE_CHILDREN' 	=> $respeoplechild,
				'ANIMAL' 			=> $resanimal
			);	
			
		};
		
		foreach($ARR_PROJECT_APARTMENT->items as $room){
			$room->PRICE_ROOM = (PRICE_SQUARE * $room->PROPERTY_AREA_VALUE) + (PRICE_SQUARE * $room->PROPERTY_AREA_VALUE) / 100 * COMISSION_PERCENT;
					
			$resanimal = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $room->ID,																								   
																    '=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY],[], 1);
			
			$respeoplechild = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $room->ID,
																		 '=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_CHILDREN],[], 1);

			$respeople = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $room->ID,
																	'=PROPERTY_TYPE'=> PROJECT_TYPE_FAMILY_GROWN],[], 1);
		
			$room->FAMILY = array(
				'PEOPLE' 			=> $respeople,
				'PEOPLE_CHILDREN' 	=> $respeoplechild,
				'ANIMAL' 			=> $resanimal
			);	
		};
		
		$this->view->assign([
			"listDesign" 			=> $arItems,
            "pageTitle" 			=> $pageTitle,
			"show_list_plan"		=> 'N',
			"show_list_family"		=> 'N',
			"projectData" 			=> $PROJECT_ARR->items[0],
			"url"					=> '/project/step3/',
			"step"					=> 'step2',
			"nextstep"				=> 'savestep2',
			'access' 				=> $access,
			'auth'					=> $auth,
			"nextstep_d"			=> 'К оформлению',
			"PLAN_SAVE"				=> $PLAN_SAVE->items,
			"PLAN_OPTION_SAVE"		=> $PLAN_OPTION_SAVE->items,
			"ARR_ROOM"				=> $ARR_ROOM->items,
			"ARR_PROJECT_ROOM"		=> $ARR_PROJECT_ROOM->items,
			"ARR_PROJECT_APARTMENT"	=> $ARR_PROJECT_APARTMENT->items,
			"PRICE_PROJECT"			=> $PRICE_PROJECT
        ]);
    }
	
	public function savestep2Action(){
		$planModel = new Sibirix_Model_Plan();
		$projectModel = new Sibirix_Model_Project();
		$projectroomModel = new Sibirix_Model_ProjectRoom();
		$projectapartModel = new Sibirix_Model_ProjectApart();
		$projectfamilyModel = new Sibirix_Model_ProjectFamily();
		$roomModel = new Sibirix_Model_Room();
		$userModel = new Sibirix_Model_User();
		$defaultValues = $this->getAllParams();

		//если авторизован	
		if(Sibirix_Model_User::isAuthorized()){
			
			$id_user = $userModel->getId();
			//значения из формы
			$fields = array();
			$fields["NAME"] = PATTERN_ORDER_NAME .time();
			$fields["ID"] = $defaultValues['id_room'];
			$fields["PROPERTY_VALUES"]["TYPE_ROOM"] = $defaultValues['type_room'];
			$fields["PROPERTY_VALUES"]["TIME"] = $defaultValues['slider_time'];
			$fields["PROPERTY_VALUES"]["SUGGEST"] = $defaultValues['comments'];
			$fields["PROPERTY_VALUES"]["AREA"] = $defaultValues['area'];
			$fields["PROPERTY_VALUES"]["PRICE_SQUARE"] = $defaultValues['price_project'];
			$fields["PROPERTY_VALUES"]["DESIGN_LIKED"] = $defaultValues['liked_design'];
			$fields["PROPERTY_VALUES"]["TYPE_ORDER"] = $defaultValues['type_room'];
			?><pre><?echo print_r($defaultValues)?></pre><?
			exit;
			$ID_ROOM_IB_ROOM = (int) $defaultValues['id_room_ib_room'];
			//массив с заказом
			$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user], [], 1, false, 1);
			$fields["PROPERTY_VALUES"]["ID_ORDER"] = $PROJECT_ARR->items[0]->ID;
			//проверим есть ли уже заказ
			$count = $PROJECT_ARR->pageData->totalItemsCount;
			if($count == 0){
				Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
                throw new Zend_Exception('Not found', 404);
			}
			else{			
				//если заказ на комнату
				if($defaultValues['type_order'] == TYPE_ORDER_ROOM){
					//получим комнаты/квартиры заказа 
					$ARR_PROJECT_ROOM = $projectroomModel->getProjectRoomList(["=PROPERTY_ID_ORDER"=>$PROJECT_ARR->items[0]->ID], [], 1, false);
					$roomIds = array_map(function($obj){
						return $obj->ID;
					},$ARR_PROJECT_ROOM->items);
					//если ранее был сделан заказ на квартиру, то удалим ее
					if(!empty($PROJECT_ARR->items[0]->PROPERTY_TYPE_ORDER_VALUE) && $PROJECT_ARR->items[0]->PROPERTY_TYPE_ORDER_VALUE != PROJECT_TYPE_ORDER_ROOM){
						$apartIds = array_map(function ($obj) {
						return $obj->PROPERTY_ROOM_ORDER_VALUE;
						}, $PROJECT_ARR->items);
						$apartIds = array_unique($apartIds);
						foreach($apartIds as $id){
							$result = $projectapartModel->deleteApart($id[0]);
						};					
					}
					//если ранее был сделан заказ на эту комнату		
					if($fields['ID'] > 0){	
			
						if(in_array($fields['ID'], $roomIds)){
							
							$ARR_PROJECT_FAMILY = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $fields['ID']], [], 1, false);
							$familyIds = array_map(function ($obj) {
								return $obj->ID;
							}, $ARR_PROJECT_FAMILY->items);
							$familyIds = array_unique($familyIds);
						
							if(count($familyIds) > 0){
								foreach($familyIds as $id){																
									$projectfamilyModel->deleteFamily($id);
								};
							};
								
							//обновим комнату
							$result = $projectroomModel->editRoomProject($fields);
							
							//семья комнаты
							if($result && (int) $fields['ID'] > 0 && $ID_ROOM_IB_ROOM > 0){
				
								/*=============================================*/
								
								foreach($defaultValues['people'] as $people){
									$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
									$gender = false;
									if($people["gender"] == "man"){
										$gender = 95;
									}
									else if($people["gender"] == "female"){
										$gender = 96;
									}
									$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $fields["ID"];
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people["age"];
									$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_GROWN;
									$projectfamilyModel->editProjectFamily($fieldsfamily);
								}
								
								/*===========================================*/
								
								$fieldsfamily = array();
								foreach($defaultValues['people_children'] as $people_children){
									$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
									$gender = false;
									if($people_children["gender"] == "man"){
										$gender = 95;
									}
									else if($people_children["gender"] == "female"){
										$gender = 96;
									}
									$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $fields["ID"];
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people_children["age"];
									$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_CHILDREN;
									$projectfamilyModel->editProjectFamily($fieldsfamily);
								}
								
								/*=======================================*/
								
								$fieldsfamily = array();
								foreach($defaultValues['animal'] as $animal){
									$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
									$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 	= $fields['ID'];
									$fieldsfamily['PROPERTY_VALUES']['ANIMAL'] 		= $animal;
									$fieldsfamily['PROPERTY_VALUES']['TYPE'] 		= PROJECT_TYPE_FAMILY_ANIMAL;
									$projectfamilyModel->editProjectFamily($fieldsfamily);		
								}
																						
								//цена комнаты
								$ROOM = $roomModel->select(['PROPERTY_AREA'], true)->getElement($ID_ROOM_IB_ROOM);
								$area = $ROOM->PROPERTY_AREA_VALUE;
								$price_square = PRICE_SQUARE;
								if((int) $defaultValues['price_project'] > 0){
									$price_square = $defaultValues['price_project'];
								}								
								$PRICE_ORDER = ($price_square * $area) + ($price_square * $area) / 100 * COMISSION_PERCENT;
									
								$PRICE_PROJECT = $PRICE_ORDER;

								//Установление цены для товара
								$PRICE_TYPE_ID = 1;
								
								$PRICE_PROJECT = $PRICE_ORDER;

								$arFields = array(
									"PRODUCT_ID" => $fields['ID'],
									"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
									"PRICE" => $PRICE_PROJECT,
									"CURRENCY" => "RUB",
									"QUANTITY_FROM" => 1,
									"QUANTITY_TO" => 1
								);

								$res = CPrice::GetList(
									array(),
									array(
										"PRODUCT_ID" => $fields['ID'],
										"CATALOG_GROUP_ID" => $PRICE_TYPE_ID
									)
								);
							
								if ($arr = $res->Fetch())
								{
									CPrice::SetBasePrice($fields['ID'], $PRICE_PROJECT, 'RUB');						
								}
								else
								{
									CPrice::Add($arFields);
								}
							};
						}
					}
					else{
				
						$result = $projectroomModel->editRoomProject($fields);
						
						if((int) $result > 0 && $ID_ROOM_IB_ROOM > 0){
							//семья комнаты							
							/*=============================================*/
							
							foreach($defaultValues['people'] as $people){
								$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
								$gender = false;
								if($people["gender"] == "man"){
									$gender = 95;
								}
								else if($people["gender"] == "female"){
									$gender = 96;
								}
								$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $fields["ID"];
								$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
								$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people["age"];
								$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $result;
								$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_GROWN;
								$projectfamilyModel->editProjectFamily($fieldsfamily);
							}
							
							/*===========================================*/
							
							$fieldsfamily = array();
							foreach($defaultValues['people_children'] as $people_children){
								$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
								$gender = false;
								if($people_children["gender"] == "man"){
									$gender = 95;
								}
								else if($people_children["gender"] == "female"){
									$gender = 96;
								}
								$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $fields["ID"];
								$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
								$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people_children["age"];
								$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $result;
								$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_CHILDREN;
								$projectfamilyModel->editProjectFamily($fieldsfamily);
							}
							
							/*=======================================*/
							
							$fieldsfamily = array();
							foreach($defaultValues['animal'] as $animal){
								$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
								$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 	= $result;
								$fieldsfamily['PROPERTY_VALUES']['ANIMAL'] 		= $animal;
								$fieldsfamily['PROPERTY_VALUES']['TYPE'] 		= PROJECT_TYPE_FAMILY_ANIMAL;
								$projectfamilyModel->editProjectFamily($fieldsfamily);					
							}
							
													
							//установим цену для комнаты
							$ROOM = $roomModel->select(['PROPERTY_AREA'], true)->getElement($ID_ROOM_IB_ROOM);
							$area = $ROOM->PROPERTY_AREA_VALUE;
							$price_square = PRICE_SQUARE;
							if((int) $defaultValues['price_project'] > 0){
								$price_square = $defaultValues['price_project'];
							}
							$PRICE_ORDER = ($price_square * $area) + ($price_square * $area) / 100 * COMISSION_PERCENT;
							//добавляет параметры товара
							$arFields = array(
								"ID" => $result,
								"VAT_ID" => 1, //тип ндс
								"VAT_INCLUDED" => "Y" //НДС входит в стоимость
							);

							CCatalogProduct::Add($arFields);

							//Установление цены для товара
							$PRICE_TYPE_ID = 1;
							
							$PRICE_PROJECT = $PRICE_ORDER;

							$arFields = array(
								"PRODUCT_ID" => $result,
								"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
								"PRICE" => $PRICE_PROJECT,
								"CURRENCY" => "RUB",
								"QUANTITY_FROM" => 1,
								"QUANTITY_TO" => 1
							);

							CPrice::Add($arFields);
							$PFields['ID'] = $PROJECT_ARR->items[0]->ID;
							array_push($roomIds, $result);
							
							//$PFields['PROPERTY_VALUES']["ROOM_ORDER"] = $roomIds;
							//упдате
							$step = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
							if(empty($step) || $step < 3){
								$PFields["PROPERTY_VALUES"]["STEP"] = 3;
							}
							else{
								$PFields["PROPERTY_VALUES"]["STEP"] = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
							}
							$PFields['PROPERTY_VALUES']['TYPE_ORDER'] = PROJECT_TYPE_ORDER_ROOM;
							
							$projectModel->editProject($PFields);		
						}
					}
				}
				//если заказ на всю квартиру
				else if($defaultValues['type_order'] == TYPE_ORDER_APARTMENT){
					//получим комнаты/квартиры заказа 
					$ARR_PROJECT_APART = $projectapartModel->getProjectApartList(["=PROPERTY_ID_ORDER"=>$PROJECT_ARR->items[0]->ID], [], 1, false);
					$roomIds = array_map(function($obj){
						return $obj->ID;
					},$ARR_PROJECT_APART->items);
					
			
					if(!empty($PROJECT_ARR->items[0]->PROPERTY_TYPE_ORDER_ENUM_ID) && $PROJECT_ARR->items[0]->PROPERTY_TYPE_ORDER_ENUM_ID != PROJECT_TYPE_ORDER_APARTMENT){
						$Ids = array_map(function ($obj) {
						return $obj->PROPERTY_ROOM_ORDER_VALUE;
						}, $PROJECT_ARR->items);
						$Ids = array_unique($Ids);

						foreach($Ids as $id){
							$result = $projectroomModel->deleteRoom($id[0]);
						};
						
					}
				
					//если ранее был заказ на квартиру
					if($fields['ID'] > 0){
					
						if(in_array($fields['ID'], $roomIds)){
							
							$ARR_PROJECT_FAMILY = $projectfamilyModel->getProjectFamilyList(['=PROPERTY_ID_ORDER' => $fields['ID']], [], 1, false);
							$familyIds = array_map(function ($obj) {
								return $obj->ID;
							}, $ARR_PROJECT_FAMILY->items);
							$familyIds = array_unique($familyIds);
							if(count($familyIds) > 0){
								foreach($familyIds as $id){																
									$projectfamilyModel->deleteFamily($id);
								};
							};
							
										
							//обновим квартиру
							$result = $projectapartModel->editApartProject($fields);
							
							//семья квартиры
							if($result && (int) $fields['ID'] > 0){
							/*=============================================*/
								
								foreach($defaultValues['people'] as $people){
									$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
									$gender = false;
									if($people["gender"] == "man"){
										$gender = 95;
									}
									else if($people["gender"] == "female"){
										$gender = 96;
									}
									$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $fields["ID"];
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people["age"];
									$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_GROWN;
									$projectfamilyModel->editProjectFamily($fieldsfamily);
								}
								
								/*===========================================*/
								
								$fieldsfamily = array();
								foreach($defaultValues['people_children'] as $people_children){
									$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
									$gender = false;
									if($people_children["gender"] == "man"){
										$gender = 95;
									}
									else if($people_children["gender"] == "female"){
										$gender = 96;
									}
									$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $fields["ID"];
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
									$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people_children["age"];
									$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_CHILDREN;
									$projectfamilyModel->editProjectFamily($fieldsfamily);
								}
								
								/*=======================================*/
								
								$fieldsfamily = array();
								foreach($defaultValues['animal'] as $animal){
									$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
									$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 	= $fields['ID'];
									$fieldsfamily['PROPERTY_VALUES']['ANIMAL'] 		= $animal;
									$fieldsfamily['PROPERTY_VALUES']['TYPE'] 		= PROJECT_TYPE_FAMILY_ANIMAL;
									$projectfamilyModel->editProjectFamily($fieldsfamily);		
								}
												
								
								$ID_APART = $PROJECT_ARR->items[0]->PROPERTY_ID_FLAT_VALUE;
								//установим цену для квартиры
								if((int) $fields['ID'] > 0 && $ID_APART > 0){
									$APARTMENT = $planModel->select(['PROPERTY_AREA'], true)->getElement($ID_APART);
									$area = $APARTMENT->PROPERTY_AREA_VALUE;
									
									$price_square = PRICE_SQUARE;
									if((int) $defaultValues['price_project'] > 0){
										$price_square = $defaultValues['price_project'];
									}								
									$PRICE_ORDER = ($price_square * $area) + ($price_square * $area) / 100 * COMISSION_PERCENT;
										
									$PRICE_PROJECT = $PRICE_ORDER;

									//Установление цены для товара
									$PRICE_TYPE_ID = 1;
									
								
									$arFields = array(
										"PRODUCT_ID" => $fields['ID'],
										"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
										"PRICE" => $PRICE_PROJECT,
										"CURRENCY" => "RUB",
										"QUANTITY_FROM" => 1,
										"QUANTITY_TO" => 1
									);

									$res = CPrice::GetList(
										array(),
										array(
											"PRODUCT_ID" => $fields['ID'],
											"CATALOG_GROUP_ID" => $PRICE_TYPE_ID
										)
									);
									
									if ($arr = $res->Fetch())
									{
										CPrice::SetBasePrice($fields['ID'], $PRICE_PROJECT, 'RUB');						
									}
									else
									{
										CPrice::Add($arFields);
									}
								};	
							}
						}
					}
					else{
						//добавим квартиру + семью

						$result = $projectapartModel->editApartProject($fields);
						
						foreach($defaultValues['people'] as $people){
							$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
							$gender = false;
							if($people["gender"] == "man"){
								$gender = 95;
							}
							else if($people["gender"] == "female"){
								$gender = 96;
							}
							$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
							$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people["age"];
							$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $result;
							$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_GROWN;
							$projectfamilyModel->addProjectFamily($fieldsfamily);
				
						}
						$fieldsfamily = array();
						foreach($defaultValues['people_children'] as $people_children){
							$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
							$gender = false;
							if($people["gender"] == "man"){
								$gender = 95;
							}
							else if($people["gender"] == "female"){
								$gender = 96;
							}
							$fieldsfamily['PROPERTY_VALUES']['PEOPLE_GENDER'] 		= $gender;
							$fieldsfamily['PROPERTY_VALUES']['PEOPLE_AGE'] 			= $people_children["age"];
							$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 			= $result;
							$fieldsfamily['PROPERTY_VALUES']['TYPE'] 				= PROJECT_TYPE_FAMILY_CHILDREN;
							$projectfamilyModel->addProjectFamily($fieldsfamily);
							
						}
						$fieldsfamily = array();
						foreach($defaultValues['animal'] as $animal){
							$fieldsfamily['NAME'] = PATTERN_ORDER_NAME .time();
							$fieldsfamily['PROPERTY_VALUES']['ANIMAL'] 		= $animal;
							$fieldsfamily['PROPERTY_VALUES']['ID_ORDER'] 	= $result;
							$fieldsfamily['PROPERTY_VALUES']['TYPE'] 		= PROJECT_TYPE_FAMILY_ANIMAL;
							$projectfamilyModel->addProjectFamily($fieldsfamily);		
						}
											
						$ID_APART = $PROJECT_ARR->items[0]->PROPERTY_ID_FLAT_VALUE;
				
						//установим цену для квартиры
						if((int) $result > 0 && $ID_APART > 0){
							
							$APARTMENT = $planModel->select(['PROPERTY_AREA'], true)->getElement($ID_APART);
							
							$area = $APARTMENT->PROPERTY_AREA_VALUE;
							$price_square = PRICE_SQUARE;
							if((int) $defaultValues['price_project'] > 0){
								$price_square = $defaultValues['price_project'];
							}
							$PRICE_ORDER = ($price_square * $area) + ($price_square * $area) / 100 * COMISSION_PERCENT;
							//добавляет параметры товара
							$arFields = array(
								"ID" => $result,
								"VAT_ID" => 1, //тип ндс
								"VAT_INCLUDED" => "Y" //НДС входит в стоимость
							);
							
							CCatalogProduct::Add($arFields);

							//Установление цены для товара
							$PRICE_TYPE_ID = 1;
							
							$PRICE_PROJECT = $PRICE_ORDER;

							$arFields = array(
								"PRODUCT_ID" => $result,
								"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
								"PRICE" => $PRICE_PROJECT,
								"CURRENCY" => "RUB",
								"QUANTITY_FROM" => 1,
								"QUANTITY_TO" => 1
							);

							CPrice::Add($arFields);
								
							array_push($roomIds, $result);

							$fields['PROPERTY_VALUES']["ROOM_ORDER"] = $roomIds;
							//упдате
							$step = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
							if(empty($step) || $step < 3){
								$fields["PROPERTY_VALUES"]["STEP"] = 3;
							}
							else{
								$fields["PROPERTY_VALUES"]["STEP"] = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
							}
							$fields['PROPERTY_VALUES']['TYPE_ORDER'] = PROJECT_TYPE_ORDER_APARTMENT;
							//впишем IDs комнат в заказ
							CIBlockElement::SetPropertyValuesEx($PROJECT_ARR->items[0]->ID, IB_ORDER_PROJECT, $fields['PROPERTY_VALUES']);
						}
					}
				}
				
				/*=============================================================================
					//установим цену для всего проекта в целом, вместе с комнатами/квартирами
				=============================================================================*/
				
				$PRICE_TYPE_ID = 1;			
				$PRICE_PROJECT = 0;
			
				foreach($roomIds as $order){
					$PRICE_PROJECT += CPrice::GetBasePrice($order)['PRICE'];
				}
			
				$arFields = array(
					"PRODUCT_ID" => $PROJECT_ARR->items[0]->ID,
					"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
					"PRICE" => $PRICE_PROJECT,
					"CURRENCY" => "RUB",
					"QUANTITY_FROM" => 1,
					"QUANTITY_TO" => 1
				);

				$res = CPrice::GetList(
					array(),
					array(
						"PRODUCT_ID" => $PROJECT_ARR->items[0]->ID,
						"CATALOG_GROUP_ID" => $PRICE_TYPE_ID
					)
				);
							
				if ($arr = $res->Fetch())
				{
					CPrice::SetBasePrice($PROJECT_ARR->items[0]->ID, $PRICE_PROJECT, 'RUB');						
				}
				else
				{
					CPrice::Add($arFields);
				}		
			};
		}
		else{
			$this->_helper->json(['result' => false, "response" => false]);
		}
		$this->_helper->json(['result' => $result, "response" => true]);
	}
	
	public function step3Action() {
		
		$APP = Zend_Registry::get('BX_APPLICATION');	
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'create-project-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'project');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Оформление"));
		
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$projectModel = new Sibirix_Model_Project();
		$projectroomModel = new Sibirix_Model_ProjectRoom();
		$projectapartModel = new Sibirix_Model_ProjectApart();
		$projectfamilyModel = new Sibirix_Model_ProjectFamily();
		$roomModel = new Sibirix_Model_Room();
		$userModel = new Sibirix_Model_User();
		
		/*=============================================
			АВТОРИЗОВАН?
		=============================================*/
		if(Sibirix_Model_User::isAuthorized()){
			$auth = true;
			$id_user = $userModel->getId();
			/*==========================================
				Узнаем есть ли у пользователя уже заказ
				Если есть берем
			=========================================*/
			$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user], [], 1, false, 1);
			//если не пройден 2 шаг
			if($PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE < 3){
				$this->redirect('/project/step2/');
			}
			if($PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE >= 3){
				$access = true;
			}
			$count = $PROJECT_ARR->pageData->totalItemsCount;
			if($count > 0){
				$item = $PROJECT_ARR->items[0];
				//id планировки
				$id_flat = $item->PROPERTY_ID_FLAT_VALUE;
				//id планировки(вариант)
				$id_plan = $item->PROPERTY_ID_OPTION_PLAN_VALUE;
				
				if((int) $id_plan > 0){
					$PLAN_OPTION_SAVE = $planoptionModel->getPlanList(['=ID'=>$id_plan], [], 1, false, 1);		
					//получии путь к картинке
					foreach($PLAN_OPTION_SAVE->items as $plan){
						if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
							$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'COMPLEX_LIST');
						} else {
							$image = '/local/images/proomer2.png';
						};
						$plan->PLAN_IMAGE = $image;
					};
				};		
				/*============================================
					Узнаем цену проекта
				============================================*/
				$PRICE_PROJECT += CPrice::GetBasePrice($item->ID)['PRICE'];
				
				$orderIds = array_map(function ($obj) {
					return $obj->PROPERTY_ROOM_ORDER_VALUE;
				}, $PROJECT_ARR->items);
				$orderIds = array_unique($orderIds);
						
				if($item->PROPERTY_TYPE_ORDER_ENUM_ID == PROJECT_TYPE_ORDER_ROOM){
					$ARR_PROJECT_ORDER = $projectroomModel->getProjectRoomList(['=ID' => $orderIds], [], 1, false);
				}
				else if($item->PROPERTY_TYPE_ORDER_ENUM_ID == PROJECT_TYPE_ORDER_APARTMENT){
					$ARR_PROJECT_ORDER = $projectapartModel->getProjectApartList(['=ID' => $orderIds], [], 1, false);	
				}
				
				//Получаем характеристики
				$TYPE_ROOM = Sibirix_Model_Reference::getReference(HL_TYPEROOM, array("UF_NAME"), "UF_XML_ID");		
				$animalList = Sibirix_Model_Reference::getReference(HL_ANIMAL, array("UF_NAME"), "UF_XML_ID");

				foreach($ARR_PROJECT_ORDER->items as $order){
			
					if(isset($order->PROPERTY_TYPE_ROOM_VALUE)){
						$propertyAnimalsValue = $TYPE_ROOM[$order->PROPERTY_TYPE_ROOM_VALUE];	
						$order->PROPERTY_TYPE_ROOM_VALUE = $propertyAnimalsValue;
					};
					$order->PRICE_ROOM = (PRICE_SQUARE * $order->PROPERTY_AREA_VALUE) + (PRICE_SQUARE * $order->PROPERTY_AREA_VALUE) / 100 * COMISSION_PERCENT;
					$peopleIds = $order->PROPERTY_PEOPLE_VALUE;
					$peoplechildIds = $order->PROPERTY_PEOPLE_CHILDREN_VALUE;
					$peopleanimalIds = $order->PROPERTY_ANIMAL_VALUE;

					if(count($peopleanimalIds) > 0){$resanimal = $projectfamilyModel->getProjectFamilyList(['=ID' => $peopleanimalIds],[], 1);}
					else{$resanimal = array();};
			
					if(count($peoplechildIds) > 0){$respeoplechild = $projectfamilyModel->getProjectFamilyList(['=ID' => $peoplechildIds],[], 1);}
					else{$respeoplechild = array();};
			
					if(count($peopleIds) > 0){$respeople = $projectfamilyModel->getProjectFamilyList(['=ID' => $peopleIds],[], 1);}
					else{$respeople = array();};
				
					$propertyStylesValue = array();
					foreach($resanimal->items as $item){
							$propertyAnimalsValue = $animalList[$item->PROPERTY_ANIMAL_VALUE];	
							$item->PROPERTY_ANIMAL_VALUE = $propertyAnimalsValue;
					};
					
					$order->FAMILY = array(
						'PEOPLE' 			=> $respeople,
						'PEOPLE_CHILDREN' 	=> $respeoplechild,
						'ANIMAL' 			=> $resanimal
					);	
				};

			};
		}
		else{
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
		};
	
		$this->view->assign([
            "pageTitle" 		=> $pageTitle,
			"projectData" 		=> $PROJECT_ARR->items[0],
			"ARR_PROJECT_ORDER"	=> $ARR_PROJECT_ORDER->items,
			"PLAN_OPTION_SAVE"	=> $PLAN_OPTION_SAVE->items
		]);
		
    }
	
	public function savestep3Action(){

		$planModel = new Sibirix_Model_Plan();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
		//$id = $this->getParam('id');
		
		//если авторизован
		$catalogSort = ['ASC'];
		if(Sibirix_Model_User::isAuthorized()){
			$id_user = $userModel->getId();
			$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user, '=PROPERTY_STATUS' => PROJECT_STATUS_DRAFT], $catalogSort, 1, false, 1);
			
			//получаем планировку
			$result = $planModel->getPlanList(['=ID' => $PROJECT_ARR->items[0]->PROPERTY_ID_PLAN], [], 1, false, 1);
			$item = $result->items[0];
			$fields = array();
			//провери есть ли уже добавленные заказы
			$count = $PROJECT_ARR->pageData->totalItemsCount;
			if($count == 0){
				$fields["PROPERTY_VALUES"]["STEP"] = 3;
				//адд
				if($projectModel -> addProject($fields)){
				};
			}
			else{
				//упдате
				$step = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;

				if(empty($step) || $step < 4){
					$fields["PROPERTY_VALUES"]["STEP"] = 4;
				}
				else{
					$fields["PROPERTY_VALUES"]["STEP"] = $PROJECT_ARR->items[0]->PROPERTY_STEP_VALUE;
				}
				CIBlockElement::SetPropertyValuesEx($PROJECT_ARR->items[0]->ID, IB_ORDER_PROJECT, $fields['PROPERTY_VALUES']);
			};
		}
		else{
			//$APP = Zend_Registry::get('BX_APPLICATION');
			//$APP->set_cookie("selectPlanItem", "", time()-3600);
			//$APP->set_cookie("selectPlanItem", $id, time()+3600 * 24 * 3);
		};
		$this->_helper->json(['result' => 1, "response" => true]);
	}
	
	public function planlistAction(){
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('planlist', 'html')
		->initContext();
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
		$id_flat = $this->getParam('id_flat');
		$result = $planoptionModel->getPlanList(['=PROPERTY_PLAN_FLAT' => $id_flat], [], 1, false, LIMIT_SELECT_ITEM);
		foreach($result->items as $plan){
				if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
					$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'COMPLEX_LIST');
				} else {
					$image = '/local/images/proomer2.png';
				}
				$plan->PROPERTY_IMAGES_VALUE = $image;
			}
		$this->view->assign([
			"planList" 	=> $result,
			"id_flat" 	=> $id_flat,
        ]);
	}
	
	public function projectcartAction(){

		$APP = Zend_Registry::get('BX_APPLICATION');
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('projectcart', 'html')
		->initContext();
		/*==========================================
			Авторизован?
		=========================================*/
		if(Sibirix_Model_User::isAuthorized()){
			$auth = true;
		}
		else{
			$auth = false;
		}
		
		$userModel = new Sibirix_Model_User();
		$planModel = new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
		
		$id_user = $userModel->getId();	
		$id_items = array();
		
		//если планировка(квартира)
		if($this->getParam('type') == 'apartment'){
			//получим id планировки(квартиры)
			$id_flat = $this->getParam('id_flat');
			$id_option = '';
			$filter = ['=ID'=>$id_flat];
			//получим выбранную планировку(квартиру)
			$ARR_PLAN = $planModel->getPlanList($filter);
			//получим путь для картинок
			foreach($ARR_PLAN->items as $plan){
				if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
					$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'COMPLEX_LIST');
				} else {
					$image = '/local/images/proomer2.png';
				}
				$plan->PLAN_IMAGE = $image;
			}
		}
		//если планировка(вариант)
		else if($this->getParam('type') == 'plan'){
			$id_flat = $this->getParam('id_flat');
			$id_option = $this->getParam('id_plan_option');
			$catalogSort["SORT"] = "ASC";	
			//получим выбранную планировку(квартиру)
			$ARR_PLAN = $planModel->getPlanList(['=ID'=>$id_flat], $catalogSort, 1, false, 1);
			//получим выбранную планировку(вариант)
			$ARR_PLAN_OPTION = $planoptionModel->getPlanList(['=ID'=>$id_option], $catalogSort, 1, false, 1);
			//получим путь для картинок
			foreach($ARR_PLAN->items as $plan){
				if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
					$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
				} else {
					$image = '/local/images/proomer2.png';
				}
				$plan->PLAN_IMAGE = $image;
			}
			//получим путь для картинок
			foreach($ARR_PLAN_OPTION->items as $plan){
				if (!empty($plan->PROPERTY_IMAGES_VALUE)) {
					$image = Resizer::resizeImage($plan->PROPERTY_IMAGES_VALUE, 'PLAN_LIST');
				} else {
					$image = '/local/images/proomer2.png';
				}
				$plan->PLAN_IMAGE = $image;
			}
		}
		else{
			return;
		};
		
		if(!empty($id_flat) && $id_flat > 0){
			$url = '/project/step2/';
			$nextstep = 'savestep1';
			$nextstep_d =	'К параметрам <br/>дизайн-проекта';
			if(!empty($id_flat) && $id_flat > 0 && !empty($id_option) && $id_option > 0){
				$access = true;
				$url = '/project/step2/';
				$nextstep = 'savestep1';
				$nextstep_d =	'К параметрам <br/>дизайн-проекта';
			};
			$show_plan = 'N';
		}
		else{
			$url = '/basket';
			$nextstep = 'savestep3';
			$nextstep_d = 'К оплате';
		};
		
		//заказ
		$PROJECT_ARR = $projectModel->getProjectList(['=PROPERTY_USER' => $id_user], [], 1, false, 1);

        $this->view->assign([
            "pageTitle" => $pageTitle,
			"ARR_PLAN_AJAX" => $ARR_PLAN->items,
			"ARR_PLAN_OPTION_AJAX" => $ARR_PLAN_OPTION->items,
			"projectData" => $PROJECT_ARR->items[0],
			"show_plan" => $show_plan,
            "filter"    => $filter,
			"auth" => $auth,
			"access" => $access,
			"url"		=> $url,
			"nextstep"		=> $nextstep,
			"nextstep_d"	=> $nextstep_d
        ]);
	}

    public function selectplanAction() {
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'service-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'service');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Выбор планировки"));
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('selectplan', 'html')
		->initContext();
		$planModel = new Sibirix_Model_Plan();
		$id_item = $this->getParam('id_item');
        $result = $planModel->getPlanList(['=ID'=>$id_item], [], 1, false, 1);
        $this->view->assign([
            "pageTitle" => $pageTitle,
            "filter"    => $filter,
            "test"  => $result->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
    }

	public function showplanAction() {
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'service-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'service');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Варианты планировок"));
		$step4Form = new Sibirix_Form_AddDesignStep4();
		$this->view->step4Form   = $step4Form;
    }
	
	
	/*public function uploadplanAction() {
        $upload = new Zend_File_Transfer();
		$planModel = new Sibirix_Model_Plan();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
        $fileModel = new Sibirix_Model_Files();
        $fileType   = $this->getParam("fileType");
        $resizeType = $this->getParam("resizeType");
		$id_user = $userModel->getId();
        $fields = array(
            "ID" => $designId,
            $fileType => $upload->getFileInfo()["file"]
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }
		$fields['PROPERTY_USER'] = $id_user;
        $accessExtensions = "";
        switch ($fileType) {
            case "DETAIL_PICTURE":
            case "PROPERTY_PLAN_FLAT":
                $accessExtensions = "jpg,png,jpeg,gif,bmp";
                break;
            case "PROPERTY_DOCUMENTS":
                $accessExtensions = "doc,docx,xls,xlsx,pdf,rar,tar,zip";
                break;
            default:
                $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
                break;
        }

        if (!$fileModel->checkFile($upload->getFileInfo()["file"], $accessExtensions)) {
            $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
        }

        $editResult = $projectModel->editProject($fields);

        $newFile = false;
        if ($editResult) {
            $newFile = $this->_model->select([$fileType], true)->getElement($designId);
        }

        if ($this->getParam("files") == "true") {
            $response = array();
            if ($editResult) {
                $fileData = EHelper::getFileData($newFile->$fileType);
                $response = array(
                    "valueId" => $newFile->$fileType,
                    "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                    "fileName" => $fileData["ORIGINAL_NAME"],
                    "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
                );
            }
        } else {
            $response = array();
            if ($editResult) {
                if (empty($resizeType)) {
                    $resizeType = "DROPZONE_MAIN_PHOTO";
                }
                $response["imageSrc"] =  Resizer::resizeImage($newFile->$fileType, $resizeType);
            }

        }

        $this->_helper->json(['result' => (bool)$editResult, "response" => $response]);
    }*/
	
	    /**
     * Загрузка изображений с dropzone "налету"
     */
    public function uploadplanAction() {
        $upload = new Zend_File_Transfer();
		$planModel = new Sibirix_Model_Plan();
		$projectModel = new Sibirix_Model_Project();
		$userModel = new Sibirix_Model_User();
        $designId = $this->getParam("designId");
        $fileModel = new Sibirix_Model_Files();

        $fileType   = $this->getParam("fileType");
        $resizeType = $this->getParam("resizeType");

        $fields = array(
            "ID" => $designId,
            $fileType => $upload->getFileInfo()["file"]
        );

        $regep = '/^PROPERTY_(.*)$/';
        if (preg_match($regep, $fileType, $matches)) {
            $fields["PROPERTY_VALUES"][$matches[1]] = $fields[$fileType];
            unset($fields[$fileType]);
        }
		
		$id_user = $userModel->getId();
		
		$fields['PROPERTY_VALUES']['USER'] = $id_user;
		$fields['PROPERTY_VALUES']['STATUS'] = PROJECT_STATUS_MODERATION;
        $accessExtensions = "";
        switch ($fileType) {
            case "DETAIL_PICTURE":
			case "PREVIEW_PICTURE":
            case "PROPERTY_PLAN_FLAT":
                $accessExtensions = "jpg,png,jpeg,gif,bmp";
                break;
            case "PROPERTY_DOCUMENTS":
                $accessExtensions = "doc,docx,xls,xlsx,pdf,rar,tar,zip";
                break;
            default:
                $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
                break;
        }

        if (!$fileModel->checkFile($upload->getFileInfo()["file"], $accessExtensions)) {
            $this->_helper->json(['result' => false, "response" => "Invalid file type"]);
        }
        $editResult = $projectModel->editProject($fields);

        $newFile = false;
        if ($editResult) {
            $newFile = $this->_model->select([$fileType], true)->getElement($designId);
        }

        if ($this->getParam("files") == "true") {
            $response = array();
            if ($editResult) {
                $fileData = EHelper::getFileData($newFile->$fileType);
                $response = array(
                    "valueId" => $newFile->$fileType,
                    "fileType" => GetFileExtension($fileData["FILE_NAME"]),
                    "fileName" => $fileData["ORIGINAL_NAME"],
                    "fileSize" => round($fileData["FILE_SIZE"]/1024) . " Кб",
                );
            }
        } else {
            $response = array();
            if ($editResult) {
                if (empty($resizeType)) {
                    $resizeType = "DROPZONE_MAIN_PHOTO";
                }
                $response["imageSrc"] =  Resizer::resizeImage($newFile->$fileType, $resizeType);
            }

        }

        $this->_helper->json(['result' => (bool)$editResult, "response" => $response]);
    }
	
	public function selectdesignAction() {

    }
	public function showdesignAction() {

    }
	public function orderAction() {

    }
	public function listAction(){
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'service-page');
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'project');
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Все проекты"));
	}
	public function superManAction() {
        $form = new Sibirix_Form_SuperMan();
		$model = new Sibirix_Model_SuperMan();
        $formData = $this->getAllParams();
		
        if ($form->isValid($formData)) {
            $validData = $form->getValues();

            if (!$form->antiBotCheck()) {
                $this->_response->stopBitrix(true);
                $this->_helper->viewRenderer->setNoRender();
                return false;
            }
			$validData['city'] = $formData['city'];

            $addResult = $model->add($validData);

            if ($addResult) {
                $notification = new Sibirix_Model_Notification();
                $emailTo = Settings::getOption("FEEDBACK_EMAIL_TO");
                $titleMail = "Вызов замерщика";

                $notification->sendFeedback($addResult, $validData, 'alexme777@yandex.ru', $titleMail);
            } else {
                $form->setFieldErrors("name", "Ошибка добавления");
            }

        } else {
            $form->getFieldsErrors();
        }

        $this->_helper->json(['success' => !$form->issetError(), 'errorFields' => $form->formErrors]);
    }
	
	public function deleteAction() {
		$projectModel = new Sibirix_Model_Project();
        $designId = $this->getParam("designId", 0);
        $result = $projectModel->update($designId, ['PROPERTY_VALUES' => ['STATUS' => PROJECT_STATUS_DELETED]]);
        $this->_helper->json(['success' => true, 'result' => $result]);
    }
	
	public function changetyperoomAction() {
	
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('changetyperoom', 'html')
		->initContext();
		
		$type_room = (int) $this->getParam('type_room');
		
		CModule::IncludeModule("fileman");
		CMedialib::Init();
		$arCollections = CMedialibCollection::GetList(array('arOrder'=>Array('NAME'=>'ASC'),'arFilter' => array('ACTIVE' => 'Y'))); 
		$arItems = CMedialibItem::GetList(array('arCollections' => array("0" => $type_room)));
		
		$this->view->assign([
            "listDesign" => $arItems
        ]);
		
	}
}