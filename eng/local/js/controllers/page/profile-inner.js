(function($, APP) {
    'use strict';

    /**
     * внутренняя страница профиля
     **/
    APP.Controls.Page.ProfileInner = can.Control.extend({

        init: function() {
			this.$fader = this.element.find('.sidebar-fader');
            this.$panel = this.element.find('.js-basket-panel');
            this.$counter = this.element.find('.js-counter');
            this.$title = this.element.find('.js-title');
            this.$total = this.element.find('.js-total');
            this.$empty = this.element.find('.js-empty');
            this.$full = this.element.find('.js-full');
            this.$titleWrapper = this.element.find('.js-title-wrapper');
            this.$bottom = this.element.find('.js-bottom');
            this.$scroller = this.element.find('.js-sidebar-scroller');
            this.$basketInner = this.element.find('.js-sidebar-basket-list');
            this.maxHeight = 0;
            this.scrollerInited = false;
			
			new APP.Controls.FilterForm(this.element.find(".js-filter-finance-form"));
	
            APP.Controls.SelectMulti.initList(this.element.find('.js-select-multi'));

            this.element.find('.js-tooltip').each(function(){
                var $this = $(this);
                $this.css({'left': -($this.outerWidth()/2 - 10)});
            });

            this.element.find('.js-profile-form').each(function() {
                new APP.Controls.ProfileForm(this);
            });

            new APP.Controls.ProfileSidebar(this.element.find('.js-sidebar'));			
			new APP.Controls.DesignerFinanceSettingsForm(this.element.find(".js-finance-form"));
			new APP.Controls.ProfileSettingsForm(this.element.find(".js-profile-form"));

            if (this.element.find('.js-profile-content').data('type') == 0) {
                $.fancybox.open([{
                    href: '#type-popup',
                    padding: 0,
                    helpers: {
                        media: {},
                        overlay: {
                            locked: false
                        }
                    },
                    tpl: {
                        closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>'
                    }
                }]);
            }
			
			$(".js-example-theme-multiple").select2({
				theme: "classic",
				placeholder: ""
			});		
			$(".js-select-sinle-type-room").select2({
				theme: "classic",
				placeholder: "",
				maximumSelectionLength: 1
			});
			$(".js-select-position").select2({
				theme: "classic",
				placeholder: "",
				maximumSelectionLength: 1
			});
			$(".js-select-operations").select2({
				theme: "classic",
				placeholder: "Все операции",
				maximumSelectionLength: 1
			});
			
			this.element.find("#tabs").tabs({active: 0});
			
			$(function() {
				$( ".datepicker" ).datepicker({
					dateFormat: "dd.mm.yy",
					dayNames: [ "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье" ],
					dayNamesMin: [ "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс" ],
					monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
					prevText: "Предыдущий",
					nextText: "Следующий"
				});
			});
	
			$(function() {
				$( ".radio" ).buttonset();
			});
			
			//Загрузчик документов
			new APP.Controls.DropzoneArea(this.element.find(".js-docs-scan-dropzone"), {
				url: '/profile/finance-settings-upload-scan',
				maxFilesize: 20,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .rar, .tar, .zip',
				files: true,
				multiple: true,
				paramsQuery: {
					fileType: "UF_SCAN",
					resizeType: "DROPZONE_MAIN_PHOTO"
				}
			});		
			
			var h_hght = 80; // высота шапки
			var h_mrg = 0;    // отступ когда шапка уже не видна
			this.scrollTopBlock(h_hght, h_mrg);
        },
		
		'#proccess_get change': function(el, e){
			var self = this.element;
			el.find('option').each(function() {
				if ($(this).is(':selected')){
		
					if($(this).val() == 'visa'){var text = 'Номер карты VISA'; var prefix_name = 'visa';}
					else if($(this).val() == 'mastercard'){var text = 'Номер карты MasterCard'; var profix_name = 'mastercard';}
					else{
						var text = 'Номер карты';
					}
			
					self.find('#add-payment').before('<div class="wrap-data">'+
						'<div class="row input-row">'+
							'<div class="col-xs-2 col-sm-2 col-md-2">'+
								'<label for="about" class="label-data required">'+text+'</label>'+
							'</div>'+
							'<div class="col-xs-4 col-sm-4 col-md-4">'+
								'<input type="text" class="required" maxlength="16" name="number_cart[]"/>'+
								'<span class="js-error error-message"></span>'+
							'</div>'+
							'<div class="col-xs-6 col-sm-6 col-md-6">'+
								'<div class="help_m_out">'+
									'<span class="help_m">Например:1234 5678 9012 3456</span>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>' );	
								
				}
			});		
		},
		
		'.js-add-proccess-get click': function(el, e){
			e.preventDefault();
			var target = e.target;
			if(this.element.find('input[title = "number_cart"]').length < 3){
				el.closest(this.element.find('#add-payment')).before(			
					'<div class="wrap-data">'+
							'<div class="row input-row">'+
								'<div class="col-xs-2 col-sm-2 col-md-2">'+
									'<label for="about" class="label-data required">Номер карты</label>'+
								'</div>'+
								'<div class="col-xs-4 col-sm-4 col-md-4">'+
									'<input type="text" class="required" maxlength="20" name="number_cart[]"/>'+
									'<span class="js-error error-message"></span>'+
								'</div>'+
								'<div class="col-xs-6 col-sm-6 col-md-6">'+
									'<div class="help_m_out">'+
										'<span class="help_m">Например:1234 5678 9012 3456</span>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'
				);

				if(this.element.find('input[name = "number_cart[]"]').length >= 3){
					this.element.find('#wrap_btn_add_account').remove();
				}
				
			};
		
		},
		
		'.js-save-finance click': function(el, e){
			e.preventDefault();
			var target = e.target;
		},
		
		'.js-cancel-part click': function(el,ev) {
			APP.helpers.showFancyboxDelete('Вы точно желаете снять работу с конкурса?', this.cancelPart, el, this);
		},
		
		'.js-list-social click': function(el, e){
			e.preventDefault();
			var target = e.target;
		
			while (target != this) {
				if (target.classList.contains('js-del-social')) {
					var form = $(target).parent().parent().prev();
					form.removeClass('hide').css('visibility', 'visible');
					$(target).parent().prev().text('Сохранить').removeClass('js-add-communiacation').addClass('js-save-communication');
					$(target).parent().addClass('hide');
					$(target).parent().prev().removeClass('hide');
					return;
				}
				else if (target.classList.contains('js-add-communiacation')) {										
					$(target).parent().prev().css('visibility', 'visible').removeClass('hide');
					$(target).text('Сохранить').removeClass('js-add-communiacation').addClass('js-save-communication');
					return;
				}
				else if (target.classList.contains('js-save-communication')) {										
					var form = $(target).parent().prev();
					var data = form.serializeArray();
					$.ajax({
						type: "POST",
						url: "/profile/add-communication",
						data: data,
						dataType: 'JSON',
						success: function(result){
							if(!result.success) return;
							form.addClass('hide');
							$(target).addClass('hide');
							$(target).next().removeClass('hide');
							$(target).next().children('.login_soc').text(result.login);
						}
					});
					return
				}
				target = target.parentNode;
			}
		},
		
		'.js-delete-design click': function(el,ev) {
			APP.helpers.showFancyboxDelete('Вы уверены, что хотите удалить этот проект?', this.removeDesign, el, this);
		},
		
		'.sidebar .profile-menu a click': function($el) {
			$el.closest('ul').find('.active').removeClass('active');
			$el.addClass('active');
		},

		'.js-upload-avatar click': function (el) {
			this.element.find('.panel input[type=file]').click();
		},
		'.js-upload-logo click': function (el) {
			this.element.find('.panel input[type=file]').click();
		},

		'.js-image change': function (el, ev) {
			var $form = el.closest('#upload-image');
			var options = {
				beforeSend: function()
				{
				
					el.closest('.avatar').addClass('ajax-loading');
				},
				success: function(data)
				{
					if (data.success) {
						el.closest('.avatar').css({'background-image': 'url("' + data.avatar + '")'});
						$('.js-profile-open').css({'background-image': 'url("' + data.headerAvatar + '")'});
						el.closest('.avatar').removeClass('ajax-loading');
						if (el.closest('.avatar').find('.js-remove-avatar').length == 0) {
							el.closest('.avatar').find('.js-upload-avatar').after('<div><span class="delete under-link js-remove-avatar">удалить фото</span></div>');
							el.closest('.avatar').find('.js-upload-avatar').html('загрузить новое фото');
						}
					} else {
						APP.helpers.showFancyboxMessage(data.messageTitle, data.messageText, 3000);
						el.val('');
						el.closest('.avatar').removeClass('ajax-loading');
					}
				}
			};

			$form.ajaxForm(options);
			$form.submit();
		},
		
		'.js-image-avatar change': function (el, ev) {
			
			var $form = el.closest('#upload-image');
			var self = this;
			var options = {
				beforeSend: function()
				{
					el.closest('.avatar').addClass('ajax-loading');
				},
				success: function(data)
				{
					if (data.success) {
						
						self.element.find('.avatar').css({'background-image': 'url("' + data.avatar + '")'});
						self.element.find('.avatar').empty();
						//$('.js-profile-open').css({'background-image': 'url("' + data.headerAvatar + '")'});
						el.closest('.avatar').removeClass('ajax-loading');
						if (el.closest('.avatar').find('.js-remove-avatar').length == 0) {
							//el.closest('.avatar').find('.js-upload-avatar').after('<div><span class="delete under-link js-remove-avatar">удалить фото</span></div>');
							el.closest('.avatar').find('.js-upload-avatar').html('Заменить фото');
						}
					} else {
						APP.helpers.showFancyboxMessage(data.messageTitle, data.messageText, 3000);
						el.val('');
						el.closest('.avatar').removeClass('ajax-loading');
					}
				}
			};

			$form.ajaxForm(options);
			$form.submit();
		},

		'.js-remove-avatar click': function(el,ev) {
			APP.helpers.showFancyboxDelete('Вы уверены, что хотите удалить эту фотографию?', this.removePhoto, el, this);
		},
				
		'.js-file-delete click': function (el) {
			this.element.ajaxl({
				url: '/profile/finance-settings-remove-scan',
				data: {file_id:el.closest(".upload-doc").data('value-id'), fileType: "UF_SCAN"},
				dataType: 'JSON',
				type: 'POST',
				success: this.proxy(function (data) {
					el.closest(".upload-doc").remove();
					$(window).trigger("dropzone.checkItems");
				})
			});
        },

		removePhoto: function(el) {
			el.closest('.avatar').addClass('ajax-loading');
			$.ajax({
				url: APP.urls.removeAvatar,
				data: {sessid: APP.sessid},
				type: 'POST',
				dataType: 'json',
				success: function (data) {
					if (data.success) {
						el.closest('.avatar').css({'background-image': 'url("' + data.avatar + '")'});
						$('.js-profile-open').css({'background-image': 'url("' + data.headerAvatar + '")'});
					}
				},
				complete: function (data) {
					if (data.success) {
						el.closest('.avatar').removeClass('ajax-loading');
						el.closest('.avatar').find('.js-upload-avatar').html('загрузить фото');
						el.remove();
					}
				}
			});
		},
		
		//отменяет участие в конкурсе
		cancelPart: function(el, self){
			var competitionId = el.closest('.competition').data('id-competition');
			$.ajax({
                url: '/profile/design/cancelpart',
                data: {competitionId: competitionId, sessid: APP.sessid},
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (!data.success) return;
					$(el).closest('.competition').remove();
                }
            });
		},
		
		removeDesign: function(el, self) {
			var $item = el.closest('.design-item');

			if ($item.hasClass('ajax-loading')) {
				return;
			}
			$item.addClass("ajax-loading");
			var elementId = el.data('element-id');
			var productId = el.data('product-id');
			
            $.ajax({
                url: APP.urls.basket.deleteDesign,
                data: {designId: elementId, sessid: APP.sessid},
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                        if (!data.success) return;

						if (data.basketCnt >= 0) {

							//this.$counter.html(data.basketCnt);
							//this.$title.html('В корзине ' + data.basketCnt + ' проект' + APP.helpers.getWordForm(data.basketCnt, []));
							//this.$total.html(APP.helpers.price(data.totalPrice, true));

							$item.animate({
								'margin-left': '100%',
								'height': 0
							}, 300, function() {
								$item.remove();
								if ($('.side-cart-item').length == 0) {
									this.$panel.toggle();
								}
							this.element.trigger("basketCountChanged", data.basketCnt);
								this.element.trigger("removeItemFromBasket", productId);
								this.reinitScroll();
							}.bind(this));
						} else { //пустая корзина
							//this.$counter.hide();
							//this.$full.hide();
							//this.$empty.show();
							//this.$title.html('В корзине пусто');
						}

                    this.element.trigger("basketCountChanged", data.basketCnt);
                },
                complete: function (data) {
                }
            });
        },
		
		'.js-get-maney-maney click': function(el, e){
			e.preventDefault();
			var target = e.target;
			var wrap_maney_maney = this.element.find("#wrap_maney_maney");
			wrap_maney_maney.fadeIn(500);
		},
		
		'.js-show-more-operations click': function(el, e){
			e.preventDefault();
			var target = e.target;
			var page = Number(this.element.find('#page').val());
			this.element.find('#page').val(page+=1);
			this.element.trigger("list.contentUpdate", {});
		},
		
		'.js-save click': function(el, e){
			e.preventDefault();
			var target = e.target;
			this.element.find('.js-save-your-life').click();
		},
		/*'.js-save-your-life click': function(el, e){
			e.preventDefault();
			var target = e.target;
			var $ajaxContent = this.element.find('.js-profile-form');
			this.element.find('#result_progress').text('Ваши изменения были сохранены');
		},*/
		
		'list.contentUpdate': function (el, e, param) {
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			var data = [];
			data = this.element.find('.js-filter-finance-form').serializeArray();
			var self = this;
			$ajaxContent.ajaxl({
				topPreloader: true,
				url: location.pathname,
				data: data,
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
				})
			});
		},
		
		scrollTopBlock: function(h_hght, h_mrg) {
			var elem = $('#top_block');
			var top = $(this).scrollTop();

			if(top > h_hght){
				elem.css('top', h_mrg);
				elem.addClass('fix');
			}           

			$(window).scroll(function(){
				top = $(this).scrollTop();
				if (top+h_mrg < h_hght) {
					elem.css('top', (h_hght-top));
					elem.removeClass('fix');
				} else {
					elem.css('top', h_mrg);
					elem.addClass('fix');
				}
			});
		}
    });

})(jQuery, window.APP);