(function($, APP) {
    'use strict';

    /**
     * внутренняя страница профиля - мои проекты
     **/
    APP.Controls.Page.ProfileProvider = can.Control.extend({
		
        init: function() {
			new APP.Controls.Sorting(this.element.find(".js-sort"));
			new APP.Controls.ViewCounter(this.element.find(".js-view-counter"));
			new APP.Controls.TypeView(this.element.find(".js-view-type"));
			new APP.Controls.GoodsEditForm(this.element.find(".js-goods-form"));
			var h_hght = 80; // высота шапки
			var h_mrg = 0;    // отступ когда шапка уже не видна
			this.scrollTopBlock(h_hght, h_mrg);
			
			new APP.Controls.DropzoneArea(this.element.find('.js-imgs-dropzone'), {
				url: '/profile/upload-file',
				maxFilesize: 5,
				files: false,
				multiple: true,
				acceptedFiles: '.jpg, .png, .jpeg, .gif, .bmp',
				paramsQuery: {
					fileType: "PROPERTY_IMAGES",
					resizeType: "PROMO_PHOTO"
				}
			});	
			
			$(".js-example-theme-multiple").select2({
				theme: "classic",
				placeholder: ""
			});
			
			$(".js-select-sinle-type-room").select2({
				theme: "classic",
				placeholder: "",
				maximumSelectionLength: 1
			});
			
			$(".js-select-position").select2({
				theme: "classic",
				placeholder: "",
				maximumSelectionLength: 1
			});
			
			$(".js-select-operations").select2({
				theme: "classic",
				placeholder: "Все операции",
				maximumSelectionLength: 1
			});
			
			this.element.find("#tabs").tabs({active: 0});
        },

        'list.contentUpdate': function (el, e, param) {
            var $ajaxContent = this.element.find('.js-ajax-list-content');
            var data = [];

            if (param.page > 0) {
                data.push({
                    name: "page",
                    value: param.page
                })
            } else {
                data.push({
                    name: "page",
                    value: 1
                })
            }

            if (param.viewCounter > 0) {
                data.push({
                    name: "viewCounter",
                    value: param.viewCounter
                })
            }
	
            this.element.find(".js-sort a").each(function(){
                data.push({
                    name: "sort[" + $(this).data("type") + "]",
                    value: $(this).data("method")
                })
            });

            $.merge(data, this.element.find('.js-filter-form').serializeArray());

            var self = this;
            $ajaxContent.ajaxl({
                topPreloader: true,
                url: '/shop/catalog/',
                data: data,
                dataType: 'HTML',
                type: 'POST',
                success: this.proxy(function (data) {
					$ajaxContent.html(data);
                    if ($ajaxContent.find('.not-found').length > 0) {
                        self.element.find('.filter').css({'visibility': 'hidden'});
                    } else {
                        self.element.find('.filter').css({'visibility': 'visible'});
                    }
				
                    new APP.Controls.Pagination(this.element.find(".js-pagination"));
                    //$.scrollTo(this.element.find('.catalog-block'), 500);
                   // new APP.Controls.Likes.initList(this.element.find('.js-like'));
                    APP.Controls.AddGoodsToBasket.initList(this.element.find(".js-add-basket"));
                })
            });
        },
		
		scrollTopBlock: function(h_hght, h_mrg) {
			var elem = $('#top_block');
			var top = $(this).scrollTop();

			if(top > h_hght){
				elem.css('top', h_mrg);
				elem.addClass('fix');
			}           

			$(window).scroll(function(){
				top = $(this).scrollTop();
				if (top+h_mrg < h_hght) {
					elem.css('top', (h_hght-top));
					elem.removeClass('fix');
				} else {
					elem.css('top', h_mrg);
					elem.addClass('fix');
				}
			});
		},
		
		//всех обматерил из-за этого маусдауна вместо клика
		'.js-form-category mousedown': function(el, e) {
			var target = e.target;
			//блок категорий
			var form_category_container = this.element.find(".form-category-container");

			while (target != el) {
				if(target.tagName == 'LABEL'){
					var len_form_category = $('.form-category').length;
					$(target).closest('.form-category').find('label').removeClass('active');
					if(!$(target).hasClass('active')){
						$(target).addClass('active');
					}
					
					//id категории по который сделали тык
					var id_section = $(target).find('input').data('id');
					//id родительской категории по которой сделали тык
					var parent_id_section = $(target).find('input').data('parent-id');
					//массив категорий
					var tree = JSON.parse(localStorage.getItem('tree'));
					//массив дочерних категорий
					var child_section_id_section = [];
					var i = 0;
					//возьмем все категории у который IBLOCK_SECTION_ID == id_section
					for(i; i < tree.length; i++){				
						if(tree[i].IBLOCK_SECTION_ID == id_section){
							child_section_id_section.push(tree[i]);
						}
					}	
					if(child_section_id_section.length > 0){
						
						//шаблон
						len_form_category = len_form_category += 1;
						var new_form_category = "<div class='form-category' data-lvl = '" + len_form_category +"'><span class='form-category__title'>Категория</span>";
						
						var i = 0;
						for(i; i < child_section_id_section.length; i++){
							new_form_category += "<label class='form-category-item js-form-category-item'><input type='radio' name='root_category_id' title='"+child_section_id_section[i].NAME+"' class='js-form-category-item__input_root' data-parent-id='"+child_section_id_section[i].IBLOCK_SECTION_ID+"' data-id='"+child_section_id_section[i].ID+"'><span class='form-category-item__label'>"+child_section_id_section[i].NAME+"</span></label>";
						}
						
						new_form_category += "</div>";
						//nalert(str)
			
						this.element.find('#section_id').attr('value', id_section);
						
					while (target != el) {
						if(target.className == 'form-category'){
						
							var lvl = +$(target).data('lvl');
							//this.element.find(".select_cat").text(str);
							$('.form-category').each(function(i) {
								if(Number($(this).data('lvl')) > lvl){
									$(this).remove();
								};
							});
							
							//var new_str = '';
							//var arr_str = str.split('/');
							/*console.log(arr_str)
							for(var i = 0; i < arr_str.length; i++){
								if(lvl == i + 1){
									//arr_str.splice(lvl - 1, 1, title)
								}
								else{								
									//arr_str[i] = title;
									new_str = new_str + arr_str[i] + ' / ';
								}
							}*/
							
							//console.log(arr_str)
							//$(".select_cat").text(new_str);
							
							break;
						}
						target = target.parentNode;
					}
						
						form_category_container.append(new_form_category);
																		
					}
					else{
						this.element.find('#section_id').attr('value', id_section);
						form_category_container.hide();
						var select_cat = this.element.find(".select_cat").text();
						var str = '';
						$('.form-category label.active > input').each(function(i) {
							if(str.length > 0){
								str = str + ' / ' + $(this).attr('title');
							}
							else{
								str = $(this).attr('title');
							}
						});
						this.element.find(".select_cat").text(str);
					}
					
					
				
					
					break;
				}
				target = target.parentNode;
			};
		},
		'.js-show-form-category click': function(el, e) {
			this.element.find("#form-category-container").show();
		}
    });

})(jQuery, window.APP);