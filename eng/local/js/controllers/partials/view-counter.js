(function ($, APP) {
    "use strict";

    APP.Controls.ViewCounter = can.Control.extend({},
        {
            init: function () {
            },

            'a click': function (el, e) {
                e.preventDefault();
                var value = el.data("value");

                if (value != undefined) {
                    this.element.find("a").removeClass("active");
                    el.addClass("active");
                    this.element.trigger("list.contentUpdate", {viewCounter: value});
                }
            }
        }
    );

    APP.Controls.ViewCounterItem = can.Control.extend({},
        {
            init: function () {
            },

            'a click': function (el, e) {
                e.preventDefault();
                var value = el.data("value");

                if (value != undefined) {
                    this.element.find("a").removeClass("active");
                    window.location.href = location.pathname+'?view='+value;
                    el.addClass("active");
                }
            }
        }
    );

})(jQuery, APP);

(function ($, APP) {
    "use strict";

    APP.Controls.TypeView = can.Control.extend({},
        {
            init: function () {
            },

            'a click': function (el, e) {
                e.preventDefault();
				
                el.parent().find('a').removeClass('active');
				el.addClass('active');
				
				if(el.data('value') == 'tile'){ 
					if($('.goods-list').hasClass('list')){
						$('.goods-list').removeClass('list');
						$('.goods-list').addClass('tile');
					}
	
				}
				else if(el.data('value') == 'list'){
					if($('.goods-list').hasClass('tile')){
						$('.goods-list').removeClass('tile');
						$('.goods-list').addClass('list');
					}
				}
            }
        }
    );

})(jQuery, APP);