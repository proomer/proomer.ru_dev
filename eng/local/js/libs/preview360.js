(function ( jqPw ) {
  jqPw.fn.preview360 = function( options ) {
    var settings = jqPw.extend({
      all_images:   36,
      count_images: 8,
      image_prefix: 'img-',
      image_ext:    '.jpg',
      images:       [],
    }, options );

    var period = settings.all_images / settings.count_images;

    return this.each(function() {
      var gallery = jqPw( this );
      var parentOffset = gallery.parent().offset();

      gallery.attr( 'data-current', 0 );

      if (gallery.attr('data-json')) {
        var preview_360_arr = [];
        jqPw.each(jQuery.parseJSON(gallery.attr('data-json')), function( index, value ) {
          preview_360_arr.push(value);
        });
        settings.images = preview_360_arr;
      }

      if (settings.images.length > 1) {
        settings.count_images = settings.images.length;
        for ( var i = 0; i < settings.images.length; i++ ) {
          jqPw( '<img>' ).appendTo( gallery ).addClass( 'preview360' ).attr( 'src', settings.images[i] ).attr( 'data-index', i+1 ).addClass('preview360-hide');
        }
      } else {
        for ( var i = 1; i < settings.count_images + 1; i++ ) {
          var cur_index = i * period;
          var cur_image_src = settings.image_prefix + cur_index + settings.image_ext;
          jqPw( '<img>' ).appendTo( gallery ).addClass( 'preview360' ).attr( 'src', cur_image_src ).attr( 'data-index', i ).addClass('preview360-hide');
        }
      }

      gallery.on( 'mousemove', function( e ) {
        var view_mouse_x = ( e.pageX - parentOffset.left );
        var view_width = gallery.width();
        var view_index = Math.ceil( view_mouse_x / Math.ceil(view_width / settings.count_images) );
        if ( (view_index >= 1) && (view_index <= settings.count_images) ) {
          if ( gallery.attr( 'data-current' ) != view_index) {
            gallery.attr( 'data-current', view_index );
            gallery.find( "img[data-index!='" + view_index + "']" ).css('z-index', 9)/*addClass( 'preview360-hide' )*/;
            gallery.find( "img[data-index='" + view_index + "']" ).css('z-index', 12)/*removeClass( 'preview360-hide' )*/;
          }
        }
      });
    });
  };
}( jQuery ));
