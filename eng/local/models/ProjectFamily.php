<?

/**
 * Class Sibirix_Model_Plan
 *
 */
class Sibirix_Model_ProjectFamily extends Sibirix_Model_Bitrix {

    protected $_iblockId = IB_ORDER_PROJECT_FAMILY;
    protected $_pageSize;
	protected $_bxElement;
    protected $_selectListFields = [
		'ID',
        'CODE',
        'NAME',
		'PROPERTY_PEOPLE_AGE',
		'PROPERTY_PEOPLE_GENDER',
		'PROPERTY_ID_ORDER',
		'PROPERTY_TYPE',
		'PROPERTY_ANIMAL',
		'PROPERTY_PEOPLE_AGE'
    ];
	
	protected $_selectListFields2 = [
        'ID',
        'CODE',
        'NAME',
		'PROPERTY_PEOPLE_AGE',
		'PROPERTY_PEOPLE_GENDER',
		'PROPERTY_ID_ORDER',
		'PROPERTY_TYPE',
		'PROPERTY_ANIMAL',
		'PROPERTY_PEOPLE_AGE'
    ];
	
	public function init($initParams = NULL) {
        $this->_bxElement = new CIBlockElement();
    }
	
	public function getProjectFamilyList($filter, $sort, $page, $profile=false, $limit = LIMIT_SELECT_ITEM) {
        $planList = $this->select($this->_selectListFields, true)->orderBy($sort, true)->where($filter)->getPageItem($page, $limit);
		return $planList;
    }
			
    public function addProjectFamily($fields){   
		if (!empty($fields["NAME"])) {
			$fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
		}
		$result = $this->Add($fields);
		return $result;
    }
	
    public function editProjectFamily($fields) {

        $bxElement           = new CIBlockElement();
        $fields["IBLOCK_ID"] = IB_ORDER_PROJECT_FAMILY;
	
        if ($fields["ID"] > 0) {
            $designId    = $fields["ID"];
            $designProps = $fields["PROPERTY_VALUES"];
            unset($fields["ID"], $fields["PROPERTY_VALUES"]);

            if (!empty($fields["NAME"])) {
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }

            $updateResult = $bxElement->Update($designId, $fields);

            if ($updateResult && !empty($designProps)) {
                $bxElement->SetPropertyValuesEx($designId, IB_ORDER_PROJECT_FAMILY, $designProps);

                //оповещение администратору
                if (array_key_exists('STATUS', $designProps) && $designProps['STATUS'] == DESIGN_STATUS_MODERATION) {
                    $notificationModel = new Sibirix_Model_Notification();

                    $design = $this->getElement($designId);
                    $designer = Sibirix_Model_User::getCurrent();
                    $notificationModel->statusModeraion($design, $designer);
                }
            }

            $result = $updateResult;
        } else {
			
            if (empty($fields["NAME"])) {
                $fields["NAME"] = 'Project Order Name'. time();
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }
            $newId = $bxElement->Add($fields);

            $result = $newId;
        }
        if (empty($fields["PRICE_VALUE"])) {
           // return $result;
        }

        $bxPrice = new CPrice();
        $res = $bxPrice->GetList(array(), array(
                "PRODUCT_ID"       => $fields["PRICE_VALUE"]["PRODUCT_ID"],
                "CATALOG_GROUP_ID" => $fields["PRICE_VALUE"]["CATALOG_GROUP_ID"],
                "CURRENCY"         => $fields["PRICE_VALUE"]["CURRENCY"]
            ));

        if ($arr = $res->Fetch()) {
            $bxPrice->Update($arr["ID"], $fields["PRICE_VALUE"]);
        } else {
            $bxCatalogProduct = new CCatalogProduct();
            $bxCatalogProduct->Add(["ID" => $fields["PRICE_VALUE"]["PRODUCT_ID"]]);
            $bxPrice->Add($fields["PRICE_VALUE"]);
        }

        return $result;
    }
	
	/**
     * Удаляет члена семьи
     * @param $Id
     * @return bool
     */
    public function deleteFamily($Id) {
		if (!((int)$Id > 0)) return false;
        $delResult = $this->_bxElement->Delete($Id);
        return $delResult;
    }
}
