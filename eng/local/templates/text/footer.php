<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$APPLICATION->AddHeadScript('/local/js/libs/jquery.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/masonry.pkgd.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/imagesloaded.pkgd.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery-migrate-1.2.1.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/can.custom.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/select2.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.mousewheel.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.scrollTo.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.jscrollpane.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery-ui.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.bxslider.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.ui.touch-punch.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.roundabout.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.fancybox.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.fancybox-thumbs.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.fancybox-thumbs.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.validate.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/preview360.js', 'common');
//$APPLICATION->AddHeadScript('/local/js/libs/materialize.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/share42.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.form.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/TweenMax.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/dropzone.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jstree.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/plugins.js', 'common');
$APPLICATION->AddHeadScript('/local/js/helpers.js', 'common');
$APPLICATION->AddHeadScript('/local/js/libs/jquery.knob.js', 'common');

$APPLICATION->AddHeadScript('/local/js/controllers/-pre-init.js', 'common');

$APPLICATION->AddHeadScript('/local/js/controllers/page/basket.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/builder-list.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/builder-detail.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/competition-list.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/competition-detail.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/complex-detail.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/complex-detail-plan.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/complex-list.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/shop-list.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/contacts.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/page/designer.js', 'common');
//$APPLICATION->AddHeadScript('/local/js/controllers/page/service.js', 'common');

$APPLICATION->AddHeadScript('/local/js/controllers/page/main.js', 'common');

$APPLICATION->AddHeadScript('/local/js/controllers/base.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/add-basket.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/add-favourite.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/basket-sidebar.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/order.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/fancybox-link.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/header-menu.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/likes.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/main-tooltip.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/moving-slider.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/select-multi.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/pagination.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/sorting.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/view-counter.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/range-slider.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/search-service.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/slon.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/preloader.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/profile-sidebar.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/dropzone-area.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/partials/dropzone2-area.js', 'common');


$APPLICATION->AddHeadScript('/local/js/controllers/form/form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/auth-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/change-pass-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/feedback-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/feedbackbuilder-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/planirovka-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/address-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/super-man-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/filter-form.js', 'common');
//$APPLICATION->AddHeadScript('/local/js/controllers/form/project-step5-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/order-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/profile-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/registration-form.js', 'common');
$APPLICATION->AddHeadScript('/local/js/controllers/form/type-form.js', 'common');

/** 360 Preview */
$APPLICATION->AddHeadScript('/local/js/libs/preview360.js', 'common');
/** Kyco preloader */
$APPLICATION->AddHeadScript('/local/js/libs/jquery.kyco.preloader.min.js', 'common');
$APPLICATION->AddHeadScript('/local/js/application.js', 'common');
include ($_SERVER["DOCUMENT_ROOT"] . P_LAYOUT . 'footer.php');



