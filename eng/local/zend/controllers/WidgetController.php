<?

/**
 * Виджет сервиса
 * Class WidgetController
 */
class WidgetController extends Sibirix_Controller {

    public function init() {
        
    }

    /**
     *  Step 1
     */
    public function complexesAction() {
        header('Access-Control-Allow-Origin: *');

        $complexModel = new Sibirix_Model_Complex();
        $houseModel = new Sibirix_Model_House();
        $entranceModel = new Sibirix_Model_Entrance();
        $floorModel = new Sibirix_Model_Floor();
        $flatModel = new Sibirix_Model_Flat();
        $designModel = new Sibirix_Model_Design();
        $planModel = new Sibirix_Model_Plan();
        $planoptionModel = new Sibirix_Model_PlanOption();
        //id застройщика
        $developer_id = $this->getParam('developer_id');
        //выберем комлексы у которых застройщик $developer_id
        $complexes = ["COMPLEXES" => $complexModel->select(['ID', 'NAME', 'DETAIL_PICTURE', 'PROPERTY_LOCATION'], true)->where(["PROPERTY_DEVELOPER" => $developer_id])->getElements()];

        foreach ($complexes['COMPLEXES'] as $complex) {
            //получим путь для картинок

            if (!empty($complex->DETAIL_PICTURE)) {
                $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($complex->DETAIL_PICTURE, 'WIDGET_COMPLEX_PREVIEW_PICTURE');
            } else {
                $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
            }

            $complex->IMG = $image;
            //получим дома комплекса
            $houseList = $houseModel->select(['ID', 'PROPERTY_STREET', 'PROPERTY_HOUSE_NUMBER'], true)->where(["PROPERTY_COMPLEX" => $complex->ID])->getElements();

            $addresses = array();
            foreach ($houseList as $house) {

                array_push($addresses, $house->PROPERTY_STREET . ',' . $house->PROPERTY_HOUSE_NUMBER);
                //получим дизайн
                $designList = $designModel->getDesignComplexList($house->ID);
            }

            $complex->ADDRESS = $addresses;

            $complex->DESIGNS = $designList;
        }
        echo json_encode($complexes);
        exit;
    }

    /**
     * 	Step 2
     */
    public function housesAction() {
        header('Access-Control-Allow-Origin: *');

        $complexModel = new Sibirix_Model_Complex();
        $houseModel = new Sibirix_Model_House();
        $entranceModel = new Sibirix_Model_Entrance();

        //id комплекса
        $complex_id = (int) $this->getParam('complex_id');

        if ($complex_id > 0) {
            $complex = $complexModel->select(['ID', 'NAME', 'PROPERTY_COMPLEX_PLAN'], true)->getElement($complex_id);
            $sort = ['ID' => 'ASC'];

            //получим путь до картинки комплекса
            if (!empty($complex->PROPERTY_COMPLEX_PLAN_VALUE)) {

                $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($complex->PROPERTY_COMPLEX_PLAN_VALUE, 'WIDGET_COMPLEX_GREAT_PICTURE');
            } else {
                $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
            }

            $complex->IMG = $image;
            //найдем все дома комплекса
            $houses = $houseModel->select(['ID', 'NAME', 'PROPERTY_STREET', 'PROPERTY_HOUSE_NUMBER', 'PROPERTY_COORDS'], true)->where(['=PROPERTY_COMPLEX' => $complex->ID])->orderBy($sort, true)->getElements();

            foreach ($houses as $house) {
                $coords = explode(",", $house->PROPERTY_COORDS_VALUE);
                $house->COORDS = ['X' => trim($coords[0]), 'Y' => trim($coords[1])];
                //найдем все подъезды дома
                $entrances = $houseModel->select(['ID', 'NAME'], true)->where(['=PROPERTY_HOUSE' => $house->ID], true)->orderBy($sort, true)->getElements();
                $house->ENTRANCES = $entrances;
                foreach ($entrances as $entrance) {
                    //найдем все этажи подъезда
                    $floors = $houseModel->select(['ID', 'NAME'], true)->where(['=PROPERTY_ENTRANCE' => $entrance->ID], true)->orderBy($sort, true)->getElements();
                    $entrance->FLOORS = $floors;
                }
            }
            $complex->HOUSES = $houses;

            $complex->DESIGNS_LINK = 'https://' . $_SERVER['SERVER_NAME'] . '/design/?complexId=' . $complex->ID;
            echo json_encode($complex);
            exit;
        } else {
            $this->_helper->json(['result' => false, "response" => false]);
            exit;
        };
    }

	
	
    /**
     * 	Step 3
	 *
	 *	@author Роман Камлюк <roman@kamlyuk.com>
     */
	 
    public function apartmentsAction() {
	
		// Разрешить использование ответа на сторонних сайтах
        header('Access-Control-Allow-Origin: *');

		// Подключение используемых моделей
        $complexModel = new Sibirix_Model_Complex();
        $houseModel = new Sibirix_Model_House();
        $entranceModel = new Sibirix_Model_Entrance();
        $floorModel = new Sibirix_Model_Floor();
        $flatModel = new Sibirix_Model_Flat();
		
		// Направление сортировки
        $sort = ['ID' => 'ASC'];
		
        // Идентификатор дома
		$house_id = (int) $this->getParam('house_id');
		
		// Идентификатор дома корректный
        if ($house_id > 0) {
		
			// Подъезды дома
			$entrances->ENTRANCES = $houseModel->select(['ID', 'NAME'], true)->where(['=PROPERTY_HOUSE' => $house_id], true)->orderBy($sort, true)->getElements();
			foreach($entrances->ENTRANCES as $entrance)
			{
			
				// Этажи подъезда
				$entrance->FLOORS = $floorModel->select(['ID', 'NAME', 'PROPERTY_FLOOR_PLAN'], true)->where(["=PROPERTY_ENTRANCE" => $entrance->ID])->orderBy($sort, true)->getElements();
				foreach ($entrance->FLOORS as $floor)
				{
				
					// Квартиры этажа
					$floor->APARTMENTS = $flatModel->select(['ID', 'NAME', 'PROPERTY_COORDS'], true)->where(["=PROPERTY_FLOOR" => $floor->ID])->orderBy($sort, true)->getElements();
					
					// Схема этажа
					if (!empty($floor->PROPERTY_FLOOR_PLAN_VALUE)) {
						$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($floor->PROPERTY_FLOOR_PLAN_VALUE, 'WIDGET_FLOOR_PICTURE');
					} else {
						$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
					}
					$floor->IMG = $image;
				}
			}
			
			// Ответ
            echo json_encode($entrances);
            exit;
        }
		
		// Идентификатор дома не корректный
		else {
            $this->_helper->json(['result' => false, "response" => false]);
            exit;
        };
    }

    /**
     * 	Step 4
     */
    public function designsAction() {
        header('Access-Control-Allow-Origin: *');

        $complexModel = new Sibirix_Model_Complex();
        $houseModel = new Sibirix_Model_House();
        $entranceModel = new Sibirix_Model_Entrance();
        $floorModel = new Sibirix_Model_Floor();
        $flatModel = new Sibirix_Model_Flat();
        $designModel = new Sibirix_Model_Design();
        $designRoomModel = new Sibirix_Model_DesignRoom();
        $planModel = new Sibirix_Model_Plan();
        $planoptionModel = new Sibirix_Model_PlanOption();
		
		// Направление сортировки
        $sort = ['ID' => 'ASC'];
        // Id квартиры
        $apartment_id = (int) $this->getParam('apartment_id');

		
        if ($apartment_id > 0) {
			
            // Квартира
            $apartment = $flatModel->select(['ID', 'NAME', 'PROPERTY_PLAN'], true)->getElement($apartment_id);
			
			// Площадь
			$apartment->SQUARE = ['TOTAL' => null, 'LIVING' => null];
			
			// Дизайны
			$arr_designs = array();
			
			if ($apartment->PROPERTY_PLAN_VALUE > 0)
			{
				
				// Планировка квартиры
				$plan = $planModel->select(['ID', 'NAME', 'PROPERTY_AREA', 'PROPERTY_PLAN_FLAT'], true)->getElement($apartment->PROPERTY_PLAN_VALUE);

				// Общая площадь
				$apartment->SQUARE = ['TOTAL' => $plan->PROPERTY_AREA_VALUE];
			   
			   // Варианты планировок
				$planoptions = $planoptionModel->select(['ID', 'NAME', 'PROPERTY_PLAN_FLAT'], true)->where(['=PROPERTY_PLAN_FLAT' => $plan->ID])->orderBy($sort, true)->getElements();

				foreach ($planoptions as $planoption)
				{
					
					$designs = $designModel->select(
					[
						'ID', 
						'CODE', 
						'NAME', 
						'PROPERTY_PLAN_FLAT', 
						'PROPERTY_IMG_3', 
						'DETAIL_PICTURE', 
						'PROPERTY_AREA'
					], true)->where(
					[
						'=PROPERTY_PLAN' => $planoption->ID,
						'=PROPERTY_STATUS' => DESIGN_STATUS_PUBLISHED
					])->orderBy($sort, true)->getElements();
					
					foreach ($designs as $design)
					{
						
						$design->LINK = 'https://' . $_SERVER['SERVER_NAME'] . '/design/' . $design->CODE . '/';
						
						$temp_price = $designModel->getPrice($design->ID);
						$design->PRICE = number_format($temp_price[$design->ID]['PRICE'], 0, '.', ' ');
						
						// Список идентификаторов дизайна комнат
						$design_room_id_list = $designModel->getDesignRoomIds($design->ID);
						
						//  Список идентификаторов  изображений дизайна комнат
						$design_rooms_images_id = $designRoomModel->getDesignRoomsImagesId($design_room_id_list);
						
						// Цены спецификаций каждого дизайна комнаты
						$design->PRICES = $designRoomModel->getSumPricesByDesignRoomIds($design_room_id_list);
					
						// Формат цен
						$prices = array();
						
						foreach($design->PRICES as $key => $price)
						{
							$prices[$key] = number_format($price, 0, '.', ' ');
						}
						
						$design->PRICES = $prices;
						
						
						$img_3_big = array(); // Большие 3d
						$img_3_medium = array(); // Средние 3d
						$img_3_little = array(); // Маленькие 3d
						$img_rooms_big = array(); // Большие изображения дизайна комнат
						$img_rooms_medium = array(); // Средние изображения дизайна комнат
						
						if (!empty($design->PROPERTY_IMG_3_VALUE))
						{
							foreach ($design->PROPERTY_IMG_3_VALUE as $img)
							{
								if (!empty($img))
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_BIG');
								}
								
								else
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
								}
								array_push($img_3_big, $image);
							}
						}
						
						else
						{
							if (!empty($design->DETAIL_PICTURE))
							{
								$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($design->DETAIL_PICTURE, 'WIDGET_IMG_3_BIG');
							}
							
							else
							{
								$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
							}
							array_push($img_3_big, $image);
						}
						
						
						
						if (!empty($design->PROPERTY_IMG_3_VALUE))
						{
							foreach ($design->PROPERTY_IMG_3_VALUE as $img)
							{
								if (!empty($img))
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_MEDIUM');
								}
								
								else
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
								}
								array_push($img_3_medium, $image);
							}
						}
						
						else
						{
							if (!empty($design->DETAIL_PICTURE))
							{
								$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($design->DETAIL_PICTURE, 'WIDGET_IMG_3_MEDIUM');
							}
							
							else
							{
								$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
							}
							array_push($img_3_medium, $image);
						}
						
						
						
						$i = 0;
						if (!empty($design->PROPERTY_IMG_3_VALUE))
						{
							foreach ($design->PROPERTY_IMG_3_VALUE as $img)
							{
								$i += 1;
								if ($i == 4)
								{
									if (!empty($img))
									{
										$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_LITTLE');
									}
									
									else
									{
										$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
									}
									array_push($img_3_little, $image);
									$i = 0;
								}
								else
								{
									continue;
								}
							}
						}
						else
						{
							if (!empty($design->DETAIL_PICTURE))
							{
								$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($design->DETAIL_PICTURE, 'WIDGET_IMG_3_LITTLE');
							}
							
							else
							{
								$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
							}
							array_push($img_3_little, $image);
						}
						
						// Уменьшение средних изображений дизайнов комнат
						if (!empty($design_rooms_images_id))
						{
							foreach ($design_rooms_images_id as $img)
							{
								if (!empty($img))
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_MEDIUM');
								}
								
								else
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
								}
								array_push($img_rooms_medium, $image);
							}
						}
						
						// Уменьшение больших изображений дизайнов комнат
						if (!empty($design_rooms_images_id))
						{
							foreach ($design_rooms_images_id as $img)
							{
								if (!empty($img))
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_BIG');
								}
								
								else
								{
									$image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
								}
								array_push($img_rooms_big, $image);
							}
						}
						
						$design->IMG = ['SMALL' => $img_3_little, 'MEDIUM' => $img_3_medium, 'BIG' => $img_3_big, 'DESIGN_ROOMS_MEDIUM' => $img_rooms_medium, 'DESIGN_ROOMS_BIG' => $img_rooms_big];
						array_push($arr_designs, $design);
					}
				}
			}
			
            $apartment->DESIGNS = $arr_designs;
            echo json_encode($apartment);
            exit;
        }
		
		else
		{
            $this->_helper->json(['result' => false, "response" => false]);
            exit;
        }
    }

    /**
     * Поиск комплекса по названию или адресу
     */
    public function searchComplexAction() {
        header('Access-Control-Allow-Origin: *');
        $q = $this->getParam('q');
        $complexModel = new Sibirix_Model_Complex();

        $filter = [
            [
                "LOGIC" => "OR",
                ["NAME" => $q . "%"],
                ["PROPERTY_LOCATION" => $q . "%"],
            ],
        ];
        $complexList = $complexModel->select(array("ID", "NAME", "PROPERTY_CITY"), true)->where($filter)->getElements();

        echo json_encode($complexList);
        exit;
    }

    /**
     * Поиск плана по названию
     */
    public function searchPlanAction() {
        $formStep = new Sibirix_Form_SearchService_StepPlan();
        $formStep->populate($this->getAllParams());

        $planModel = new Sibirix_Model_Plan();
        $planList = $planModel->select(array("ID", "NAME"), true)
                ->where(["NAME" => "%" . $formStep->getValue("planName") . "%"])
                ->getElements();

        $planList = EHelper::prepareForForm($planList, "none");

        $this->view->itemList = $planList;
        $this->_response->stopBitrix(true);
    }

    /**
     * Подробности одного дизайн-проекта для 4 шага
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */
    public function designAction() {
        header('Access-Control-Allow-Origin: *');

        $designModel = new Sibirix_Model_Design();
		$designRoomModel = new Sibirix_Model_DesignRoom();
        $sort = ['ID' => 'ASC'];

        $design_id = (int) $this->getParam('design_id');

        if ($design_id > 0) {

            $design = $designModel->select(['ID', 'CODE', 'NAME', 'PROPERTY_PLAN_FLAT', 'PROPERTY_IMG_3', 'DETAIL_PICTURE', 'PROPERTY_AREA'], true)->where(['=ID' => $design_id, '=PROPERTY_STATUS' => DESIGN_STATUS_PUBLISHED])->orderBy($sort, true)->getElement();

            $design->LINK = 'https://' . $_SERVER['SERVER_NAME'] . '/design/' . $design->CODE . '/';
					
			$temp_price = $designModel->getPrice($design->ID);
			$design->PRICE = number_format($temp_price[$design->ID]['PRICE'], 0, '.', ' ');
			
			// Список идентификаторов дизайна комнат
			$design_room_id_list = $designModel->getDesignRoomIds($design->ID);
		
			// Цены спецификаций каждого дизайна комнаты
			$design->PRICES = $designRoomModel->getSumPricesByDesignRoomIds($design_room_id_list);
		
			// Формат цен
			$prices = array();
			foreach($design->PRICES as $key => $price)
			{
				$prices[$key] = number_format($price, 0, '.', ' ');
			}
			$design->PRICES = $prices;
			
			$img_3_big = array();
            $img_3_medium = array();
            $img_3_little = array();
            if (!empty($design->PROPERTY_IMG_3_VALUE)) {
                foreach ($design->PROPERTY_IMG_3_VALUE as $img) {
                    if (!empty($img)) {
                        $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_BIG');
                    } else {
                        $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
                    }
                    array_push($img_3_big, $image);
                }
            } else {
                if (!empty($design->DETAIL_PICTURE)) {
                    $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($design->DETAIL_PICTURE, 'WIDGET_IMG_3_BIG');
                } else {
                    $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
                }
                array_push($img_3_big, $image);
            }
			
			
            if (!empty($design->PROPERTY_IMG_3_VALUE)) {
                foreach ($design->PROPERTY_IMG_3_VALUE as $img) {
                    if (!empty($img)) {
                        $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_MEDIUM');
                    } else {
                        $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
                    }
                    array_push($img_3_medium, $image);
                }
            } else {
                if (!empty($design->DETAIL_PICTURE)) {
                    $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($design->DETAIL_PICTURE, 'WIDGET_IMG_3_MEDIUM');
                } else {
                    $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
                }
                array_push($img_3_medium, $image);
            }
			
			
            $i = 0;
            if (!empty($design->PROPERTY_IMG_3_VALUE)) {
                foreach ($design->PROPERTY_IMG_3_VALUE as $img) {
                    $i += 1;
                    if ($i == 4) {
                        if (!empty($img)) {
                            $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($img, 'WIDGET_IMG_3_LITTLE');
                        } else {
                            $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
                        }
                        array_push($img_3_little, $image);
                        $i = 0;
                    } else {
                        continue;
                    }
                }
            } else {
                if (!empty($design->DETAIL_PICTURE)) {
                    $image = 'https://' . $_SERVER['SERVER_NAME'] . Resizer::resizeImage($design->DETAIL_PICTURE, 'WIDGET_IMG_3_LITTLE');
                } else {
                    $image = 'https://' . $_SERVER['SERVER_NAME'] . '/local/images/proomer2.png';
                }
                array_push($img_3_little, $image);
            }
            $design->IMG = ['SMALL' => $img_3_little, 'MEDIUM' => $img_3_medium, 'BIG' => $img_3_big];

            echo json_encode($design);
            exit;
        } else {
            $this->_helper->json(['result' => false, "response" => false]);
            exit;
        }
    }

	
    /**
     * Заявка с виджета
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */
    public function requestAction() {
        header('Access-Control-Allow-Origin: *');

		$modelNotification = new Sibirix_Model_Notification();
		$modelBuilder = new Sibirix_Model_Builder();
		$modelBank = new Sibirix_Model_Bank();
		
		// Фильтруем post
		foreach ($_POST as $key => $value)
		{
			$post[$key] = htmlspecialchars(trim($value), ENT_QUOTES);
		}
		
		// Получаем наш email
		$our_email = Settings::getOption("EMAIL_FROM_WIDGET");
		
		// Email застройщика
		$developer_email = '';
		if(!empty($post['developer_id']))
		{
			$developer_email = $modelBuilder->getEmail($post['developer_id']);
			$developer_email = $developer_email->PROPERTY_E_MAIL_CENTER;
		}
		
		// Email банка
		$bank_email = '';
		if(!empty($post['bank_id']))
		{
			$bank_email = $modelBank->getEmail($post['bank_id']);
			$bank_email = $bank_email->PROPERTY_EMAIL;
		}
		
		switch($post['form'])
		{
			// Что искали
			case 'close':
			
				// Если не заполнено поле телефона
				if(empty($post['phone']))
				{
					http_response_code(400);
					exit;
				}
				
				// Отправка email нам
				if(!empty($our_email))
				{
					$modelNotification->widgetClose(
						$our_email,
						$post['name'],
						$post['phone'],
						$post['comment'],
						$post['site_name']
					);
				}
				
				// Отправка email застройщику
				if(!empty($developer_email))
				{
					$modelNotification->widgetClose(
						$developer_email,
						$post['name'],
						$post['phone'],
						$post['comment'],
						$post['site_name']
					);
				}
				
				// Отправка email банку
				if(!empty($bank_email))
				{
					$modelNotification->widgetClose(
						$bank_email,
						$post['name'],
						$post['phone'],
						$post['comment'],
						$post['site_name']
					);
				}
				break;
				
			// Вопрос
			case 'question':
				
				// Если не заполнено поле телефона
				if(empty($post['phone']))
				{
					http_response_code(400);
					exit;
				}
				
				// Отправка email нам
				if(!empty($our_email))
				{
					$modelNotification->widgetQuestion(
						$our_email,
						$post['name'],
						$post['phone'],
						$post['question'],
						$post['site_name']
					);
				}
				
				// Отправка email застройщику
				if(!empty($developer_email))
				{
					$modelNotification->widgetQuestion(
						$developer_email,
						$post['name'],
						$post['phone'],
						$post['question'],
						$post['site_name']
					);
				}
								
				// Отправка email банку
				if(!empty($bank_email))
				{
					$modelNotification->widgetQuestion(
						$bank_email,
						$post['name'],
						$post['phone'],
						$post['question'],
						$post['site_name']
					);
				}
				break;
				
			// Дизайн-проект
			case 'design':
				
				// Если не заполнено поле телефона или email
				if(empty($post['phone'].$post['email']))
				{
					http_response_code(400);
					exit;
				}
				
				// Формирование ссылки на страницу с дизайн-проектом
				// И ссылки на скачивание дизайн-проекта
				$designModel = new Sibirix_Model_Design();
				$design = $designModel->select(['ID', 'CODE', 'PROPERTY_DOCUMENTS'], true)->where(['=ID' => $post['design_id']])->getElement();
                $design_link = 'https://' . $_SERVER['SERVER_NAME'] . '/design/' . $design->CODE . '/';
				$design_file_link = 'https://' . $_SERVER['SERVER_NAME'] . CFile::GetPath($design->PROPERTY_DOCUMENTS_VALUE);
				
				// Отправка email нам
				if(!empty($our_email))
				{
					$modelNotification->widgetDesign(
						$our_email,
						$post['name'],
						$post['phone'],
						$post['email'],
						$design_link,
						$post['site_name']
					);
				}
				
				// Отправка email клиенту
				if(!empty($post['email']))
				{
					$modelNotification->widgetDesignClient(
						$post['email'],
						$post['name'],
						$post['phone'],
						$post['email'],
						$design_file_link,
						$post['site_name']
					);
				}
				
				// Отправка email застройщику
				if(!empty($developer_email))
				{
					$modelNotification->widgetDesign(
						$developer_email,
						$post['name'],
						$post['phone'],
						$post['email'],
						$design_link,
						$post['site_name']
					);
				}				
				
				// Отправка email банку
				if(!empty($bank_email))
				{
					$modelNotification->widgetDesign(
						$bank_email,
						$post['name'],
						$post['phone'],
						$post['email'],
						$design_link,
						$post['site_name']
					);
				}
				break;
		}
		
    }

	
    /**
     * Поиск плана по адресу
     */
    public function searchPlanNameAction() {

        /* $ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
          $ajaxContext->addActionContext('search-plan-name', 'html')
          ->initContext();
         */
    }

    /*
     * Подключение скрипта-загрузчика, для застройщика
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */

    public function developerAction() {
        header('Access-Control-Allow-Origin: *');
        header('x-content-type-options: none');
        //$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
        //id застройщика
        $developer_id = $this->getParam('developer_id');

        // Если id отсутствует или неверный
        if (empty($developer_id)) {
            $developer_id = '';
        }

        $this->view->assign([
            "developer_id" => $developer_id,
			'autoload' => 'true',
			'first_step' => 1
        ]);
    }


    /*
     * Подключение скрипта-загрузчика, для банка
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */

    public function bankAction() {
        header('Access-Control-Allow-Origin: *');
        header('x-content-type-options: none');
        //$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
        //id банка
        $bank_id = $this->getParam('bank_id');

        // Если id отсутствует или неверный
        if (empty($bank_id)) {
            $bank_id = '';
        }

        $this->view->assign([
            "bank_id" => $bank_id,
			'autoload' => 'false',
			'first_step' => 3
        ]);
    }
	
    /*
     * Подключение html
     * 
     * @author Роман Камлюк <roman@kamlyuk.com>
     */

    public function htmlAction() {
        header('Access-Control-Allow-Origin: *');
    }

}
