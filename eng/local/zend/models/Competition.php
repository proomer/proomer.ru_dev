<?
/**
 * Class Sibirix_Model_Design
 * @method Sibirix_Model_Design_Row getElement($id=false)
 */
class Sibirix_Model_Competition extends Sibirix_Model_Bitrix {

    protected $_iblockId;

    protected $_selectFields = array(
        'ID',
        'NAME',
        'CODE'
    );

    protected $_selectListFields = [
        'ID',
        'CODE',
        'NAME'
    ];

    /**
     * Получает конкурсы
     * @param $designId
     * @return array|bool
     */
    public function getListCompetition($element) {

        $apartModel    = new Sibirix_Model_ProjectApart();
        $roomModel     = new Sibirix_Model_Room();
		
		$arFilter = Array( 
			array( 
				"LOGIC"=>"OR", 
				array("IBLOCK_ID"=>IB_ORDER_PROJECT_ROOM), 
				array("IBLOCK_ID"=>IB_ORDER_PROJECT_APARTMENT), 
			) 
		);

        $competitionList = $this->select(['ID', 'NAME', 'PROPERTY_TIME'], true)->where($arFilter)->getElements();
       
        return $competitionList;
    }
	
	public function getCompetition($element) {

        $apartModel    = new Sibirix_Model_ProjectApart();
        $roomModel     = new Sibirix_Model_Room();
		
		$arFilter = Array( 
			array( 
				"LOGIC"=>"OR", 
				array("IBLOCK_ID"=>IB_ORDER_PROJECT_ROOM, "ID"=>$element), 
				array("IBLOCK_ID"=>IB_ORDER_PROJECT_APARTMENT, "ID"=>$element), 
			) 
		);

        $competition = $this->select(['ID', 'NAME', 'CODE', 'PROPERTY_AREA'], true)->where($arFilter)->getPageItem(1, 10);
     
        return $competition;
    }



}
