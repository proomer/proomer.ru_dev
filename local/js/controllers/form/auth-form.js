(function ($, APP) {
    'use strict';

    APP.Controls.AuthForm = APP.Controls.Form.extend(
        {
            pluginName: 'authForm'
        },
        {
            onSuccess: function (data) {
				if(data.type == 5){
					window.location.href = APP.urls.profileDesign;
				}
				else{
					window.location.href = APP.urls.profile;
				}
            }
        }
    );

})(jQuery, window.APP);