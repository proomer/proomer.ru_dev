(function($, APP) {
    'use strict';
    /**
     * Контроллер детальной страницы жилого коплекса
     **/
    APP.Controls.Page.BuilderDetail = can.Control.extend({

        init: function() {
			var self = this;
			new APP.Controls.FeedbackForm(this.element.find(".js-feedbackbuilder-form"));
        },

        '.js-q input': function(el) {
            var q = el.val();
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			$ajaxContent.ajaxl({
				topPreloader: false,
				url: location.pathname,
				data: {'q': q},
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
				})
			});
        },
		
		'.js-complex-detail-nav click': function(el, e){
			e.preventDefault();
			var target = e.target;
			// цикл двигается вверх от target к родителям до this
			
			while (target != el) {
				if ($(target).is('a.linkblock')) {				
					// нашли элемент, который нас интересует!
					el.find('a.linkblock').each(function (i) {
						if($(this).hasClass('selected')){
							$(this).removeClass('selected');
						}
					});
					$(target).addClass('selected');
					var classname = target.getAttribute('data-for');
					this.element.find('.block').each(function (i) {
						if(!$(this).hasClass('disabled')){
							$(this).addClass('disabled');
						}
					});
				
					this.element.find('.block.'+classname).removeClass('disabled');
					return;
				}
				else if($(target).is('li.house')){
					el.find('li.house').each(function (i) {	
						$(this).removeClass('active');				
					});
					
					this.element.find('#address_flat').html(target.getAttribute('data-location'))
					if(!$(target).hasClass('active')){
						$(target).addClass('active');
						var id_house = target.getAttribute('data-id');
						this.element.find('#id_house').attr('value', id_house);				
						this.element.trigger("list.contentUpdate", {});
					}
					return;
				}
				target = target.parentNode;
			}
		}
		
    });

})(jQuery, window.APP);