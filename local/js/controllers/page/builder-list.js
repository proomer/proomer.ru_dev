(function($, APP) {
    'use strict';
    /**
     * Контроллер детальной страницы жилого коплекса
     **/
    APP.Controls.Page.BuilderList = can.Control.extend({

        init: function() {
			var self = this;
        },

        '.js-q input': function(el) {
            var q = el.val();
			var $ajaxContent = this.element.find('.js-ajax-list-content');
			$ajaxContent.ajaxl({
				topPreloader: false,
				url: location.pathname,
				data: {'q': q},
				dataType: 'HTML',
				type: 'POST',
				success: this.proxy(function (data) {
					$ajaxContent.html(data);
				})
			});
        }
    });

})(jQuery, window.APP);