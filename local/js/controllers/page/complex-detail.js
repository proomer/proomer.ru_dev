(function($, APP) {
    'use strict';
    /**
     * Контроллер детальной страницы жилого коплекса
     **/
    APP.Controls.Page.ComplexDetail = can.Control.extend({
		
        init: function() {
			var self = this;
			new APP.Controls.FilterForm(this.element.find(".js-filter-form"));
			
            this.element.find('.js-select').select2({
                minimumResultsForSearch: 500,
                width: 'style'
            });

            this.houseId = [];

            if (this.element.find('.js-item').length > 0) {
				
				ymaps.ready(function(){
				   self.initMap();
				});
                
                var self = this;
                var $slider = this.element.find('.js-building-slider');
                this.slider = $slider.bxSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    slideMargin: 30,
                    pager: true,
                    controls: false,
                    onSliderLoad: function () {
                        $slider.parent().css({'width': '512'});
                    },
                    onSlideBefore: function () {
                        $slider.parent().css({'width': '482'});
                    },
                    onSlideAfter: function ($slideElement) {
                        $slider.parent().css({'width': '512'});
                        $slider.parent().css({'height': $slideElement.height()});
                    }
                });
            }

            this.element.find('.js-slider').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                moveSlides: 1,
                slideMargin: 0,
                pager: true,
                controls: false
            });
            new APP.Controls.Likes.initList(this.element.find('.js-like'));
            APP.Controls.AddBasket.initList(this.element.find(".js-add-basket"));
        },

        '.js-item click': function(el) {
            if (!el.hasClass('active')) {
                this.activateHouse(el);
            }
        },
		
		'.js-select-room click': function(el, e){
			e.preventDefault();
			if(e.target.tagName == 'A' && e.target.classList.contains('room')){
				var target = e.target;
				if(e.target.classList.contains('selected')){e.target.classList.remove('selected');
					if($("#countRoom").attr('value')){
						var arr_count_room = JSON.parse($("#countRoom").attr('value'));
					}
					else{
						var arr_count_room = [];
					}
					var i = 0;
				
					for(i; i <= arr_count_room.length; i++){
						if(arr_count_room[i] == target.getAttribute('data-value')){
							arr_count_room.splice(i, 1);
							break;
						}
					};
				
					$("#countRoom").attr('value',JSON.stringify(arr_count_room));
					this.element.trigger("list.contentUpdate", {});
				}
				else{
					e.target.classList.add('selected');
					if($("#countRoom").attr('value')){
						var arr_count_room = JSON.parse($("#countRoom").attr('value'));
					}
					else{
						var arr_count_room = [];
					}
					var i = 0;
					var flag = 1;
					for(i; i <= arr_count_room.length; i++){
						if(arr_count_room[i] == target.getAttribute('data-value')){
							flag = 0;
							break;
						}
					};
					if(flag == 1){
						arr_count_room.push(+target.getAttribute('data-value'));
					}
					$("#countRoom").attr('value',JSON.stringify(arr_count_room));
					this.element.trigger("list.contentUpdate", {});
				}
				var filterParams = $('.js-filter-form').serializeArray();

                var validFilterParams = $.map(filterParams, function (el) {
                    return el.value.length > 0 ? el : null;
                });

                var getStr = $.map(validFilterParams, function (el) {
                    return el.name + "=" + el.value;
                }).join("&");

                var newUrl = location.origin + location.pathname + "?" + getStr;
                history.pushState({}, '', newUrl);
			}
		},
		
		'.js-select-item-plan click': function (el, e){
			e.preventDefault();
			var target = e.target;
		
			var target = e.target;
			// цикл двигается вверх от target к родителям до this
			while (target != el) {
				if ($(target).is('.plan') && $(target).is('.link')) {
					document.location.href = $(target).data('link');
					//complex/grad-moskvoskiy/3843
					// нашли элемент, который нас интересует!
					/*$.each(this.element.find(".list-plan .plan"), function( i, item ) {
						item.classList.remove('selected');
					});
					if(this.element.find("#nextstep").hasClass('disabled')){
						this.element.find("#nextstep").removeClass('disabled');
						//nextstep123
						var href = this.element.find("#nextstep").attr('href');
						href = href+'/'+target.getAttribute('data-id');
						this.element.find("#nextstep").attr('href',href);			
					}
					target.classList.add('selected');
					return;*/
				}
				target = target.parentNode;
			}
			
		},

        activateHouse: function(el) {
            var houseId = el.data('house-id');

            if (!this.houseId[houseId]) {
                el.find('.js-features li:not(:first-child)').remove();
                el.addClass('ajax-loading');

                $.ajax({
                    url: APP.urls.houseFlatCounter,
                    data: {'houseId': houseId},
                    type: 'POST',
                    dataType: 'HTML',
                    success: this.proxy(function (data) {
                        this.houseId[houseId] = data;
                        el.find('.js-features').append(data);
                        el.closest('.bx-viewport').css({'height': el.parent().height()});
                    }),
                    complete: this.proxy(function (data) {
                        el.removeClass('ajax-loading');
                    })
                });
            }

            this.element.find('.js-item.active').removeClass('active');

            if (this.element.find('.js-building-slider').length > 0) {
                var index;
                if (el.closest('.slide')) //если не на активном слайде
                {
                    index = el.closest('.slide').data('index');
                }
                this.slider.goToSlide(index);
            }
            el.addClass('active');
            el.closest('.bx-viewport').height(el.parent().height());

            this.moveMap();
        },

        initMap: function() {
			console.log(ymaps)
			var centerCoords = this.element.find('.js-item.active').data('map-position').split(',');
			
				
			var centerCoords = this.element.find('.js-item.active').data('map-position').split(',');

            var mapOptions = {
                /*center: new google.maps.LatLng(centerCoords[0],centerCoords[1]),
                zoom: 17,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                disableDoubleClickZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
                },
                scaleControl: true,
                scrollwheel: false,
                panControl: true,
                streetViewControl: true,
                draggable : true,
                overviewMapControl: true,
                overviewMapControlOptions: {
                    opened: false
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP*/
				center: [centerCoords[0],centerCoords[1]], 
				zoom: 17
            };
			
			this.map = new ymaps.Map ("map", mapOptions);
			
			  this.initMarkers();
			
            /*var centerCoords = this.element.find('.js-item.active').data('map-position').split(',');

            var mapOptions = {
                center: new google.maps.LatLng(centerCoords[0],centerCoords[1]),
                zoom: 17,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                disableDoubleClickZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
                },
                scaleControl: true,
                scrollwheel: false,
                panControl: true,
                streetViewControl: true,
                draggable : true,
                overviewMapControl: true,
                overviewMapControlOptions: {
                    opened: false
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var mapElement = this.element.find('.js-map');
            this.map = new google.maps.Map(mapElement.get(0), mapOptions);
			
			var styles = [
			  {
				stylers: [
				  { hue: "#00ffe6" },
				  { saturation: -20 }
				]
			  },{
				featureType: "road",
				elementType: "geometry",
				stylers: [
				  { lightness: 100 },
				  { visibility: "simplified" }
				]
			  },{
				featureType: "road",
				elementType: "labels",
				stylers: [
				  { visibility: "off" }
				]
			  }
			];

			this.map.setOptions({styles: styles});
            this.initMarkers();*/
        },

        moveMap: function() {
            var centerCoords = this.element.find('.js-item.active').data('map-position').split(',');

            var pos = new google.maps.LatLng(centerCoords[0],centerCoords[1]);
            this.map.panTo(pos);

            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
            this.markers = [];
            this.activeMarker.setMap(null);

            this.initMarkers();
        },

        initMarkers: function() {
            var coords = [];
			var house_number = [];
			var house_street = [];
            var ids = [];
            this.element.find('.js-item:not(.active)').each(function(el) {
                coords.push($(this).data('map-position').split(','));
                ids.push($(this).data('house-id'));
				house_number.push($(this).data('house-number'));
				house_street.push($(this).data('house-street'));
            });

            var activeCoords = this.element.find('.js-item.active').data('map-position').split(',');
            var centerCoords = this.element.find('.js-item.active').data('map-position').split(',');

            this.markers = [];

            var locations = coords;
            for (var i = 0; i < locations.length; i++) {
				
				var myPlacemark = new ymaps.Placemark(
					// Координаты метки
					[locations[i][0], locations[i][1]], {
						/* Свойства метки:
						   - контент значка метки */
						id: ids[i],
						address: house_street[i]+', '+house_number[i],
						iconContent: house_number[i],
						iconColor: '#0095b6',
						// Описываем опции геообъекта.
						// Цвет заливки.
						fillColor: '#fff',
						// Цвет обводки.
						strokeColor: '#fff',
						// Общая прозрачность (как для заливки, так и для обводки).
						opacity: 0.5,
						// Ширина обводки.
						strokeWidth: 5,
						// Стиль обводки.
						strokeStyle: 'shortdash'
						// - контент балуна метки
						//balloonContent: "1"
					},  
				{
					iconImageHref: '/local/images/marker.png', // картинка иконки
					iconImageSize: [25, 28], // размер иконки
			
					balloonLayout: "default#imageWithContent", // указываем что содержимое балуна кастомная херь
					balloonShadow: false
				});
				
				var $ajaxContent = this.element.find('.js-ajax-list-content');
				
				myPlacemark.events.add('click', function (e) {
					// Получение ссылки на дочерний объект, на котором произошло событие.
					var id_house = e.get('target').properties.get('id');
					var address = e.get('target').properties.get('address');
					$('body.complex-detail-page #id_house').attr('value', id_house);
					//$('#address_flat').html(address);
					$('.js-dynamic-title').html('Квартиры по адресу '+address)
					$('body.complex-detail-page').trigger("list.contentUpdate", {});
					/*var id_house = target.getAttribute('data-id');
					this.element.find('#id_house').attr('value', id_house);
					$ajaxContent.ajaxl({
						url: location.pathname,
						data: data,
						dataType: 'HTML',
						type: 'POST',
						success: function (data) {
						$ajaxContent.html(data);
						$.scrollTo($(".list-plan"), 500);

						}
					});*/
				});
							
				// Добавление метки на карту
				this.map.geoObjects.add(myPlacemark);
				
                /*var marker = new google.maps.Marker({
                    icon: {
                        url: '/local/images/sprite.png',
                        size: new google.maps.Size(54, 39),
                        origin: new google.maps.Point(230, 240),
                        anchor: new google.maps.Point(14, 39)
                    },
                    position: new google.maps.LatLng(locations[i][0], locations[i][1]),
                    map: this.map
                });

                this.markers.push(marker);
                var self = this;

                (function (map, marker, id) {
                    google.maps.event.addListener(marker, 'click', function () {
                        //активировать дом с id
                        self.activateHouse(self.element.find('.js-item[data-house-id=' + id + ']'));
                        return;
                    });
                }(this.map, marker, ids[i]));*/
            }

            var activeLocation = activeCoords;
			
            this.activeMarker = new google.maps.Marker({
                icon: {
                    url: '/local/images/sprite_r.png',
                    size: new google.maps.Size(62, 65),
                    origin: new google.maps.Point(746, 345),
                    anchor: new google.maps.Point(25, 71)
                },
                position: new google.maps.LatLng(activeLocation[0], activeLocation[1]),
                map: this.map
            });
        },
		
		'.js-complex-detail-nav click': function(el, e){
			e.preventDefault();
			var target = e.target;
			// цикл двигается вверх от target к родителям до this
			while (target != el) {
				if ($(target).is('a.linkblock')) {				
					// нашли элемент, который нас интересует!
					el.find('a.linkblock').each(function (i) {
						if($(this).hasClass('selected')){
							$(this).removeClass('selected');
						}
					});
					$(target).addClass('selected');
					var classname = target.getAttribute('data-for');
					el.find('.block').each(function (i) {
						if(!$(this).hasClass('disabled')){
							$(this).addClass('disabled');
						}
					});
					el.find('.block.'+classname).removeClass('disabled');
					return;
				}
				else if($(target).is('li.house')){
					el.find('li.house').each(function (i) {	
						$(this).removeClass('active');				
					});
					
					this.element.find('#address_flat').html(target.getAttribute('data-location'))
					if(!$(target).hasClass('active')){
						$(target).addClass('active');
						var id_house = target.getAttribute('data-id');
						this.element.find('#id_house').attr('value', id_house);				
						this.element.trigger("list.contentUpdate", {});
					}
					return;
				}
				target = target.parentNode;
			}
		},
		
		'.js-pin click': function(el, e){
			e.preventDefault();
			var target = e.target;
						
			if(!el.hasClass('active')){
				
				this.element.find('.js-pin').each(function() {
					if($(this).hasClass('active')){
						$(this).removeClass('active');
					};
				});
				
				el.addClass('active');
				var id_house = el.data('id');
				this.element.find('#id_house').attr('value', id_house);				
				this.element.trigger("list.contentUpdate", {});
				
			}
			else{
				el.removeClass('active');
			}

		},
		
		'list.contentUpdate': function (el, e, param) {
            var $ajaxContent = this.element.find('.js-ajax-list-content');
            var data = [];

            if (param.page > 0) {
                data.push({
                    name: "page",
                    value: param.page
                })
            } else {
                data.push({
                    name: "page",
                    value: 1
                })
            }

            if (param.viewCounter > 0) {
                data.push({
                    name: "viewCounter",
                    value: param.viewCounter
                })
            }

            this.element.find(".js-sort a").each(function(){
                data.push({
                    name: "sort[" + $(this).data("type") + "]",
                    value: $(this).data("method")
                })
            });

            $.merge(data, this.element.find('.js-filter-form').serializeArray());

            var self = this;
			var filterParams = $('.js-filter-form').serializeArray();

                var validFilterParams = $.map(filterParams, function (el) {
                    return el.value.length > 0 ? el : null;
                });

                var getStr = $.map(validFilterParams, function (el) {
                    return el.name + "=" + el.value;
                }).join("&");

                var newUrl = location.origin + location.pathname + "?" + getStr;
                history.pushState({}, '', newUrl);
				$ajaxContent.ajaxl({
					url: location.pathname,
					data: data,
					dataType: 'HTML',
					type: 'POST',
					success: this.proxy(function (data) {
						$ajaxContent.html(data);
						$('.content-plan-block').removeClass('disabled');
			
						$.scrollTo($(".list-plan").offset().top - 200, 500);
						
					})
				});
        }
    });

})(jQuery, window.APP);