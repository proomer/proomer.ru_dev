   <?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
    </main>
    <?= EZendManager::sidebarBasket() ?>
    </div><!-- /.wrapper -->

    <footer>
        <div class="top">
            <div class="content-container">
                <div class="left-column">
                    <?Zend_Registry::get('BX_APPLICATION')->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        Array(
                            "ROOT_MENU_TYPE" => "bottom1",
                            "MAX_LEVEL" => "1",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(),
                        ),
                        false
                    );?>
                    <div class="copyright">&copy; <?= date('Y') ?> <? EHelper::includeArea("footer/copyright") ?></div>
                </div>

                <div class="center-column">
					<?/*Zend_Registry::get('BX_APPLICATION')->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        Array(
                            "ROOT_MENU_TYPE" => "bottom2",
                            "MAX_LEVEL" => "1",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(),
                        ),
                        false
                    );*/?>
					<div class="payment">
						<a class="visa" href="http://visa.com" title="Visa" target="_blank"></a>
						<a class="master" href="http://.mastercard.com/" title="Master Card" target="_blank"></a>
						<a class="webm" href="http://webmoney.com" title="Webmoney" target="_blank"></a>
						<a class="yandex" href="http://yandex.ru" title="Yandex" target="_blank"></a>
					</div>
                </div>

                <div class="right-column">
                      <!--<a class="to-top js-scroll-top" href="javascript:void(0);"></a>-->
					  <div class="socials" id="bx-composite-banner"></div>
                    <?//Zend_Registry::get('BX_APPLICATION')->IncludeComponent("sibirix:socials", "", array('CODE'=>'SOCIALS'), false);?>
					<div class="cont">г. Кемерово: <span class="number">+7 (996) 965-9457</span></div>
                </div>
            </div>
        </div>
        <!--<div class="bottom">
            <div class="content-container">
                <div class="sibirix">
                    <div class="slon">
                        <span class="slon-icon"></span>
                        <span class="js-slon-animation slon-animation"><img src="<?= P_IMAGES?>slon.gif"></span>
                    </div>

                    Разработка сайта — <a href="http://www.sibirix.ru" target="_blank" class="under-link">«Сибирикс»</a>
                </div>
            </div>
        </div>-->
    </footer>
	<? include(P_DR . P_APP . 'layout' . DIRECTORY_SEPARATOR . 'popups.php') ?>
	<? include(P_DR . P_APP . 'layout' . DIRECTORY_SEPARATOR . 'templates.php') ?>
	<?// include(P_DR . P_APP . 'layout' . DIRECTORY_SEPARATOR . 'footer-scripts.php') ?>
	<?if($_SERVER["REQUEST_URI"] == '/complex/'){?>
		<script data-skip-moving="true" src="https://<?= $_SERVER['SERVER_NAME'] ?>/widget/developer/0" type="text/javascript"></script>
	<?}?>
<?	
	// подключать скрипт будем только залогиненным пользователям
if($GLOBALS['USER']->isAuthorized()) {
   ?><script type="text/javascript"><!--
      // <этот кусок можно вынести в отдельный файл>
      var CActivityUpdate = function(sActivityUrl, iActivityTime) {
         var _this = this;
         this.sActivityUrl = sActivityUrl;
         this.iActivityTime = iActivityTime;

         // определение ajax-функции
         if(typeof(window['jQuery']['get']) == 'function') {
            // jQuery
            this.funcAjax = jQuery.get;
         } else if(typeof(window['BX']['ajax']['get']) == 'function') {
            // Bitrix
            this.funcAjax = BX.ajax.get;
         }

         // получение значения cookie
         this.getCookie = function(name) {
            var cookie = " " + document.cookie;
            var search = " " + name + "=";
            var setStr = null;
            var offset = 0;
            var end = 0;
            if(cookie.length > 0) {
               offset = cookie.indexOf(search);
               if(offset != -1) {
                  offset += search.length;
                  end = cookie.indexOf(";", offset)
                  if(end == -1) {
                     end = cookie.length;
                  }
                  setStr = unescape(cookie.substring(offset, end));
               }
            }
            return(setStr);
         };

         // установка значения cookie
         this.setCookie = function(sName, sValue, sExpires, sPath, sDomain, bSecure) {
            var sCookie = '';
            sCookie += sName + '=' + escape(sValue);
            sCookie += sExpires ? '; expires=' + sExpires : '';
            sCookie += sPath ? '; path=' + sPath : '';
            sCookie += sDomain ? '; domain=' + sDomain : '';
            sCookie += bSecure ? '; secure' : '';
            document.cookie = sCookie;
         };
         
         // выполнение "холостого" хита для обновления даты активности пользователя
         this.updateActivity = function() {
            var funcAjax = _this.funcAjax;
            if(funcAjax && typeof(funcAjax) == 'function') {
               var iLastActivity = _this.getCookie('BX_activity');
               iLastActivity = iLastActivity ? parseInt(iLastActivity) : 0;
               var oDate = new Date();
               var iCurTime = oDate.getTime();
               var bNeedUpdate = iCurTime >= (iLastActivity + _this.iActivityTime) || iCurTime < (iLastActivity - _this.iActivityTime);
               if(bNeedUpdate) {
                  var sExpires = false; //'Mon, 01-Jan-2018 00:00:00 GMT'
                  _this.setCookie('BX_activity', iCurTime, sExpires, '/');
                  funcAjax(_this.sActivityUrl);
               }
            }
         };

         // запуск обновления даты активности пользователя с заданным интервалом
         this.startUpdating = function() {
            if(_this.funcAjax && typeof(_this.funcAjax) == 'function') {
               setInterval(_this.updateActivity, _this.iActivityTime);
            }
         };
      };
      // </этот кусок можно вынести в отдельный файл>

      // запрашиваемая страница - /ajax_user_activity_update.php
      // интервал обновления - две минуты (в миллисекундах)
      var appActivityUpdate = new CActivityUpdate('/ajax_user_activity_update.php', (1000*60*2));
      appActivityUpdate.startUpdating();
   //--></script><?
}
	?>
</body>
</html>