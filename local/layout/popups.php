<div class="popups">
    <div class="popup feedback-popup" id="feedback">
        <h2>Написать нам</h2>
        <?= new Sibirix_Form_Feedback(); ?>
    </div>
	
	<div class="popup feedback-popup" id="messDesigner">
        <h2>Написать сообщение</h2>
        <?= new Sibirix_Form_MessDesigner(); ?>
    </div>

	<div class="popup feedbackbuilder-popup" id="feedbackbuilder">
        <h2>Написать нам</h2>
        <?= new Sibirix_Form_Feedbackbuilder(); ?>
    </div>

    <div class="popup success-popup" id="success">
        <h2></h2>
        <div class="text">
            <div class="descr"></div>
        </div>
    </div>
    <?
    $bxSite = new CSite();
    if($bxSite->InDir('/design/') || EHelper::isMain()):
        ?>
        <div class="search-service" id="searchService">
            <?= EZendManager::action("index", "search-service"); ?>
        </div>
    <? endif ?>
    <div class="popup info-popup" id="info-popup">
        <div class="popup-title">
            <h2><?= Settings::getOption('aboutDesignProductLink'); ?></h2>
            <div class="title-info" style="font: 300 22px/28px 'Roboto Slab', sans-serif;"><?= Settings::getOption('aboutDesignProductTitle'); ?></div>
        </div>
        <div class="info">
            <h3 class="title-info" style="font: 300 22px/28px 'Roboto Slab', sans-serif;"><?= Settings::getOption('aboutDesignProductText'); ?></h3>
            <div class="static-content" style="font: 400 14px/20px 'PT Sans', sans-serif;">
                <?= Settings::getOptionText('aboutDesignProductText'); ?>
            </div>
        </div>
    </div>

    <? if (!Sibirix_Model_User::isAuthorized()) { ?>
        <div class="popup register-popup" id="registration-popup">
            <?= new Sibirix_Form_Registration(); ?>
			<!-- <div class="socials">
                <div class="label">Войти с помощью:</div>

                <?/* $APPLICATION->IncludeComponent(
                    "sibirix:auth.authorize",
                    "custom",
                    Array(
                        "REGISTER_URL" => "/",
                        "PROFILE_URL" => "/profile/",
                        "SHOW_ERRORS" => "Y"
                    ),
                    false
                );*/?>
            </div>-->
        </div>

        <div class="popup login-popup" id="login-popup">
            <?= new Sibirix_Form_Auth(); ?>
            <!--<div class="socials">
                <div class="label">Войти с помощью:</div>

                <?/*$APPLICATION->IncludeComponent(
                    "sibirix:auth.authorize",
                    "custom",
                    Array(
                        "REGISTER_URL" => "/",
                        "PROFILE_URL" => "/profile/",
                        "SHOW_ERRORS" => "Y"
                    ),
                    false
                );*/?>
            </div>-->
        </div>

    <? } ?>

    <div class="popup register-popup" id="type-popup">
        <?= new Sibirix_Form_ProfileType(); ?>
    </div>

	<div class="popup planirovka-popup" id="planirovka">
		<div class="row">
			<?= new Sibirix_Form_Planirovka(); ?>
		</div>
    </div>

	<div class="popup address-popup" id="address-popup">
		<div class="row">
			<form enctype="application/x-www-form-urlencoded" class="js-address-form addressForm formTabber" data-title="Регистрация прошла успешно" data-text="" action="/search-service/step-plan-name/search
" method="post" novalidate="novalidate">
				<h2 class="win_title">Ваш адрес?</h2>
				<div class="input-row">
					<input type="text" name="address" id="address-popup-input" value="" class="required" autocomplete="off" placeholder="">
									<div class="result preloaderController"><ul>
				</ul></div>
				</div>
				<div class="btn-wrapper">
				<a class="btn blue waves-effect view_n3 js-search-form">Искать мой дом</a>
			</div>
			</form>
		</div>
    </div>

	<div class="popup super-man-popup" id="call-super-man">
		<div class="row">
			<?= new Sibirix_Form_SuperMan(); ?>
		</div>
    </div>

	<div class="popup give-reco-popup" id="give-reco" style="padding:23px;">
		<style>
			#give-reco .design-list{
				width:9999px;
			}
			#give-reco .feature-list{
				position: relative;
				overflow: auto;
			}
			#give-reco .design-list .item {
				margin: 0 27px 29px 0;
			}
			#give-reco .design-list .item:nth-child(3n) {
				margin: 0 0 29px 0;
			}			
			#give-reco .feature-list .out-feature-item {
				height: 400px;
				padding: 0 10px;
				width:278px;
				display:inline-block;
			}
			#give-reco .feature-list .out-feature-item{
				box-sizing: border-box; 
				margin-bottom: 18px; 
				overflow: hidden; 
				padding: 20px 20px; 
				position: relative;
			}
			#give-reco .feature-list .out-feature-item:hover .feature-item{
				border: 1px solid rgba(1, 1, 1, 1);
				cursor:pointer;
			}
			#give-reco .feature-list .out-feature-item img{
				max-height:241px;
			}
			#give-reco .feature-list {
				text-align: center;
			}
			.preview360-gallery, .preview360-gallery > img {
			  height: 238px;
			  width: 238px;
			}
			.preview360-gallery {
				height: 238px;
				width: 238px;
				position: relative;
			}
			.preview360-gallery > img {
				height: 238px;
				width: 238px;
				left: 0;
				position: absolute;
			}
			.preview360-hide {
			  z-index:9;
			}
		</style>
		<div class="feature-out">
		<?
		$designs = EHelper::getMyDesignList();
		if(count($designs->items) > 0){?>
			<div class="feature-list row">
				<div class="design-list" style="width:<?=278 * count($designs->items)?>px">
				<?
					//$profile = true;
					foreach($designs->items as $item){

				?>
						<div class="out-feature-item js-select-design" style="height: 521px;" data-id="<?=$item->ID?>">
						  <div class="feature-item">
							<div style="padding-bottom: 10px; height:246px;">
						
							  <?if (isset($item->GALLERY3D)) {
								$preview_360_one = $item->GALLERY3D[0];
								unset($design->GALLERY3D[0]);
								$preview_360_json = json_encode($item->GALLERY3D);
							  ?>
								<div class="preview360-gallery" data-json='<?=$preview_360_json;?>'>
								  <img src="<?=$preview_360_one;?>" class="preview360">
								</div>
							  <?} else{?>
								<img src="<?=$item->MAIN_IMAGE;?>">
							  <?} ?>

							</div>	
							<div style="position: relative; min-height: 60px">
							  <hr style="position: relative; top: 25px"/>
							  <img src="<?=$item->PERSONAL_PHOTO?>" style="border: 2px solid #fafafa; border-radius: 25px; height: 47px; width: 47px; z-index:9px; position: inherit; box-shadow: 0 0 0 1px rgba(51, 67, 75, 0.22);">

							</div>
							<div>
							  <span style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif;"><?=$item->DESIGNER->NAME ." ". $item->DESIGNER->LAST_NAME?></span>
							</div>
							<div style="height: 70px;">
								<a href="/design/<?=$item->CODE?>/" style="font-size: 15px; font-weight: bold; font-family: 'RobotoSlabRegular'; padding-top: 10px; min-height: 74px;"><?=$item->NAME?></a>
							</div>
							<div style="padding: 15px 0">
							  <hr style="position: relative; top: 2px"/>
							  <div style="background-color: #3fb1da; width: 47px; height: 3px; margin: auto; position: relative"></div>
							</div>
							
							<div class="row">
							  <div class="col-xs-6 col-sm-6 col-md-6">
								<p style="color: #7c7b7b; font: 300 13px/20px 'Fira Sans',sans-serif; text-align: left; padding-top: 6px">
								  <span style="background: rgba(0, 0, 0, 0) url('/local/images/sprite_r.png') repeat scroll -55px -349px; display: inline-block; height: 18px; margin-right: 0; vertical-align: middle; width: 20px;"></span> <?=(int) $item->PROPERTY_VIEW_COUNT_VALUE; ?>
								</p>
							  </div>
							</div>
						  </div>
						</div>											
					<?}?>
					<div class="clearfix"></div>
				</div>
			</div>
			<br/>
			<?}?>
			<div class="btn-wrapper">
				<a href="/profile/design/add/" class="btn blue waves-effect view_n3">Добавить дизайн-проект</a>
			</div>
		</div>
    </div>

    <div class="popup send-pass-popup" id="send-pass-popup">
        <?= new Sibirix_Form_Remind(); ?>
    </div>

    <div class="popup change-pass-popup" id="change-pass-popup">
        <?= new Sibirix_Form_ChangePassword(); ?>
    </div>

    <div class="popup delete-popup" id="delete-popup">
        <div class="popup-inner">
            Вы уверены, что хотите удалить эту фотографию?
        </div>
        <div class="btn-wrapper-double">
            <a href="javascript:void(0);" id="ok" class="btn blue waves-effect">Да</a>
            <a href="javascript:void(0);" id="cancel" class="btn blue waves-effect">Нет</a>
        </div>
    </div>

</div>
