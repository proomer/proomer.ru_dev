<?

// Подключение путей, констант и функций
require($_SERVER["DOCUMENT_ROOT"] . "/local/define.php");

// регистрируем последнюю активность пользователя, если модуль соцсетей не установлен
//if(!IsModuleInstalled('socialnetwork')) {
   AddEventHandler('main', 'OnBeforeProlog', 'CustomSetLastActivityDate');
   function CustomSetLastActivityDate() {
      if($GLOBALS['USER']->IsAuthorized()) {
         CUser::SetLastActivityDate($GLOBALS['USER']->GetID());
      }
   }
//}


