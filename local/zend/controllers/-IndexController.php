<?
/**
 * Контроллер главной страницы
 * Class IndexController
 */
class IndexController extends Sibirix_Controller {

    public function indexAction() {

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'main');
        Zend_Registry::get('BX_APPLICATION')->SetTitle("Proomer");
        $promoModel = new Sibirix_Model_Promo();
        $sliderModel = new Sibirix_Model_MainSlider();
        $complexModel = new Sibirix_Model_Complex();
		$designModel = new Sibirix_Model_Design();
		$modelRoom = new Sibirix_Model_Room();
		$goodsModel = new Sibirix_Model_Goods();
		$userModel = new Sibirix_Model_User();
		$planoptionModel     = new Sibirix_Model_PlanOption();
		$projectModel 		= new Sibirix_Model_Project();
		$apartModel    		 = new Sibirix_Model_ProjectApart();
		$roomModel     		 = new Sibirix_Model_ProjectRoom();

		$modelSibirix_Model_Goods = new Sibirix_Model_Goods();
		$modelPin = new Sibirix_Model_Pin();
		$design = $designModel->where(["CODE" => "klassika"])->getElement();
		$design->ROOMS = $modelRoom->where(['PROPERTY_DESIGN' => $design->ID])->getElements();
		$modelSibirix_Model_Goods->getImageData($design->ROOMS, ["PROPERTY_IMAGES_VALUE"]);

		//Получаем пины
		foreach($design->ROOMS as $room){
			$room->PINS = $modelPin->getPin($room->ID);
			foreach($room->PINS as $pin){
				//x = 488px - 100%; y = 493px - 100%;
				$coords = explode(",", $pin->PROPERTY_COORDS_VALUE[0]);
				//$y_y = 569 / 493;
				//$x_x = 1024 / 488;

				$x = $coords[0] - 16;
				$y = $coords[1] + 16;
				$pin->X = $x;
				$pin->Y = $y;
			}
		}
		foreach($design->ROOMS as $room){
			$modelSibirix_Model_Goods->getImageData($room->PINS, ["PREVIEW_PICTURE"]);
		}
        $promo = $promoModel->getItems();
        $slides = $sliderModel->getItems();
        $complexes = $complexModel->getSliderList();

		//для блока мастеров
		$users = $userModel->getUserList(['GROUPS_ID'=>5], ['PERSONAL_PHOTO'=>'DESC'], 1, false, 6);

		foreach($users->items as $user){
			if (!empty($user->PERSONAL_PHOTO)) {
				$image = Resizer::resizeImage($user->PERSONAL_PHOTO, 'MAIN_PAGE_DESIGNERS_PREVIEW');
			} else {
				$image = '/local/images/proomer2.png';
			}
			$user->PERSONAL_PHOTO = $image;
		}

		/*===================================================*/

		/*=====================Конкурсы======================*/

		$competitionList = $apartModel->getProjectApartbList(['ID', 'NAME', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER'],[]);
		$competitionList2 = $roomModel->getProjectRoombList(['ID', 'NAME', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'PROPERTY_TIME', 'PROPERTY_ID_ORDER'],[]);
	
		$competitions->items = array_merge($competitionList->items, $competitionList2->items);

		foreach($competitions->items as $item){
			$item->PRICE_VALUE = CPrice::GetBasePrice($item->ID);

			$project = $projectModel->select(['ID', 'PROPERTY_ID_OPTION_PLAN'], true)->getElement($item->PROPERTY_ID_ORDER);
			
			$planOption = $planoptionModel->select(['ID', 'PROPERTY_PLAN_FLAT', 'PROPERTY_IMAGES'], true)->getElement($project->PROPERTY_ID_OPTION_PLAN);
			
			$item->IMG = $planOption->PROPERTY_IMAGES;
			
			if (!empty($item->IMG)) {
				$image = Resizer::resizeImage($item->IMG, 'PLAN_LIST');
			} else {
				$image = '/local/images/img-not-found.jpg';
			}
			$item->IMG = $image;
		}

		/*===================================================*/

    /* случайный показ дизайнов с 3d просмотром */

    $fields = array("PROPERTY_IMG_3", "CREATED_BY", "DETAIL_PICTURE", "NAME", "PROPERTY_VIEW_COUNT", "CODE");

    $randDesignListTemp = $designModel->getDesignRand($fields, 4);
    $randDesignList = array();
	
    foreach ($randDesignListTemp as $randDesignTemp)
    {
		
      /* формируем выходные данные для вьюхи */
      $randDesign = array();
      $randDesign['PERSONAL_PHOTO'] = $randDesignTemp->DESIGNER->PERSONAL_PHOTO['SRC'] ? $randDesignTemp->DESIGNER->PERSONAL_PHOTO['SRC'] : '/local/images/no-avatar-small.png'; // фото дизайнера
      $randDesign['DESIGNER_NAME'] = $randDesignTemp->DESIGNER->NAME ." ". $randDesignTemp->DESIGNER->LAST_NAME; // имя дизайнера
      $randDesign['NAME'] = $randDesignTemp->NAME; // имя дизайна
      $randDesign['PRICE'] = $randDesignTemp->PRICE; // цена дизайна
	  $randDesign['VIEW_COUNT'] = $randDesignTemp->PROPERTY_VIEW_COUNT_VALUE ? $randDesignTemp->PROPERTY_VIEW_COUNT_VALUE : 0; // количество просмотров
	  $randDesign['CODE'] = $randDesignTemp->CODE; // код для ссылки

      /* решаем показать главную картинку или 3d просмотр*/
      if (!$randDesignTemp->PROPERTY_IMG_3_VALUE) {
        $randDesign['MAIN_IMAGE'] = Resizer::resizeImage($randDesignTemp->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
      } else {
        $period = 32/8;
        $gallery3d = array();
        for ($i=1; $i < 8+1; $i++) {
          $cur_index = $i * $period;
          $gallery3d[] = Resizer::resizeImage($randDesignTemp->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
        }
        $randDesign['GALLERY3D'] = $gallery3d;
      }
      $randDesignList[] = $randDesign;
    }

    /* конец случайного показа дизайнов с 3d просмотром*/

		$goods = $goodsModel->getProductList([], 'SHOW_COUNTER', 4);
        $this->view->assign([
            'slides' => $slides,
			'userList' => $users->items,
            'promo' => $promo,
            'complexes' => $complexes,
			'competitions' => $competitions,
			'design' => $design,
			'goods' => $goods->items,
      'rand_design' => $randDesignList // случайные дизайны
        ]);

    }

    public function sliderRefreshAction() {
        $complexId = $this->getParam('complexId');
        $elementCnt = Settings::getOption('slidesDesignsElementsCount', 12);
        $designModel = new Sibirix_Model_Design();
        $designs = $designModel->getSliderItems($complexId, $elementCnt);

        $this->view->designs = $designs;
        $this->view->complexId = $complexId;
        $this->_response->stopBitrix(true);
    }

}
