<?

/**
 * Виджет сервиса
 * Class WidgetController
 */
class BuilderController extends Sibirix_Controller {
	
	protected $_model;
	 
	public function init() {
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('index', 'html')
		->initContext();

        $this->_model  = new Sibirix_Model_Builder();
    }

	public function indexAction() {
		
	}
	
	public function builderListAction(){
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'builder-list');
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('builder-list', 'html')
            ->initContext();
		
		$builderModel = new Sibirix_Model_Builder();
		$complexModel = new Sibirix_Model_Complex();
		$cityModel    = new Sibirix_Model_City();
		$userModel	  = new Sibirix_Model_User();
        $cityList     = $cityModel->getElements();
		$userLocation = $userModel::getUserLocation();
		$city = array();
		$city['NAME'] = 'Кемерово'; 
		$city['ID'] = 23;
				
		foreach ($cityList as $item){
			if ($item->ID == $userLocation){
				$city['NAME'] = $item->NAME;
				$city['ID'] = $item->ID;
			}
		};
		Zend_Registry::get('BX_APPLICATION')->SetTitle(("Застройщики города ".$city['NAME']));
		$q = $this->getParam('q');		
        $complexModel = new Sibirix_Model_Complex();
		
		if(!empty($q)){
			$filter = [
				[
					"LOGIC" => "OR",
					["NAME" => "%".$q."%"],
					["PROPERTY_OFFICE" => "%".$q."%"],
				],
				"PROPERTY_CITY" => $city['ID']
			];
		}
		else{
			$filter = ["PROPERTY_CITY" => $city['ID']];
		}
		
		$catalogSort["SORT"] = "ASC";
			
	
		$ARR_BUILDER = $builderModel->getBuilderList([
														'ID',
														'NAME',
														'CODE',
														'PREVIEW_PICTURE',
														'PREVIEW_TEXT',
														'PROPERTY_LOCATION'
													 ],$filter, $catalogSort, $this->getParam("page"), 5);
		

		//получим путь картинки
		foreach($ARR_BUILDER->items as $builder){
		
			if (!empty($builder->PREVIEW_PICTURE)) {
				$image = Resizer::resizeImage($builder->PREVIEW_PICTURE, 'BUILDER_PREVIEW_PICTURE');
			} else {
				$image = '';
			}
				
			$builder->PREVIEW_PICTURE = $image;
			$catalogSort["DATA_CREATED"] = "DESC";
			$ARR_COMPLEX = $complexModel->getComplexbList([
															'ID',
															'NAME',
															'CODE',
															'DETAIL_PICTURE'
														  ],['PROPERTY_DEVELOPER' => $builder->ID], $catalogSort);
														  
			foreach($ARR_COMPLEX->items as $complex){
				if (!empty($complex->DETAIL_PICTURE)) {
					$image = Resizer::resizeImage($complex->DETAIL_PICTURE, 'COMPLEX_PREVIEW');
				} else {
					$image = '';
				}
				$complex->IMG = $image;
			}
			$builder->COMPLEX = $ARR_COMPLEX;
		}

		$this->view->assign([
            "ARR_BUILDER" 		=> $ARR_BUILDER,
			"city"		=> $city,
			'paginator' => EHelper::getPaginator($ARR_BUILDER->pageData->totalItemsCount, $ARR_BUILDER->pageData->current, $ARR_BUILDER->pageData->size),
        ]);
		
	}
	
	public function builderDetailAction(){
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'builder-detail');
		
		$builder_id = $this->getParam('builder_id');
		
		if($builder_id){
			$builderModel = new Sibirix_Model_Builder();
			$complexModel = new Sibirix_Model_Complex();
			$cityModel    = new Sibirix_Model_City();
			$userModel	  = new Sibirix_Model_User();
			
			$filter = [
				[
					"LOGIC" => "OR",
					["=ID" => $builder_id],
					["=CODE" => $builder_id],
				],
				"PROPERTY_CITY" => $city['ID']
			];
			
			$catalogSort["SORT"] = "ASC";
			
			$builder = $builderModel->select(["ID", 
											  "NAME", 
											  "DETAIL_TEXT", 
											  "PREVIEW_TEXT",
											  "PREVIEW_PICTURE",
											  "PROPERTY_POSITION_MAP_CENTER",
											  "PROPERTY_SITE_CENTER",
											  "PROPERTY_E_MAIL_CENTER",
											  "PROPERTY_NUMBER_CENTER",
											  "PROPERTY_ADDRESS_CENTER",
											  "PROPERTY_POSITION_MAP_SALE",
											  "PROPERTY_E_MAIL_SALE",
											  "PROPERTY_NUMBER_SALE",
											  "PROPERTY_ADDRESS_SALE",
											  "PROPERTY_SITE"], true)->where($filter, true)->getElement();
											  
			if (!empty($builder->PREVIEW_PICTURE)) {
				$image = Resizer::resizeImage($builder->PREVIEW_PICTURE, 'BUILDER_PREVIEW_PICTURE');
			} else {
				$image = '';
			}
			$builder->PREVIEW_PICTURE = $image;
			
			
			Zend_Registry::get('BX_APPLICATION')->SetTitle($builder->NAME);
			$complexList = $complexModel->getComplexbList(['ID',
														'CODE',
														'IBLOCK_ID',
														'NAME',
														'DETAIL_TEXT',
														'PREVIEW_TEXT',
														'DETAIL_PICTURE',
														'PROPERTY_CONSTRUCTOR',
														'PROPERTY_DESIGN_CNT',
														'PROPERTY_PHOTO_COMPLEX',
														'PROPERTY_COMPLEX_PLAN',
														'PROPERTY_API'],
														['PROPERTY_DEVELOPER' => $builder->ID], $catalogSort);
			
		
			
		}
		else{
			throw new Zend_Exception('Not found', 404);
		};
		
		$this->view->assign([
			"builder"		=> $builder,
			"complexList"	=> $complexList		
		]);
		
	}
	public function feedbackAction() {
        $form = new Sibirix_Form_Feedback();
        $formData = $this->getAllParams();

        if ($form->isValid($formData)) {
            $validData = $form->getValues();

            if (!$form->antiBotCheck()) {
                $this->_response->stopBitrix(true);
                $this->_helper->viewRenderer->setNoRender();
                return false;
            }

            $addResult = $this->_model->add($validData);

            if ($addResult) {
                $notification = new Sibirix_Model_Notification();
                $emailTo = Settings::getOption("FEEDBACK_EMAIL_TO");
                $titleMail = "Новое сообщение";

                $notification->sendFeedback($addResult, $validData, $emailTo, $titleMail);
            } else {
                $form->setFieldErrors("name", "Ошибка добавления");
            }

        } else {
            $form->getFieldsErrors();
        }

        $this->_helper->json(['success' => !$form->issetError(), 'errorFields' => $form->formErrors]);
    }

}