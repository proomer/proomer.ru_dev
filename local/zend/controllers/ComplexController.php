<?

/**
 * Контроллер страницы жилых компексов
 * Class ComplexController
 */
class ComplexController extends Sibirix_Controller {

    /**
     * @var Sibirix_Model_Complex
     */
    protected $_model;

    public function init() {
        /**
         * @var $ajaxContext Sibirix_Controller_Action_Helper_SibirixAjaxContext
         */
        $ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('complex-list', 'html')
            ->initContext();

        $this->_model = new Sibirix_Model_Complex();
    }

    public function complexListAction() {
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'complex-list');
        Zend_Registry::get('BX_APPLICATION')->SetTitle("Список жилых комплексов");
		
        if ($this->getParam("viewCounter") > 0) {
            Sibirix_Model_ViewCounter::setViewCounter($this->getParam("viewCounter"));
            $this->_model->reinitViewCounter();
        }

        $sortData = $this->getParam("sort");

        if (!empty($sortData)) {
            if (!empty($sortData["popular"])) {
                $catalogSort["PROPERTY_DESIGN_CNT"] = strtoupper($sortData["popular"]);
            }
            if (!empty($sortData["price"])) {
                $catalogSort["PROPERTY_AVERAGE_DESIGN_PRICE"] = strtoupper($sortData["price"]);
            }
        }
        $catalogSort["SORT"] = "ASC";

        $filter = new Sibirix_Form_FilterComplex();

        $filterParams = $this->getAllParams();

        if ($filterParams["avgPriceMin"] === null || $filterParams["avgPriceMax"] === null) {
            $filterParams["avgPrice"] = $this->_model->getExtremeAvgPrice();
        } else {
            $filterParams["avgPrice"] = array(
                $filterParams["avgPriceMin"],
                $filterParams["avgPriceMax"]
            );
        }

        $filter->populate($filterParams);

        $catalogFilter = $this->_model->prepareFilter($filter->getValues());
		
        $result = $this->_model->getComplexList($catalogFilter, $catalogSort, $this->getParam("page", 1));
		
        $this->view->assign([
            "filter"    => $filter,
            "itemList"  => $result->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
    }

    public function detailAction() {

        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'complex-detail');
		/*=============================================
			Определим контекст для AJAX
		=============================================*/
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
		$ajaxContext->addActionContext('detail', 'html')
		->initContext();
		/*==========================================*/
		
        \Bitrix\Main\Page\Asset::getInstance()->addJs(GOOGLE_MAPS);
		
        $elementCode = $this->getParam("elementCode");
        $complex = $this->_model->select([
											'ID',
											'CODE',
											'IBLOCK_ID',
											'NAME',
											'DETAIL_TEXT',
											'DETAIL_PICTURE',
											'PROPERTY_CONSTRUCTOR',
											'PROPERTY_DEVELOPER',
											'PROPERTY_DESIGN_CNT',
											'PROPERTY_PHOTO_COMPLEX',
											'PROPERTY_COMPLEX_PLAN',
											'PROPERTY_API'
										],true)->where(["CODE" => $elementCode])->getElement();
						
        if (!$complex) {
            throw new Zend_Exception('Not found', 404);
        }
	
        //$this->_model->getImageData($complex, "DETAIL_PICTURE");
		
		$arr_img = array();
		
		foreach($complex->PROPERTY_PHOTO_COMPLEX_VALUE as $img){
			if (!empty($img)) {
				$image = CFile::GetFileArray($img);
			} else {
				$image = '';
			}
			array_push($arr_img, $image);
		}
		
		$complex->PHOTO_COMPLEX = $arr_img;
		$complex->PROPERTY_COMPLEX_PLAN_VALUE = Resizer::resizeImage($complex->PROPERTY_COMPLEX_PLAN_VALUE, 'COMPLEX_PLAN');

		//$this->_model->getImageData($complex, "PROPERTY_PHOTO_COMPLEX_VALUE");
        $this->_model->getSeoElementParams($complex);

        $houseModel = new Sibirix_Model_House();
		$entranceModel  = new Sibirix_Model_Entrance();
		$floorModel  = new Sibirix_Model_Floor();
		$planModel  = new Sibirix_Model_Plan();
		$flatModel = new Sibirix_Model_Flat();
		$builderModel  = new Sibirix_Model_Builder();
        $houseList = $houseModel
            ->where(["PROPERTY_COMPLEX" => $complex->ID])
            ->getElements();

		$filterParams = $this->getAllParams();
		
		$id_house = (int) $this->getParam('id_house');
		$house = array();

		if($id_house > 0){
			$house = $houseModel->getElement($id_house);
		};

		$ARR_FLAT = $houseModel->getFlatListb($id_house);
		
		foreach($houseList as $house){	
			// Подъезды дома
			$flats = array('ONE_ROOMS' => array(), 'TWO_ROOMS' => array(), 'THREE_ROOMS' => array(), 'FOUR_ROOMS' => array(), 'FIVE_ROOMS' => array(), 'SIX_ROOMS' => array());
			$entrances->ENTRANCES = $houseModel->select(['ID', 'NAME'], true)->where(['=PROPERTY_HOUSE' => $house->ID], true)->getElements();
			foreach($entrances->ENTRANCES as $entrance)
			{	
				// Этажи подъезда
				$entrance->FLOORS = $floorModel->select(['ID', 'NAME', 'PROPERTY_FLOOR_PLAN'], true)->where(["=PROPERTY_ENTRANCE" => $entrance->ID])->getElements();
				foreach ($entrance->FLOORS as $floor)
				{			
					// Квартиры этажа
					$floor->APARTMENTS = $flatModel->select(['ID', 'NAME', 'PROPERTY_COORDS', 'PROPERTY_ROOM_CNT', 'PROPERTY_PLAN'], true)->where(["=PROPERTY_FLOOR" => $floor->ID])->getElements();	
					foreach($floor->APARTMENTS as $flat){		
						if($flat->PROPERTY_ROOM_CNT_VALUE == 1){
							array_push($flats['ONE_ROOMS'], $flat);			
						}
						else if($flat->PROPERTY_ROOM_CNT_VALUE == 2){
							array_push($flats['TWO_ROOMS'], $flat);			
						}
						if($flat->PROPERTY_ROOM_CNT_VALUE == 3){
							array_push($flats['THREE_ROOMS'], $flat);			
						}
						if($flat->PROPERTY_ROOM_CNT_VALUE == 4){
							array_push($flats['FOUR_ROOMS'], $flat);			
						}
						if($flat->PROPERTY_ROOM_CNT_VALUE == 5){
							array_push($flats['FIVE_ROOMS'], $flat);			
						}
						if($flat->PROPERTY_ROOM_CNT_VALUE == 6){
							array_push($flats['SIX_ROOMS'], $flat);			
						}
					}
						
				}
			}
					

			$house->FLATS = $flats;			
		}
		
		$planIds = array();
		
		foreach($house->FLATS as $a_flats){
			$plan_ids = array_map(function ($obj) {
					return $obj->PROPERTY_PLAN_VALUE;
			}, $a_flats);

			foreach($plan_ids as $id){
				array_push($planIds, $id);
			}
		}

        $catalogFilter = $planModel->prepareFilter($filterParams);
		
		$catalogFilter['=ID'] = $planIds;
		if($this->getParam('page')){
			//$limit = $this->getParam('page') * 12;
		}
		else{
			//$limit = 12;
		}
		$sort = 'ASC';
        $this->_model->getImageData($houseList, "PROPERTY_PLAN_VALUE");
		if(!empty($planIds)){
		$planList = $planModel->getPlanbList([
												'ID', 
												'NAME',
												'CODE',
												'PROPERTY_AREA', 
												'PROPERTY_IMAGES',
												'PROPERTY_ROOM'
											 ], $catalogFilter, $sort);

        }
	
		$complex->HOUSES = $houseList;

        $flats = $houseModel->getFlatList($houseList[0]->ID);
		
        $complex->FIRST_HOUSE_FLATS = $flats;

        $designModel = new Sibirix_Model_Design();
        $elementCnt = Settings::getOption('designSliderCount', 20);

        $complex->DESIGNS = $designModel->getSliderItems($complex->ID, $elementCnt);

        if (empty($complex->PROPERTY_SIMILAR_COMPLEX_VALUE)) {
            $complex->SIMILAR_COMPLEX = [];
        } else {
            $complex->SIMILAR_COMPLEX = $this->_model->where(['ID' => $complex->PROPERTY_SIMILAR_COMPLEX_VALUE])->getElements();
        }

        $this->_setSeoElementParams($complex);

		$builder = $builderModel->getElement($complex->PROPERTY_DEVELOPER_VALUE);
		$complex->BUILDER_CODE = $builder->CODE;
			
		$this->view->assign([
            "pageTitle" => $pageTitle,
            "planList"  => $planList,
			"complex"	=> $complex,
			"house"		=> $house
		]);	
    }
	
	public function planAction() {
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'complex-detail-plan');
		Zend_Registry::get('BX_APPLICATION')->SetTitle("Планировка квартиры");
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('plan', 'html')
            ->initContext();
		
		$builderModel 	= new Sibirix_Model_Builder();
		$complexModel 	= new Sibirix_Model_Complex();
		$planModel 		= new Sibirix_Model_Plan();
		$planoptionModel = new Sibirix_Model_PlanOption();
		$designModel	= new Sibirix_Model_Design();
		//выбраноое планировочное решение
		
		$option = (int) $this->getParam('option');
		/*====================================================*/
			//Комплекс elementCode
		/*====================================================*/
		$elementCode = $this->getParam("elementCode");
		
		$complex = $complexModel->select([
											'ID',
											'NAME',
											'CODE',
											'PROPERTY_DEVELOPER'
										],true)->where(["CODE" => $elementCode])->getElement();
		
        if (!$complex) {
            throw new Zend_Exception('Not found', 404);
        }
		/*=================================================*/
			//План/квартира elementCode2
		/*================================================*/
		$elementCode2 = $this->getParam("elementCode2");
		
		$filter = [
            [
                "LOGIC" => "OR",
                ["ID=" => $elementCode2],
                ["CODE" => $elementCode2],
            ],
        ];

		//планировка
		$PLAN = $planModel->getPlanComplex($complex->ID, $elementCode2);

		$PLAN->PROPERTY_PLAN_SIZE_VALUE = Resizer::resizeImage($PLAN->PROPERTY_PLAN_SIZE_VALUE, 'PLAN_PREVIEW_PHOTO');
		if($PLAN->PROPERTY_SKAN_VALUE){
			$planModel->getImageData($PLAN, 'PROPERTY_SKAN_VALUE');
		};
		$PLAN->PROPERTY_DOCUMENTS_VALUE = CFile::GetPath($PLAN->PROPERTY_DOCUMENTS_VALUE[0]);
		//планировочные решения 
		if(!$PLAN){
			Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-class', 'not-found-page');
            throw new Zend_Exception('Not found', 404);
		}

		$PLAN_OPTIONS = $planoptionModel->getPlanOptionbList(
																[
																	'ID',
																	'NAME',
																	'CODE',
																	'PROPERTY_PLAN_FLAT',
																	'PROPERTY_IMAGES',
																	'PROPERTY_ROOM',
																	'PROPERTY_AREA'
																],
																[
																	'PROPERTY_PLAN_FLAT' => $PLAN->ID
																]
															);
														
		foreach($PLAN_OPTIONS->items as $item){
			if (!empty($item->PROPERTY_IMAGES_VALUE)) {
				$image = Resizer::resizeImage($item->PROPERTY_IMAGES_VALUE, 'PLAN_PREVIEW_PHOTO');
			} else {
				$image = '';
			}
			$item->PROPERTY_IMAGES_VALUE = $image;
		}
	
		if($option > 0){

			//дизайн-проекты для планировочного решения
			$DESIGN_OPTION 	= $designModel->getDesignbList(
																[
																	'ID',
																	'NAME',
																	'CODE',
																	'CREATED_BY',
																	'PROPERTY_CREATED_BY',
																	'PREVIEW_PICTURE',
																	'DETAIL_PICTURE',
																	'PROPERTY_PLAN',
																	'PROPERTY_IMG_3',
																	'PROPERTY_VIEW_COUNT'																
																],
																['=PROPERTY_PLAN' => $option,
																 '=PROPERTY_STATUS' => DESIGN_STATUS_PUBLISHED
																]
															);
			
			$designModel->_getDesignInfo($DESIGN_OPTION->items);

			foreach($DESIGN_OPTION->items as $design){
				if (!$design->PROPERTY_IMG_3_VALUE) {
					$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
				} else {
					$period = 32/8;
					$gallery3d = array();
					for ($i=1; $i < 8+1; $i++) {
						$cur_index = $i * $period;
						$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
					}
					$design->GALLERY3D = $gallery3d;
				}
				$img = Resizer::resizeImage($design->DESIGNER->PERSONAL_PHOTO['ID'], 'DESIGN_DESIGNER_PHOTO');				
				$design->DESIGNER->PERSONAL_PHOTO['SRC'] = $img;
				$design->DESIGNER->PHOTO = $img;
			};
									
			//дизайн-проекты для квартиры
			$planIds = array_map(function ($obj) {
					return $obj->ID;
					}, $PLAN_OPTIONS->items);
			$planIds = array_unique($planIds);

			$DESIGN_FLAT = $designModel->getDesignbList(
															[
																'ID',
																'NAME',
																'CODE',
																'CREATED_BY',
																'PROPERTY_CREATED_BY',
																'PREVIEW_PICTURE',
																'DETAIL_PICTURE',
																'PROPERTY_PLAN',
																'PROPERTY_IMG_3',
																'PROPERTY_VIEW_COUNT'
															],
															['=PROPERTY_PLAN' => $planIds,
															 '=PROPERTY_STATUS' => DESIGN_STATUS_PUBLISHED
															]
														);
														
			$designModel->_getDesignInfo($DESIGN_FLAT->items);
			
			foreach($DESIGN_FLAT->items as $design){
				if (!$design->PROPERTY_IMG_3_VALUE) {
					$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
				} else {
					$period = 32/8;
					$gallery3d = array();
					for ($i=1; $i < 8+1; $i++) {
						$cur_index = $i * $period;
						$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
					}
					$design->GALLERY3D = $gallery3d;
				}
				$img = Resizer::resizeImage($design->DESIGNER->PERSONAL_PHOTO['ID'], 'DESIGN_DESIGNER_PHOTO');			
				$design->DESIGNER->PERSONAL_PHOTO['SRC'] = $img;	
				$design->DESIGNER->PHOTO = $img;				
			};
													
		}

		$builder = $builderModel->select(['ID', 'CODE', 'NAME'], true)->getElement($complex->PROPERTY_DEVELOPER_VALUE);
	
		$this->view->assign([
			"complex"		=> $complex,
			"builder"		=> $builder,
			"plan"			=> $PLAN,
			"option"		=> $option,
			"planOptions"	=> $PLAN_OPTIONS,
			"designOptions"	=> $DESIGN_OPTION,
			"designFlat"	=> $DESIGN_FLAT
		]);
    }
	
    public function flatCountAction() {
        $houseId = $this->getParam('houseId');
        $houseModel = new Sibirix_Model_House();
        $flats = $houseModel->getFlatList($houseId);

        $this->view->flats = $flats;
        $this->_response->stopBitrix(true);
    }
}