<?

/**
 * Контроллер страницы дизайнера
 * Class DesignerController
 */
class DesignerController extends Sibirix_Controller {
    /**
     * @var Sibirix_Model_User
     */
    protected $_model;

    public function init() {
        /**
         * @var $ajaxContext Sibirix_Controller_Action_Helper_SibirixAjaxContext
         */
        $ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('detail', 'html')
            ->initContext();

        $this->_model = new Sibirix_Model_User();
    }
	
	public function listAction() {
			
		Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'designer');
        Zend_Registry::get('BX_APPLICATION')->SetTitle("Список дизайнеров");
		
		$ajaxContext = $this->_helper->getHelper('SibirixAjaxContext');
        $ajaxContext->addActionContext('list', 'html')
            ->initContext();
			
		$designModel = new Sibirix_Model_Design();	
		
		//лимит
		//Sibirix_Model_ViewCounter::setViewCounter(5);
		//$this->_model->reinitViewCounter();		
		
		$sortData = $this->getParam("sort");
		$searchData = $this->getParam("q");

        if (!empty($sortData)) {
            if (!empty($sortData["name"])) {
                $catalogSort["NAME"] = $sortData["name"];
            }
        }
        $catalogSort["SORT"] = "ASC";
		

		/*$filter = [
			[
				"LOGIC" => "OR",
				["NAME" => "%" . $searchData . "%"]
			],
			'GROUPS_ID'=>5,
		];*/
		/*$filter = [
					[
						"LOGIC" => "OR",
						["NAME" => "%" . $searchData . "%"],
						["LAST_NAME" => "%" . $searchData . "%"],
					],
					'GROUPS_ID'=>5,
				];*/
						
		//$filter[] = ["LOGIC" => "OR",["NAME" => "%" . $searchData . "%"]];
		//$filter = array('GROUPS_ID' => 5);
		$filter = [
			'GROUPS_ID'=>5,
		];
		if($searchData){
			$filter['NAME'] = '%'.$searchData.'%';
		}

		//массив дизайнеров
		$result = $this->_model->getUserList($filter, $catalogSort, $this->getParam("page", 1), false, 5);
		
		//получаем доп.значения
		foreach($result->items as $user){
			if (!empty($user->PERSONAL_PHOTO)) {
				$image = Resizer::resizeImage($user->PERSONAL_PHOTO, 'MAIN_PAGE_DESIGNERS_PREVIEW');
			} else {
				$image = '/local/images/design.jpg';
			}
			$user->PERSONAL_PHOTO = $image;
			$designs = $designModel->select(['ID', 'CODE', 'NAME', 'CREATED_BY', 'PROPERTY_CREATED_BY', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 
											'PROPERTY_VIEW_COUNT', 'PROPERTY_IMG_3', 'PROPERTY_STATUS'], true)->where(['=PROPERTY_CREATED_BY' => $user->ID])->limit(3)->getElements();
		
			if(count($designs) > 0){
				/* решаем показать главную картинку или 3d просмотр*/
				foreach($designs as $design){
					
					if (!$design->PROPERTY_IMG_3_VALUE) {
						$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
					} else {						
						$period = 32/8;
						$gallery3d = array();
						for ($i=1; $i < 8+1; $i++) {
							$cur_index = $i * $period;
							$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
						}
						$design->GALLERY3D = $gallery3d;
					
					};
				}
				$designModel->_getDesignInfo($designs);	
			}
			$user->DESIGN = $designs;
		}
		
        $this->view->assign([
            "pageTitle" => $pageTitle,
            "filter"    => $filter,
            "itemList"  => $result->items,
            'paginator' => EHelper::getPaginator($result->pageData->totalItemsCount, $result->pageData->current, $result->pageData->size),
        ]);
    }
	
	public function detailAction() {
        Zend_Registry::get('BX_APPLICATION')->SetPageProperty('page-type', 'designer');

        $elementId = $this->getParam("elementId");

        $designer = $this->_model->where(["ID" => $elementId])->getElement();
        if (!$designer || $designer->getType() != DESIGNER_TYPE_ID) {
            throw new Zend_Exception('Not found', 404);
        }

        Zend_Registry::get('BX_APPLICATION')->SetTitle($designer->getFullName());

        $this->_model->getImageData($designer, 'PERSONAL_PHOTO');

        //Получаем стили
        if (!empty($designer->UF_STYLES)) {
            $styleList = Sibirix_Model_Reference::getReference(HL_STYLE, array("UF_NAME"), "ID");

            $propertyStylesValue = array();
            foreach ($designer->UF_STYLES as $key => $stylesValue) {
                $propertyStylesValue[$key] = $styleList[$stylesValue];
            }
            $designer->UF_STYLES = $propertyStylesValue;
        }

        $sort = ["SORT" => "ASC"];


        $designModel = new Sibirix_Model_Design();
        $designModel->reinitPageSize(Settings::getOption('designerPageSize', 18));

        $designs = $designModel->getDesignList(["PROPERTY_CREATED_BY" => $designer->ID], $sort, $this->getParam("page", 1));
		
		foreach($designs->items as $design){
			if (!$design->PROPERTY_IMG_3_VALUE) {
				$design->MAIN_IMAGE = Resizer::resizeImage($design->DETAIL_PICTURE, 'PREVIEW_360_SMALL');
			} else {
				$period = 32/8;
				$gallery3d = array();
				for ($i=1; $i < 8+1; $i++) {
					$cur_index = $i * $period;
					$gallery3d[] = Resizer::resizeImage($design->PROPERTY_IMG_3_VALUE[$cur_index], 'PREVIEW_360_SMALL');
				}
				$design->GALLERY3D = $gallery3d;
			}						
		};
		
        $this->view->assign([
            'designer' => $designer,
            'designs' => $designs,
            'paginator' => EHelper::getPaginator($designs->pageData->totalItemsCount, $designs->pageData->current, $designs->pageData->size),
        ]);
    }
	
	public function feedbackAction() {
        $form = new Sibirix_Form_MessDesigner();
		$model  = new Sibirix_Model_FeedbackDesigner();
		$userModel   = new Sibirix_Model_User();
        $formData = $this->getAllParams();
		$designerId = (int) $formData['designerId'];
		$user = $userModel->findById($designerId);
	
        if ($form->isValid($formData)) {
            $validData = $form->getValues();

            if (!$form->antiBotCheck()) {
                $this->_response->stopBitrix(true);
                $this->_helper->viewRenderer->setNoRender();
                return false;
            }

            $addResult = $model->add($validData);

            if ($addResult) {
                $notification = new Sibirix_Model_Notification();
                $emailTo = $user["EMAIL"];
                $titleMail = "Новое сообщение";

                $notification->sendFeedback($addResult, $validData, $emailTo, $titleMail);
            } else {
                $form->setFieldErrors("name", "Ошибка добавления");
            }

        } else {
            $form->getFieldsErrors();
        }

        $this->_helper->json(['success' => !$form->issetError(), 'errorFields' => $form->formErrors]);
    }
}