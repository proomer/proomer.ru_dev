<?

/**
 * Class DeveloperController
 */
class DeveloperController extends Sibirix_Controller {

    public function indexAction() {
		$rsUsers = CUser::GetList();
		$userList = array();
		while ($arUser = $rsUsers->Fetch())
		{
			array_push($userList, $arUser);
		}

		CModule::IncludeModule("sale");
		foreach($userList as $user){
			if(!CSaleUserAccount::GetByUserID($user['ID'], "RUB")){
				$arFields = Array("USER_ID" => $user['ID'], "CURRENCY" => "RUB", "CURRENT_BUDGET" => 0);
				CSaleUserAccount::Add($arFields);  
			}
		}
    }
	
}