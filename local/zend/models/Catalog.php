
<?
/**
 * Class Sibirix_Model_Design
 * @method Sibirix_Model_Design_Row getElement($id=false)
 */
class Sibirix_Model_Catalog extends Sibirix_Model_Bitrix {

    protected $_iblockId = IB_CATALOG;

    protected $_instanceClass = 'Sibirix_Model_Design_Row';

    protected $_selectFields = array(
        'ID',
        'NAME',
        'CODE',
		'PROPERTY_FILE'
    );

    protected $_selectListFields = [
        'ID',
        'NAME',
        'CODE',
		'PROPERTY_FILE'
    ];
	
	/**
     * Добавление/изменение каталога
     * @param $fields
     * @return bool|int
    */
    public function editCatalog($fields) {
        $bxElement           = new CIBlockElement();
		if(CModule::IncludeModule("iblock"))
        $fields["IBLOCK_ID"] = IB_CATALOG;

        if ($fields["ID"] > 0) {
            $designId    = $fields["ID"];
            $designProps = $fields["PROPERTY_VALUES"];
            unset($fields["ID"], $fields["PROPERTY_VALUES"]);

            if (!empty($fields["NAME"])) {
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
            }

            $updateResult = $bxElement->Update($designId, $fields);

            if ($updateResult && !empty($designProps)) {
                $bxElement->SetPropertyValuesEx($designId, IB_CATALOG, $designProps);
            }

            $result = $designId;
        } else {
            $patternEnumId = EnumUtils::getListIdByXmlId($this->_iblockId, "STATUS", "draft");
            if (empty($fields["NAME"])) {
                $fields["NAME"] = 'Catalog' . time();
                $fields["CODE"] = CUtil::translit($fields["NAME"], "ru");
                $fields["PROPERTY_VALUES"]["STATUS"] = $patternEnumId;
				$fields["PROPERTY_VALUES"]["CREATED_BY"] = Sibirix_Model_User::getId();
            }
					
            $newId = $bxElement->Add($fields);
			
            $result = $newId;
        }
        return $result;
    }
}
