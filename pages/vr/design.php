<?
define('DESIGN_BLOCK_ID', 11);
define('ROOM_BLOCK_ID', 36);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
/* @var $APPLICATION */

$elementCode = $_GET['elementCode'];

$images = array();

// что дергаем
$arDesignSelect = array(
    'ID',
    'NAME',
    'CODE',
    'DETAIL_PICTURE',
    'PREVIEW_TEXT',
    'DETAIL_TEXT',
    'CREATED_BY',
    'PROPERTY_DESIGN_ROOM',
);
// фильтр по текущему коду, приходит с $_REQUEST
$arDesignFilter = array(
    "IBLOCK_ID" => DESIGN_BLOCK_ID,
    'CODE' => $elementCode,
    "ACTIVE" => "Y"
);
// получем
$resDesign = CIBlockElement::GetList(array(), $arDesignFilter, false, array(), $arDesignSelect);
$obDesign = $resDesign->GetNextElement();
if ($obDesign)
{
    $arDesignFields = $obDesign->GetFields();

    foreach ($arDesignFields['PROPERTY_DESIGN_ROOM_VALUE'] as $room)
    {
        /** комната */
        $arRoomsFilter = array(
            'IBLOCK_ID' => ROOM_BLOCK_ID, // id блока с комнатами
            'ID' => $room, // id дизайна
            "ACTIVE" => "Y"
        );

        $resRoom = CIBlockElement::GetList(array(), $arRoomsFilter, false, array(), array(
                    'ID',
                    'PROPERTY_PANORAMA',
        ));
        $obRoom = $resRoom->GetNextElement();
        if ($obRoom)
        {
            $arRoomFields = $obRoom->GetFields();
            if ($arRoomFields['PROPERTY_PANORAMA_VALUE'])
            {
                $images[] = array(
                    'id' => $arRoomFields['ID'],
                    'url' => CFile::GetPath($arRoomFields['PROPERTY_PANORAMA_VALUE']),
                );
            }
        }

        $APPLICATION->setTitle($arDesignFields['NAME']);
    }
}
?>

<a-scene>
    <a-assets>
        <img id="mark" src="/local/images/question-mark.png">";
        <img id="divan" src="/local/images/divan.png">";

        <?php
        $ind = 0;
        foreach ($images as $image)
        {
            echo "<img id=\"panorama-{$ind}\" src=\"{$image['url']}\">";
            $ind++;
        }
        ?>
    </a-assets>
    <a-sky src="#panorama-0" rotation="0 0 0"></a-sky>

    <a-entity position="0 0 0">
        <a-camera id="camera">
            <a-cursor id="cursor" color="#4CC3D9"></a-cursor>
        </a-camera>
    </a-entity>

    <a-plane id="mark" position="5 -2 5" width="0.8" height="0.8" src="#mark" transparent="true" opacity="0.5" look-at="[camera]">
        <a-event name="cursor-mouseenter" scale="1.2 1.2 1.2" opacity="1"></a-event>
        <a-event name="cursor-mouseleave" scale="1 1 1" opacity="0"></a-event>
        <a-event name="cursor-mouseenter" visible="true" target="#divan"></a-event>

    </a-plane>

    <a-plane id="divan" position="4 -3 4" width="6" height="6" src="#divan" transparent="true" visible="false" look-at="[camera]">
        <a-event name="cursor-mouseleave" visible="false"></a-event>
    </a-plane>   

</a-scene>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>